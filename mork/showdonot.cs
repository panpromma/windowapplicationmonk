﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraReports.UI;
namespace mork
{
    public partial class showdonot : MetroFramework.Forms.MetroForm
    {
        public showdonot()
        {
            InitializeComponent();
        }
        SqlConnection conn;
        public int dID { get; set; }//รหัสกิจนิมนต์
        public string Address { get; set; }//ที่อยู่
        public string number { get; set; }//เวลา
        public string date { get; set; }//วันที่
        public string nameP { get; set; }//สถานะ
        public string nameS { get; set; }//เบอร์โทร
        private void Showdonot_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            
            label1.Text = dID.ToString();

            label18.Text = Address.ToString();
            label5.Text = number.ToString();

            label3.Text = nameP.ToString();
            label8.Text = date.ToString();

            label4.Text = nameS.ToString();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            anumotana f = new anumotana();
            f.ID = Convert.ToInt32(label1.Text);

            f.ShowPreviewDialog();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
