﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraReports.UI;
namespace mork
{
    public partial class donatb : MetroFramework.Forms.MetroForm
    {
        public int ID { get; set; }
        public string namef { get; set; }
        public int UserID;
        public string name;
        SqlConnection conn;
        public string status { get; set; }

        public int dID { get; set; }//รหัสกิจนิมนต์
        public string Address { get; set; }//ที่อยู่
        public string number { get; set; }//เวลา
        public string date { get; set; }//วันที่
        public string nameP { get; set; }//สถานะ
        public string nameS { get; set; }//เบอร์โทร
      


        public donatb()
        {
            InitializeComponent();
            

        }

        private void Donatb_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            conn.Open();
            string id = frmLogin.eID.ToString();

            ShowData();

            string name = frmLogin.name;
            lblID.Text = id;
            lblName.Text = name;
        }

        private void Btna_Click(object sender, EventArgs e)
        {

            frmshowSecular f = new frmshowSecular();

            f.ShowDialog();

            txtSecularID.Text = f.SecularID.ToString();
            txtPhone.Text = f.Phone.ToString();
            textname.Text = f.NameF.ToString();
            
        }
        private void ShowData()
        {
            string sql = "select Donate ,s.name ,Number , dbo.mydate(Date) , p.Name ,Description  from Donates d inner join Personnels p on d.UserID=p.UserID inner join Seculars s on d.SecularID=s.SecularID order by Donate desc";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.Columns[0].HeaderCell.Value = "รหัส";
            dataGridView1.Columns[1].HeaderCell.Value = "ชื่อผู้บริจาค";
            dataGridView1.Columns[2].HeaderCell.Value = "จำนวน";
            dataGridView1.Columns[3].HeaderCell.Value = "วันที่";
            dataGridView1.Columns[4].HeaderCell.Value = "รหัสผู้รับ";
            dataGridView1.Columns[5].HeaderCell.Value = "รายละเอียด";

           
        }
        

        private void Btnseve_Click(object sender, EventArgs e)
        {

        }
       
        private void Btnseve_Click_1(object sender, EventArgs e)
        {
            if (txtnumber.Text.Trim() == "" || txtAddress.Text.Trim() == "")
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน");
            }

            else
            {
                string sql = "insert into Donates values(   @Description ,@Number , @Date , @UserID , @SecularID )";
                SqlCommand comm = new SqlCommand(sql, conn);





                comm.Parameters
                    .AddWithValue("@Description", txtAddress.Text.Trim());

                comm.Parameters
                    .AddWithValue("@Number", txtnumber.Text.Trim());

                comm.Parameters
                    .AddWithValue("@Date", dtpImportDate.Value);

                comm.Parameters
                   .AddWithValue("@UserID", frmLogin.eID);

                comm.Parameters
                   .AddWithValue("@SecularID", txtSecularID.Text.Trim());

                string sql1 = "Select top 1 Donate from Donates order by Donate desc";
                SqlCommand comm1 = new SqlCommand(sql1, conn);
                SqlDataReader dr;
                dr = comm1.ExecuteReader();

                if (dr.HasRows)
                {
                    dr.Read();
                    ID = dr.GetInt32(dr.GetOrdinal("Donate"));
                }
                else
                {
                    ID = 1;
                }
                dr.Close();


                if (conn.State == ConnectionState.Closed) conn.Open();
                comm.ExecuteNonQuery();
                
                conn.Close();
                
                clearfrom();
                ShowData();
               


            }
            
           

        }

        private void clearfrom()
        {
            txtAddress.Text = "";
            txtnumber.Text = "";
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            frmshowSecular f = new frmshowSecular();

            f.ShowDialog();

            txtSecularID.Text = f.SecularID.ToString();
            txtPhone.Text = f.Phone.ToString();
            textname.Text = f.NameF.ToString();

        }

        private void GroupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            clearfrom();
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            frmshowlistkin f = new frmshowlistkin();

            f.ShowDialog();
        }

        private void DataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var x = dataGridView1.CurrentRow.Cells;
            dID = Convert.ToInt16(x[0].Value.ToString());
            nameS = x[1].Value.ToString();
            number = x[2].Value.ToString();
            date = x[3].Value.ToString();
            nameP = x[4].Value.ToString();
            Address = x[5].Value.ToString();

            showdonot f = new showdonot();
            f.dID = this.dID;
            f.nameS = this.nameS;
            f.number = this.number;
            f.date = this.date;
          
            f.nameP = this.nameP;
            f.Address = this.Address;
            
            f.ShowDialog();
            ShowData();
        }

        private void Txtnumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar)))
                e.Handled = true;
        }
    }
}
