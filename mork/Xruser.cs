﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Linq;
namespace mork
{
    public partial class Xruser : DevExpress.XtraReports.UI.XtraReport
    {
        public Xruser()
        {
            InitializeComponent();
        }
        DataClasses1DataContext db;

        private void Xruser_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            db = new DataClasses1DataContext();

            var v = from k in db.Personnels

                    select new
                    {
                        k.Name,
                        k.Prefix,
                        k.UserID,
                        k.Department


                    };

            this.DataSource = v;
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ID.Text = Convert.ToString(this.GetCurrentColumnValue("UserID"));
            Prefix.Text = Convert.ToString(this.GetCurrentColumnValue("Prefix"));
            Namef.Text = Convert.ToString(this.GetCurrentColumnValue("Name"));
            Department.Text = Convert.ToString(this.GetCurrentColumnValue("Department"));
        }
    }
}
