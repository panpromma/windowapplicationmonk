﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace mork
{
    public partial class frmPlace : MetroFramework.Forms.MetroForm
    {
        
        public frmPlace()
        {
            InitializeComponent();
            
        }
        SqlConnection conn;
        
        private void Place_Load(object sender, EventArgs e)
        {
           
            conn = new ConnecDB().SqlStrCon();
            ShowData();
            cbo();
        }
        private void cbo()
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[] { new DataColumn("Id", typeof(int)), new DataColumn("Name", typeof(string)) });
            dt.Rows.Add(0, "ไม่พร้อมใช้งาน");
            dt.Rows.Add(1, "พร้อมใช้งาน");


            //Insert the Default Item to DataTable.


            //Assign DataTable as DataSource.
            cboClass.DataSource = dt;
            cboClass.DisplayMember = "Name";
            cboClass.ValueMember = "Id";

        }
        private void ShowData()
        {
            string sql = "select PlaceID , PlaceName , Width , Length ,Description, case  when Status = 0 then 'ไม่พร้อมใช้งาน' when Status = 1 then 'พร้อมใช้งาน' end  from Places";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            showskin();
        }
        private void showskin()
        {
            dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
            dgvdatag.Columns[1].HeaderCell.Value = "ชื่อสถาที่";
            dgvdatag.Columns[2].HeaderCell.Value = "ความกว้าง";
            dgvdatag.Columns[3].HeaderCell.Value = "ความยาว";
            dgvdatag.Columns[4].HeaderCell.Value = "รายละเอียด";
            dgvdatag.Columns[5].HeaderCell.Value = "สถานะ";



            //dgvdatag.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            //dgvdatag.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            //dgvdatag.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            //dgvdatag.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            //dgvdatag.BackgroundColor = Color.White;

            //dgvdatag.EnableHeadersVisualStyles = false;
            //dgvdatag.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            //dgvdatag.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            //dgvdatag.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            clearfrom();
        }
        private void clearfrom()
        {
            TextID.Text = "";
            TextPlaceName.Text = "";
            txtd.Text = "";
            txtWidth.Text = "";
            txtLength.Text = "";
         
        }

        private void Add_Click(object sender, EventArgs e)
        {
            if (TextPlaceName.Text.Trim() == "" || txtWidth.Text.Trim() == "" || txtLength.Text.Trim() == "" || txtd.Text.Trim() == "")
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน");
            }
            else
            { 
            try
            {
                string sql = "insert into Places values(@PlaceName,@Width,@Length,@Description,@Status)";
                SqlCommand comm = new SqlCommand(sql, conn);





                comm.Parameters
                    .AddWithValue("@PlaceName", TextPlaceName.Text.Trim());



                comm.Parameters
                   .AddWithValue("@Width", txtWidth.Text.Trim());

                comm.Parameters
                   .AddWithValue("@Length", txtLength.Text.Trim());
                comm.Parameters
                   .AddWithValue("@Description", txtd.Text.Trim());

                comm.Parameters
                   .AddWithValue("@Status", cboClass.SelectedIndex);




                if (conn.State == ConnectionState.Closed) conn.Open();
                comm.ExecuteNonQuery();
                conn.Close();
                clearfrom();
                ShowData();
            }

            catch
            {
                MessageBox.Show("ชื่อสถานที่ซ้ำ");
                return;
            }
            }
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("!!!!คณุต้องการจะลบใช่ไหม?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {

                string sql = "delete from Places where PlaceID = @PlaceID";
                SqlCommand comm = new SqlCommand(sql, conn);



                comm.Parameters

                    .AddWithValue("@PlaceID", TextID.Text.Trim());

                try
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                    clearfrom();
                    ShowData();

                }
                catch
                {
                    MessageBox.Show("ไม่สามารถลบข้มูลได้", "Erorr");

                    return;
                }
            }
        }

        private void Datag_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            var x = dgvdatag.CurrentRow.Cells;

          
            TextID.Text = x["PlaceID"].Value.ToString();
            TextPlaceName.Text = x["PlaceName"].Value.ToString();
            
            txtWidth.Text = x["Width"].Value.ToString();
            txtLength.Text = x["Length"].Value.ToString();
            txtd.Text = x["Description"].Value.ToString();
         
        }

        private void Modify_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("!!!!คณุต้องการจะแก้ไขไหม?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {


                string sql = "update Places set PlaceName=@PlaceName,Width=@Width,Length=@Length,Description=@Description,Status=@Status  where  PlaceID=@PlaceID  ";
                SqlCommand comm = new SqlCommand(sql, conn);


                comm.Parameters
                    .AddWithValue("@PlaceID", TextID.Text.Trim());

                comm.Parameters
                .AddWithValue("@PlaceName", TextPlaceName.Text.Trim());

               

                comm.Parameters
                   .AddWithValue("@Width", txtWidth.Text.Trim());

                comm.Parameters
                   .AddWithValue("@Length", txtLength.Text.Trim());
                comm.Parameters
                   .AddWithValue("@Description", txtd.Text.Trim());

                comm.Parameters
                   .AddWithValue("@Status", cboClass.SelectedIndex);





                try
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                    clearfrom();
                    ShowData();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

      

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            showdata(txtID.Text.ToString());
        }
        private void showdata(string text)
        {
            string sql = "select PlaceID , PlaceName , Width , Length ,Description, case  when Status = 0 then 'ไม่ว่าง' when Status = 1 then 'ว่าง' end   from Places"
                + " where PlaceName like '%' + @strName + '%'";
            SqlCommand com = new SqlCommand(sql, conn);
            com.Parameters.AddWithValue("@strName", text);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            showskin();
        }
    }
}
