﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Linq;
namespace mork
{
    public partial class xrNumberdamaged : DevExpress.XtraReports.UI.XtraReport
    {
        public xrNumberdamaged()
        {
            InitializeComponent();
        }
        DataClasses1DataContext db;
        private void XrNumberdamaged_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            db = new DataClasses1DataContext();
            var v = from es in db.Equipments
                    where es.Numberdamaged !=0 && es.Numberlost !=0
                    select new
                    {

                       es.EquipmentID,
                       es.Equipmenname,
                       es.Numbertreasury,
                       es.Numberdamaged,
                       es.Numberlost

                    };

            this.DataSource = v;
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel3.Text = Convert.ToString(this.GetCurrentColumnValue("EquipmentID"));
            xrLabel10.Text = Convert.ToString(this.GetCurrentColumnValue("Equipmenname"));
            xrLabel11.Text = Convert.ToString(this.GetCurrentColumnValue("Numbertreasury"));
            xrLabel12.Text = Convert.ToString(this.GetCurrentColumnValue("Numberdamaged"));
            xrLabel2.Text = Convert.ToString(this.GetCurrentColumnValue("Numberlost"));
        }
    }
}
