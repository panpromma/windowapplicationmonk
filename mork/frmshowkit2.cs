﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraReports.UI;
using System.Runtime.InteropServices;
namespace mork
{
    public partial class frmshowkit2 : Form
    {
        public frmshowkit2()
        {
            InitializeComponent();
        }
        public string mon;
        public string Time2;
        public string WorkName { get; set; }//ชื่อ
        public string status { get; set; }
        public int KitnmonID { get; set; }//รหัสกิจนิมนต์
        public string Time { get; set; }//เวลา
        public string date { get; set; }//วันที่
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        SqlConnection conn;
        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void Frmshowkit2_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            label1.Text = KitnmonID.ToString();
            
            label3.Text = Time.ToString();
            label4.Text = date.ToString();
            
            label18.Text = WorkName.ToString();
         
        }

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            XtraReport11 f = new XtraReport11();
            f.ID = Convert.ToInt32(label1.Text);
            f.mon = label4.Text;
            f.Time2 = label3.Text;
            f.ShowPreviewDialog();
        }
    }
}
