﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace mork
{
    public partial class frmVehicle : MetroFramework.Forms.MetroForm
    {
        
        public frmVehicle()
        {
            InitializeComponent();
            
        }
        SqlConnection conn;
        
        private void Vehicle_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            ShowData();
        }
        private void ShowData()
        {
            string sql = "select VehicleID , size , Vehiclecategory , color , License , capacity , Brand  ,case  when Status = 0 then 'ไม่ว่าง' when Status = 1 then 'ว่าง' end as status from Vehicles";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            showskin();
        }
        private void showskin()
        {
            dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
            dgvdatag.Columns[1].HeaderCell.Value = "ขนาด";
            dgvdatag.Columns[2].HeaderCell.Value = "ประเภทรถ";
            dgvdatag.Columns[3].HeaderCell.Value = "สี";
            dgvdatag.Columns[4].HeaderCell.Value = "ทะเบียน";
            dgvdatag.Columns[5].HeaderCell.Value = "ความจุ";
            dgvdatag.Columns[6].HeaderCell.Value = "ยี่ห้อ";
            dgvdatag.Columns[7].HeaderCell.Value = "สถานะ";
           

            //dgvdatag.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            //dgvdatag.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            //dgvdatag.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            //dgvdatag.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            //dgvdatag.BackgroundColor = Color.White;

            //dgvdatag.EnableHeadersVisualStyles = false;
            //dgvdatag.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            //dgvdatag.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            //dgvdatag.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
        }

        private void Add_Click(object sender, EventArgs e)
        {
            if (TextSize.Text.Trim() == "")
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน");
            } 
            else
            { 
                try
                { 
            string sql = "insert into Vehicles values(@size,@Vehiclecategory,@color,@License,@capacity,@Brand,@Status)";
            SqlCommand comm = new SqlCommand(sql, conn);





            comm.Parameters
                .AddWithValue("@size", TextSize.Text.Trim());

            comm.Parameters
                .AddWithValue("@Vehiclecategory", txtVehiclecategory.Text.Trim());

            comm.Parameters
                .AddWithValue("@color", txtcolor.Text.Trim());

            comm.Parameters
               .AddWithValue("@License", txtLicenseplate.Text.Trim());

            comm.Parameters
               .AddWithValue("@capacity", txtCapacity.Text.Trim());

            comm.Parameters
               .AddWithValue("@Brand", txtBrand.Text.Trim());

            comm.Parameters
               .AddWithValue("@Status", "1");




            if (conn.State == ConnectionState.Closed) conn.Open();
            comm.ExecuteNonQuery();
            conn.Close();
            clearfrom();
            ShowData();
                }
                catch
                {
                    MessageBox.Show("ชื่อสถานที่ซ้ำ");

                    return;
                }
            }
        }

        private void clearfrom()
        {
            TextID.Text = "";
            TextSize.Text = "";
            txtVehiclecategory.Text = "";
            txtcolor.Text = "";
            txtLicenseplate.Text = "";
            txtCapacity.Text = "";
            txtBrand.Text = "";
            
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            clearfrom();
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("!!!!คณุต้องการจะลบใช่ไหม?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {

                string sql = "delete from Vehicles where VehicleID = @VehicleID";
                SqlCommand comm = new SqlCommand(sql, conn);



                comm.Parameters

                    .AddWithValue("@VehicleID", TextID.Text.Trim());

                try
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                    clearfrom();
                    ShowData();

                }
                catch 
                {
                    MessageBox.Show("ไม่สามารถลบข้มูลได้", "Erorr");

                    return;
                }
            }
        }

        private void Datag_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            var x = dgvdatag.CurrentRow.Cells;

            
            TextID.Text = x["VehicleID"].Value.ToString();
            TextSize.Text = x["size"].Value.ToString();
            txtVehiclecategory.Text = x["Vehiclecategory"].Value.ToString();
            txtcolor.Text = x["color"].Value.ToString();
            txtLicenseplate.Text = x["License"].Value.ToString();
            txtCapacity.Text = x["capacity"].Value.ToString();
            txtBrand.Text = x["Brand"].Value.ToString();
           


        }

        private void Modify_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("!!!!คณุต้องการจะแก้ไขไหม?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {


                string sql = "update Vehicles set  size=@size , Vehiclecategory=@Vehiclecategory ,color=@color ,License=@License,capacity=@capacity , Brand=@Brand ,Status=@Status  where  VehicleID=@VehicleID  ";
                SqlCommand comm = new SqlCommand(sql, conn);


                comm.Parameters
                    .AddWithValue("@VehicleID", TextID.Text.Trim());

                comm.Parameters
                     .AddWithValue("@size", TextSize.Text.Trim());

                comm.Parameters
                    .AddWithValue("@Vehiclecategory", txtVehiclecategory.Text.Trim());

                comm.Parameters
                    .AddWithValue("@color", txtcolor.Text.Trim());

                comm.Parameters
                   .AddWithValue("@License", txtLicenseplate.Text.Trim());

                comm.Parameters
                   .AddWithValue("@capacity", txtCapacity.Text.Trim());

                comm.Parameters
                   .AddWithValue("@Brand", txtBrand.Text.Trim());

                comm.Parameters
                   .AddWithValue("@Status", "1");





                try
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                    clearfrom();
                    ShowData();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            showdata(txtID.Text.ToString());
        }
        private void showdata(string text)
        {
            string sql = "select VehicleID , size , Vehiclecategory , color , License , capacity , Brand  ,case  when Status = 0 then 'ไม่ว่าง' when Status = 1 then 'ว่าง' end as status from Vehicles"
                + " where Vehiclecategory like '%' + @strName + '%'" +
                "or color like '%' + @strName + '%'" +
                " or License like '%' + @strName + '%'";
            SqlCommand com = new SqlCommand(sql, conn);
            com.Parameters.AddWithValue("@strName", text);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            showskin();
        }
    }
}
