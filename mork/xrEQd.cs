﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Linq;
namespace mork
{
    public partial class xrEQd : DevExpress.XtraReports.UI.XtraReport
    {
        public xrEQd()
        {
            InitializeComponent();
        }
        DataClasses1DataContext db;
        private void XrEQd_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            db = new DataClasses1DataContext();

            var v = from ps in db.Placerequests
                    join p in db.Places on ps.PlaceID equals p.PlaceID
                    join pp in db.Personnels on ps.UserID equals pp.UserID
                    join s in db.Seculars on ps.SecularID equals s.SecularID
                    


                    select new
                    {

                        ps.RequestID,
                        Idate = String.Format("{0:dd MMMM yyy}",ps.Date),
                        ps.Time,
                        ps.Description,
                        ps.Phone,
                        p.PlaceName,
                        pp.Name,
                        s.name


                    };

            this.DataSource = v;
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel12.Text = Convert.ToString(this.GetCurrentColumnValue("RequestID"));
            xrLabel13.Text = Convert.ToString(this.GetCurrentColumnValue("Idate"));
            xrLabel14.Text = Convert.ToString(this.GetCurrentColumnValue("Time"));
            xrLabel15.Text = Convert.ToString(this.GetCurrentColumnValue("Description"));
            xrLabel16.Text = Convert.ToString(this.GetCurrentColumnValue("Phone"));
            xrLabel11.Text = Convert.ToString(this.GetCurrentColumnValue("PlaceName"));
            xrLabel17.Text = Convert.ToString(this.GetCurrentColumnValue("Name"));
            xrLabel18.Text = Convert.ToString(this.GetCurrentColumnValue("name"));
        }
    }
}
