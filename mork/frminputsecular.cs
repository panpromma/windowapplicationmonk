﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mork
{
    public partial class frminputsecular : Form
    {
        public frminputsecular()
        {
            InitializeComponent();
        }
        public int ID { get; set; }
        public string Prefix { get; set; }
        public string NameF { get; set; }
        public int Age { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }

        public string mode { get; set; }
        private void Frminputsecular_Load(object sender, EventArgs e)
        {
            if(mode=="เพิ่ม")
            {
                btndelete.Visible = false;
                btnmodify.Visible = false;

                TextID.Text = null;
                txtNameF.Text = null;
                //cmbPrefix. = Prefix.ToString();
                txtAge.Text = null;
                txtPhone.Text = null;
                txtAddress.Text = null;
            }
            if(mode=="แก้ไข")
            {
                btnseve.Visible = false;
                TextID.Text = ID.ToString();
                txtNameF.Text = NameF.ToString();
                //cmbPrefix. = Prefix.ToString();
                txtAge.Text = Age.ToString();
                txtPhone.Text = Phone.ToString();
                txtAddress.Text = Address.ToString();
            }


            



        }
    }
}
