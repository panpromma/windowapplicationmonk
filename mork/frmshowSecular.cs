﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
namespace mork
{
    public partial class frmshowSecular : MetroFramework.Forms.MetroForm
    {
        public frmshowSecular()
        {
            InitializeComponent();
        }
        SqlConnection conn;
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        public int ID { get; set; }
        public string Prefix { get; set; }
        public string NameF { get; set; }
        public int Age { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public int SecularID { get; set; }
        private void FrmshowSecular_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            ShowData();
            
        }
        private void ShowData()
        {
            string sql = "select * from Seculars ORDER BY SecularID desc";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            showskin();
        }
       
             private void showskin()
        {
            dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
            dgvdatag.Columns[1].HeaderCell.Value = "ชื่อ";
            dgvdatag.Columns[2].HeaderCell.Value = "เบอร์โทร";
            dgvdatag.Columns[3].HeaderCell.Value = "ที่อยู่";
            dgvdatag.Columns[4].HeaderCell.Value = "อายุ";
            


            
        }
        private void showdata(string text)
        {
            string sql = "select * from Seculars "
                        + "where name like '%' + @strName + '%' ";
                       
            SqlCommand com = new SqlCommand(sql, conn);
            com.Parameters.AddWithValue("@strName", text);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            showskin();
        }

        private void Dgvdatag_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {

            var x = dgvdatag.CurrentRow.Cells;
            SecularID = Convert.ToInt16(x[0].Value);
            NameF = x[1].Value.ToString();
            Phone = x[2].Value.ToString();
            
           

        }

        private void Dgvdatag_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            
            var x = dgvdatag.CurrentRow.Cells;
            SecularID = Convert.ToInt16(x["SecularID"].Value);
            Phone = Convert.ToString(x["phone"].Value);
           NameF = Convert.ToString(x["name"].Value);
            this.Close();
            //f.mode = "แก้ไข";




        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
            
        }

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            NameF = "";
            Phone = "";
            this.Close();
           
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            showdata(txtID.Text);
        }
       

        private void Dgvdatag_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void BarraTitulo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            
            NameF = "";
            Phone = "";
            this.Close();
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }
    }
}
