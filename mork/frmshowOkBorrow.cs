﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
namespace mork
{
    public partial class frmshowOkBorrow : MetroFramework.Forms.MetroForm
    {
        
        public frmshowOkBorrow()
        {
            InitializeComponent();
           
        }
        SqlConnection conn;
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void FrmshowOkBorrow_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            ShowData2();
        }
        private void ShowData2()
        {
            string sql = "select b.BorrowID as รหัสการยืม , b.RedateB as วันที่นำมาคืน, b.Redate as วันที่ยืม , b.Time as เวลา , b.Retundate as วันที่นำมาคืนครบ  , case  when b.Approve = 0 then 'ยังไม่อนุมัติ' when b.Approve = 1 then 'อนุมัติแล้ว' end as สถานะ ,p.Name as ชื่อผู้รับงาน, s.name as ชื่อฆราวาส  from Borrows b INNER JOIN Personnels p on  b.UserID = p.UserID INNER JOIN  Seculars s on b.SecularID = s.SecularID";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }
        private void ShowData(string name)
        {
            if (txtID.Text != "")
            {
                string sql = "select b.BorrowID as รหัสการยืม , b.RedateB as วันที่นำมาคืน, b.Redate as วันที่ยืม , b.Time as เวลา , b.Retundate as วันที่นำมาคืนครบ  , case  when b.Approve = 0 then 'ยังไม่อนุมัติ' when b.Approve = 1 then 'อนุมัติแล้ว' end as สถานะ ,p.Name as ชื่อผู้รับงาน, s.name as ชื่อฆราวาส  from Borrows b INNER JOIN Personnels p on  b.UserID = p.UserID INNER JOIN  Seculars s on b.SecularID = s.SecularID" +
                    " where Redate like '%' + @name + '%'" +
                    "  or BorrowID like '%' + @name + '%'";

                SqlCommand com = new SqlCommand(sql, conn);
                com.Parameters.AddWithValue("@name", name);
                SqlDataAdapter da = new SqlDataAdapter(com);
                DataSet ds = new DataSet();
                da.Fill(ds);
               
                dgvdatag.DataSource = ds.Tables[0];
                dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            }
            else
            {
                ShowData2();
            }
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            ShowData(txtID.Text.ToString());
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
