﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Linq;
namespace mork
{
    public partial class xrReportmonk1 : DevExpress.XtraReports.UI.XtraReport
    {
        
        public xrReportmonk1()
        {
            InitializeComponent();
        }
        DataClasses1DataContext db;
        public int ID;
        int number;
        private void XrReportmonk1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            db = new DataClasses1DataContext();

            var v = from k in db.Kitnmons
                    join ck in db.CategoryKitnimons on k.CategoryKitID equals ck.CategoryKitID
                    join ks in db.Kitnimonlists on k.KitnmonID equals ks.KitnmonID
                    join p in db.Personnels on ks.UserID equals p.UserID
                    where ks.UserID == ID
                    select new
                    {
                        k.KitnmonID,
                        ck.WorkName,
                        Idate = String.Format("{0:dd MMMM yyy}", ks.Dates),
                        
                        p.Name,
                        ks.UserID,
                        k.Address,
                        ks.TimeS,
                        ks.TimeEN


                    };

            this.DataSource = v;

        }
        private void ReportHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

            
            string name;
            string WorkName;



            name = Convert.ToString(this.GetCurrentColumnValue("UserID"));
            WorkName = Convert.ToString(this.GetCurrentColumnValue("Name"));


            xrLabel4.Text = name;
            xrLabel5.Text = WorkName;

        }
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            number += 1;
            xrLabel12.Text = Convert.ToString(number);
            //xrLabel11.Text = Convert.ToString(this.GetCurrentColumnValue("KitnmonID"));
            xrLabel10.Text = Convert.ToString(this.GetCurrentColumnValue("WorkName"));

            xrLabel13.Text = Convert.ToString(this.GetCurrentColumnValue("Address"));
            xrLabel9.Text = Convert.ToString(this.GetCurrentColumnValue("Idate"));

            xrLabel20.Text = Convert.ToString(this.GetCurrentColumnValue("TimeS"));
            xrLabel22.Text = Convert.ToString(this.GetCurrentColumnValue("TimeEN"));
            //xrLabel13.Text = Convert.ToString(this.GetCurrentColumnValue("Time"));
        }
    }
}
