﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace mork
{
    public partial class frmdonat : MetroFramework.Forms.MetroForm
    {
       
        public int ID { get; set; }
        public int UserID { get; set; }
        public string namef { get; set; }
        SqlTransaction tr;
        SqlCommand comm;
        SqlConnection conn;
        public frmdonat()
        {
            InitializeComponent();
            
        }

        private void Frmdonat_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            conn.Open();
            string id = frmLogin.eID.ToString();
            lsvFormat();

            string name = frmLogin.name;

            lblID.Text = id;
            lblName.Text = name;
        }
        private void lsvFormat()
        {

            lsvShow.Columns.Add("ลำดับที่", 120, HorizontalAlignment.Right);
            lsvShow.Columns.Add("รหัสอุปกรณ์", 120, HorizontalAlignment.Right);
            lsvShow.Columns.Add("ชื่ออุปกรณ์", 120, HorizontalAlignment.Right);
            lsvShow.Columns.Add("จำนวน", 120, HorizontalAlignment.Right);
           


            lsvShow.View = View.Details;
            lsvShow.GridLines = true;
            lsvShow.FullRowSelect = true;



        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            clearfrom();
        }
        private void clearfrom()
        {
            txtIDmonk.Text = "";
            txtnamemonk.Text = "";
            txtnumber.Text = "";
            txtAddress.Text = "";
            txtnames.Text = "";
            lsvShow.Clear();
            lsvFormat();

        }
        private void Btnseve_Click(object sender, EventArgs e)
        {
            int ID = 0;
            if (txtnames.Text.Trim() == "" || txtAddress.Text.Trim() == "" || txtnumber.Text.Trim() == "")
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน");
            }
            if (lsvShow.Items.Count > 0)
            {
                if (MessageBox.Show("ต้องการบันทึกรายการสั่งซื้อหรือไม่", "กรุณายินยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    tr = conn.BeginTransaction();

                    string sqlEmpBills = "insert into Donateequipments (datedonat,description,donor,UserID)"
                                       + " values (@datedonat,@description,@donor,@UserID)";

                    comm = new SqlCommand(sqlEmpBills, conn, tr);

                    comm.Parameters.Add("@datedonat", SqlDbType.Date).Value = dtpImportDate.Value;
                    comm.Parameters.Add("@description", SqlDbType.NChar).Value = txtAddress.Text;
                    comm.Parameters.Add("@donor", SqlDbType.NChar).Value = txtnames.Text;
                    comm.Parameters.Add("@UserID", SqlDbType.Int).Value = Convert.ToInt16(lblID.Text);
                   


                    comm.ExecuteNonQuery();


                    string sql = "Select top 1 DonateequipID from Donateequipments order by DonateequipID desc";
                    SqlCommand comm1 = new SqlCommand(sql, conn, tr);
                    SqlDataReader dr;
                    dr = comm1.ExecuteReader();
                    if (dr.HasRows)
                    {
                        dr.Read();
                        ID = dr.GetInt32(dr.GetOrdinal("DonateequipID"));
                    }
                    else
                    {
                        ID = 1;
                    }
                    dr.Close();
                    int i = 0;


                    for (i = 0; i <= lsvShow.Items.Count - 1; i++)
                    {


                        string sql3 = "insert into DonateEMlists (DonateequipID , EquipmentID ,Number) values(@DonateequipID ,@EquipmentID ,@Number)";

                        SqlCommand comm = new SqlCommand(sql3, conn, tr);


                        comm.Parameters.AddWithValue("@DonateequipID", ID);

                        comm.Parameters.AddWithValue("@EquipmentID", SqlDbType.Int).Value = Convert.ToInt16(lsvShow.Items[i].SubItems[1].Text);
                        comm.Parameters.AddWithValue("@Number", SqlDbType.Int).Value = Convert.ToInt16(lsvShow.Items[i].SubItems[3].Text);

                        comm.ExecuteNonQuery();

                        //ลบสินค้า
                        string sqlUpdateStock = " update Equipments set "
                                          + " Numbertreasury = Numbertreasury + @Qty "
                                          + " where EquipmentID like @EquipmentID ";
                        comm = new SqlCommand(sqlUpdateStock, conn, tr);
                        comm.Parameters.Add("@Qty", SqlDbType.Int).Value = Convert.ToInt16(lsvShow.Items[i].SubItems[3].Text);
                        comm.Parameters.Add("@EquipmentID", SqlDbType.Int).Value = Convert.ToInt16(lsvShow.Items[i].SubItems[1].Text);
                        comm.ExecuteNonQuery();




                    }

                }
                tr.Commit();
                MessageBox.Show("บันทึกรายการเรียบร้อยแล้ว", "ผลการทำงาน");
                btnClear.PerformClick();
                lsvShow.Clear();

                clearfrom();
                


            }
        }

        private void Btnadd_Click(object sender, EventArgs e)
        {

        }

        private void Btnadd_Click_1(object sender, EventArgs e)
        {
            if (txtnumber.Text.Trim() == "")
            {
                txtnumber.Focus(); return;
            }

            ListViewItem lvi;
            int i = 0;
            string tmpProductID;
            for (i = 0; i <= lsvShow.Items.Count - 1; i++)
            {
                tmpProductID = (lsvShow.Items[i].SubItems[2].Text);

                if (txtnamemonk.Text.Trim() == tmpProductID)
                {
                    if (MessageBox.Show("มีรายการซ้ำอยู่ ต้องการเพิ่มอีกหรือไม่", "ผิดพลาด") == DialogResult.Yes)
                    {
                        txtnamemonk.Focus();
                        txtnamemonk.SelectAll();
                        break;
                    }

                    else
                    {
                        return;
                    }

                }


            }
            int j = 0;
            int No = 0;
            for (j = 0; j <= lsvShow.Items.Count; j++)
            {
                No = No + 1;
            }
            string[] anyData;
            anyData = new string[] { No.ToString(), txtIDmonk.Text.ToString(), txtnamemonk.Text, txtnumber.Text};
            lvi = new ListViewItem(anyData);
            lsvShow.Items.Add(lvi);

            btnseve.Enabled = true;
            txtnamemonk.Focus();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            using (frmshowSecular f = new frmshowSecular())
            {
                f.ShowDialog();
               
                txtnames.Text = f.NameF.ToString();
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            frmselectequipment f = new frmselectequipment();

            f.ShowDialog();
            txtIDmonk.Text = f.UserID.ToString();
            
            txtnamemonk.Text = f.namef.ToString();
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            fromdonoteEm f = new fromdonoteEm();
            f.ShowDialog();

        }
    }
}
