﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using DevExpress.XtraReports.UI;

namespace mork
{
    public partial class frmshowlistborrow : MetroFramework.Forms.MetroForm

    {
        public frmshowlistborrow()
        {
            InitializeComponent();
        }
        SqlConnection conn;

        public int BorrowID { get; set; }//รหัสกิจนิมนต์
        public string status { get; set; }
        public string RedateB { get; set; }
        public string Redate { get; set; }
        public string Time { get; set; }
        public string Retundate { get; set; }
        public string Approve { get; set; }
        public string nameP { get; set; }//ชื่อผู้รับ
        public string names { get; set; }//ชื่อฆราวาส
        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frmshowlistborrow_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            ShowData();
        }
        private void ShowData()
        {
            string sql = "select b.BorrowID as รหัสการยืม ,  dbo.mydate(b.RedateB) as วันที่นำมาคืน,  dbo.mydate(b.Redate) as วันที่ยืม , b.Time as เวลา ,  dbo.mydate(b.Retundate) as วันที่นำมาคืนครบ  , case  when b.Approve = 0 then 'ยังไม่อนุมัติ' when b.Approve = 1 then 'อนุมัติแล้ว' end as สถานะ ,p.Name as ชื่อผู้รับงาน, s.name as ชื่อฆราวาส  from Borrows b INNER JOIN Personnels p on  b.UserID = p.UserID INNER JOIN  Seculars s on b.SecularID = s.SecularID order by BorrowID desc";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            datag.DataSource = ds.Tables[0];
            datag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void Datag_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var x = datag.CurrentRow.Cells;
            BorrowID = Convert.ToInt16(x[0].Value.ToString());
            RedateB = x[1].Value.ToString();
            Redate = x[2].Value.ToString();
            Time = x[3].Value.ToString();
            Retundate = x[4].Value.ToString();
            Approve = x[5].Value.ToString();
            nameP = x[6].Value.ToString();
            names = x[7].Value.ToString();

            frmshowaddBorrow f = new frmshowaddBorrow();
            f.BorrowID = this.BorrowID;
            f.RedateB = this.RedateB;
            f.Redate = this.Redate;
            f.Time = this.Time;
            f.Retundate = this.Retundate;
            f.Approve = this.Approve;
            f.nameP = this.nameP;
            f.names = this.names;
            f.status = "แสดง";
            f.ShowDialog();
            ShowData();
        }

        private void Datag_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            ShowData(txtID.Text.ToString());
        }
        private void ShowData(string name)
        {
            if (txtID.Text != "")
            {
                string sql = "select b.BorrowID as รหัสการยืม ,  dbo.mydate(b.RedateB) as วันที่นำมาคืน,  dbo.mydate(b.Redate) as วันที่ยืม , b.Time as เวลา ,  dbo.mydate(b.Retundate) as วันที่นำมาคืนครบ  , case  when b.Approve = 0 then 'ยังไม่อนุมัติ' when b.Approve = 1 then 'อนุมัติแล้ว' end as สถานะ ,p.Name as ชื่อผู้รับงาน, s.name as ชื่อฆราวาส  from Borrows b INNER JOIN Personnels p on  b.UserID = p.UserID INNER JOIN  Seculars s on b.SecularID = s.SecularID " +
                    "where Approve = 0" +
                    " and b.Redate like '%' + @name + '%'" +

                    "or p.Name like '%' + @name + '%'" +
                    "or s.name like '%' + @name + '%'" +
                     "or b.RedateB like '%' + @name + '%'" +
                    " order by BorrowID desc";





                SqlCommand com = new SqlCommand(sql, conn);
                com.Parameters.AddWithValue("@name", name);
                SqlDataAdapter da = new SqlDataAdapter(com);
                DataSet ds = new DataSet();
                da.Fill(ds);
                datag.DataSource = ds.Tables[0];

                datag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            }
            else
            {
                ShowData();
            }
        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            Xreeee f = new Xreeee();
            f.ShowPreviewDialog();
        }
    }
}
