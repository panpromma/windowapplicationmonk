﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraReports.UI;
namespace mork
{
    public partial class frmshowworkPlace : MetroFramework.Forms.MetroForm
    {
        public int PlaceID { get; set; }
        public string PlaceName { get; set; }
        public string Width1 { get; set; }
        public string Length1 { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        SqlConnection conn;
        public frmshowworkPlace()
        {
            InitializeComponent();
        }

        private void FrmshowworkPlace_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            label2.Text = PlaceID.ToString();
            label3.Text = PlaceName.ToString();
            label4.Text = Width1.ToString();
            label6.Text = Length1.ToString();
            label5.Text = Description.ToString();
            label8.Text = Status.ToString();
            ShowData();
        }
        private void ShowData()
        {
            string sql = "select RequestID as รหัสการขอใช้, dbo.mydate(ps.Date) as วันที่ , Time as เวลา ,ps.Description  as รายละเอียด from Placerequests ps inner join Places p on ps.PlaceID=p.PlaceID where p.PlaceID='" + label2.Text + "' order by RequestID DESC";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;


        }
    }
}
