﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using DevExpress.XtraReports.UI;
namespace mork
{
    public partial class frmshowkit1cs : MetroFramework.Forms.MetroForm
    {
        
        SqlConnection conn;
        public int UserID { get; set; }
        public int ID;
        public frmshowkit1cs()
        {
            InitializeComponent();
        }

        private void Frmshowkit1cs_Load(object sender, EventArgs e)
        {
            label2.Text = this.UserID.ToString();
            conn = new ConnecDB().SqlStrCon();
            conn.Open();
            ShowData();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
            private void ShowData()
            {
                string sql = "select k.KitnmonID ,cs.WorkName ,dbo.mydate(ks.Dates) ,ks.TimeS,ks.TimeEN ,k.Address as ที่อยู่,case  when k.Approve = 0 then 'ยังไม่อนุมัติ' when k.Approve = 1 then 'อนุมัติแล้ว' end as สถานะ from Personnels  p INNER JOIN  Kitnimonlists ks on p.UserID = ks.UserID INNER JOIN  Kitnmons k on ks.KitnmonID = k.KitnmonID INNER JOIN CategoryKitnimons cs on k.CategoryKitID=cs.CategoryKitID where p.UserID='" + label2.Text + "'";
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                DataSet ds = new DataSet();
                da.Fill(ds);
                dgvdatag.DataSource = ds.Tables[0];
                dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
                dgvdatag.Columns[1].HeaderCell.Value = "ชื่องาน";
                dgvdatag.Columns[2].HeaderCell.Value = "วันที่";
                dgvdatag.Columns[3].HeaderCell.Value = "เวลาเริ่ม";
                dgvdatag.Columns[4].HeaderCell.Value = "เวลาสิ้นสุด";
        }

        private void Panel1_MouseDown(object sender, MouseEventArgs e)
        {
           
        }

        private void PictureBox1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            xrReportmonk1 f = new xrReportmonk1();
            f.ID = Convert.ToInt32(label2.Text);
            f.ShowPreviewDialog();
        }
    }
}
