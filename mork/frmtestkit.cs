﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;
using System.Globalization;
namespace mork
{
    public partial class frmtestkit : MetroFramework.Forms.MetroForm

    {
        public int ID { get; set; }
        
        SqlConnection conn;
     
        public int UserID { get; set; }
        public string name;


   
        SqlTransaction tr;
        SqlCommand comm;
        public string NameF { get; set; }

        public string Phone { get; set; }

        public int SecularID { get; set; }
        public int IDusername { get; set; }
        public string nameaa { get; set; }

        public frmtestkit()
        {
            InitializeComponent();

            


        }

        private void Frmtestkit_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            conn.Open();
            setcboCategorykitnimonm();
            lsvFormat();
            setcboVehicles();
            lsvFormat1();
            datedd();



            string id = frmLogin.eID.ToString();



            string name = frmLogin.name;
            lblID.Text = id;
            lblName.Text = name;

            
            

        }
        private void lsvFormat1()
        {

            listView1.Columns.Add("ลำดับที่", 60, HorizontalAlignment.Center);
            listView1.Columns.Add("รหัสพระ", 85, HorizontalAlignment.Right);
            listView1.Columns.Add("ชื่อพระ", 100, HorizontalAlignment.Left);
            listView1.Columns.Add("วันที่", 135, HorizontalAlignment.Left);
            listView1.Columns.Add("เวลาเริ่ม", 80, HorizontalAlignment.Left);
            listView1.Columns.Add("เวลาสิ้นสุด", 80, HorizontalAlignment.Left);



            listView1.View = View.Details;
            listView1.GridLines = true;
            listView1.FullRowSelect = true;



        }

        private void setcboVehicles()
        {
            string sql = "select Vehiclecategory,VehicleID from Vehicles";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);

            cboVehiclesname.DataSource = ds.Tables[0];
            cboVehiclesname.DisplayMember = "Vehiclecategory";
            cboVehiclesname.ValueMember = "VehicleID";

        }
        private void BtnSearch_Click(object sender, EventArgs e)
        {
            frmshowSecular f = new frmshowSecular();

            f.ShowDialog();

            txtSecularID.Text = f.SecularID.ToString();
            txtPhone.Text = f.Phone.ToString();
            textname.Text = f.NameF.ToString();
        }
        private void setcboCategorykitnimonm()
        {
            string sql = "select CategoryKitID ,WorkName from CategoryKitnimons order by CategoryKitID";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);

            cboWorkname.DataSource = ds.Tables[0];
            cboWorkname.DisplayMember = "WorkName";
            cboWorkname.ValueMember = "CategoryKitID";
        }
        private void ShowData11()
        {

            


        }
        private void BtnClear_Click(object sender, EventArgs e)
        {
            lsvShow.Clear();
            listView1.Clear();

            clearfrom();
            lsvFormat();
            lsvFormat1();
           
        }
        private void clearfrom()
        {
            textBoxID.Text = "";
            textBoxName.Text = "";
            txtAddress.Text = "";
            txtPhone.Text = "";
            textname.Text = "";
            txtSecularID.Text = "";
            txrname.Text = "";
            mtbS.Clear();
            mtben.Clear();



        }
        private void lsvFormat()
        {
            //int counter = 0;
            //ListViewItem item = lsvShow.Items.Add("ลำดับที่",(++counter).ToString(),70);


            lsvShow.Columns.Add("รหัส", 50, HorizontalAlignment.Right);
            lsvShow.Columns.Add("ประเภทรถ", 217, HorizontalAlignment.Left);


            lsvShow.View = View.Details;
            lsvShow.GridLines = true;
            lsvShow.FullRowSelect = true;



        }
        private void BunifuFlatButton1_Click(object sender, EventArgs e)
        {
            frmshowPersonnel f = new frmshowPersonnel();
           
            f.ShowDialog();
            textBoxID.Text = f.IDusername.ToString();
            textBoxName.Text = f.nameaa.ToString();


        }

        private void Label9_Click(object sender, EventArgs e)
        {

        }

        private void LsvShow_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int i = 0;
            for (i = 0; i <= lsvShow.SelectedItems.Count - 1; i++)
            {
                ListViewItem lsv = lsvShow.SelectedItems[i];
                lsvShow.Items.Remove(lsv);

            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (/*maskedTextBox1.Text.Trim() == ":  -  :" ||*/ textBoxName.Text.Trim() == "")
            {
                MessageBox.Show("กรุณาใส่ข้อมูลให้ครอบ");
                return;
            }
            ListViewItem lvi;
            int i = 0;
            string tmpProductID;
            for (i = 0; i <= listView1.Items.Count - 1; i++)
            {
                tmpProductID = (listView1.Items[i].SubItems[3].Text);

                if (textBoxName.Text.Trim() == tmpProductID)
                {
                    if (MessageBox.Show("มีรายการซ้ำอยู่ ต้องการเพิ่มอีกหรือไม่", "ผิดพลาด") == DialogResult.Yes)
                    {
                        textBoxName.Focus();
                        textBoxName.SelectAll();
                        break;
                    }

                    else
                    {
                        return;
                    }

                }


            }
            int j = 0;
            int No = 0;
            for (j = 0; j <= listView1.Items.Count; j++)
            {
                No = No + 1;
            }
            string[] anyData;
            anyData = new string[] { No.ToString(), textBoxID.Text, textBoxName.Text, dtpImportDate.Text.ToString(),mtbS.Text,mtben.Text };
            lvi = new ListViewItem(anyData);
            listView1.Items.Add(lvi);

            btnseve.Enabled = true;
            mtbS.Focus();
        }

        private void ListView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int i = 0;
            for (i = 0; i <= listView1.SelectedItems.Count - 1; i++)
            {
                ListViewItem lsv = listView1.SelectedItems[i];
                listView1.Items.Remove(lsv);

            }
        }

        private void Btnseve_Click(object sender, EventArgs e)
        {
            int ID = 0;

            if (txrname.Text.Trim() == "" || txtAddress.Text.Trim() == "" || txtSecularID.Text.Trim() == "")
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน");
            }



            if (lsvShow.Items.Count > 0)
            {
                
                if (MessageBox.Show("ต้องการบันทึกรายการสั่งซื้อหรือไม่", "กรุณายินยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    tr = conn.BeginTransaction();

                    string sqlEmpBills = "insert into Kitnmons (Address,Workname,timeD,dateD,Approve,Phone,UserID,SecularID,CategoryKitID)"
                                       + " values (@Address,@Workname,@timeD,@dateD,@Approve,@Phone,@UserID,@SecularID,@CategoryKitID )";

                    comm = new SqlCommand(sqlEmpBills, conn, tr);

                    comm.Parameters.Add("@Address", SqlDbType.NVarChar).Value = txtAddress.Text.ToString();
                    comm.Parameters.Add("@Workname", SqlDbType.NVarChar).Value = txrname.Text.ToString();
                    comm.Parameters.Add("@timeD", SqlDbType.Time).Value = DateTime.Now.ToLongTimeString();
                    comm.Parameters.Add("@dateD", SqlDbType.Date).Value = DateTime.Now.ToLongDateString();
                    comm.Parameters.Add("@Approve", SqlDbType.Int).Value = Convert.ToInt16("0");
                    comm.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = txtPhone.Text.ToString();
                    comm.Parameters.Add("@UserID", SqlDbType.Int).Value = Convert.ToInt16(lblID.Text);
                    comm.Parameters.Add("@SecularID", SqlDbType.Int).Value = Convert.ToInt16(txtSecularID.Text);
                    comm.Parameters.Add("@CategoryKitID", SqlDbType.Int).Value = Convert.ToInt16(cboWorkname.SelectedValue);
                   






                    comm.ExecuteNonQuery();
                    //for (i = 0; i <= listView1.Items.Count - 1; i++)
                    //{


                    //    string sql3 = "insert into Kitnmons (CategoryKitID,Time) values(@CategoryKitID,@Time)";

                    //    SqlCommand comm = new SqlCommand(sql3, conn, tr);



                    //    comm.Parameters.AddWithValue("@CategoryKitID", Convert.ToString(listView1.Items[i].SubItems[1].Text));
                    //    comm.Parameters.AddWithValue("@Time", Convert.ToString(listView1.Items[i].SubItems[2].Text));






                    //    comm.ExecuteNonQuery();


                    //}

                    string sql = "Select top 1 KitnmonID from Kitnmons order by KitnmonID desc";
                    SqlCommand comm1 = new SqlCommand(sql, conn, tr);
                    SqlDataReader dr;
                    dr = comm1.ExecuteReader();
                    if (dr.HasRows)
                    {
                        dr.Read();
                        ID = dr.GetInt32(dr.GetOrdinal("KitnmonID"));
                    }
                    else
                    {
                        ID = 1;
                    }
                    dr.Close();
                    int i = 0;
                    for (i = 0; i <= listView1.Items.Count - 1; i++)
                    {
                        if (lblName.Text.Trim() == "")
                        {
                            MessageBox.Show("กรุณาป้อนรหัสสินค้า", "ผิดพลาด");
                            lblName.Focus();
                        }

                        string sql2 = "insert into Kitnimonlists (Number,UserID,KitnmonID,Dates,TimeS,TimeEN) values(@Number,@UserID, @KitnmonID,@Dates,@TimeS,@TimeEN)";

                        SqlCommand comm = new SqlCommand(sql2, conn, tr);


                        comm.Parameters.AddWithValue("@Number", Convert.ToInt16(listView1.Items[i].SubItems[0].Text));
                        comm.Parameters.AddWithValue("@UserID", Convert.ToInt16(listView1.Items[i].SubItems[1].Text));
                        comm.Parameters.AddWithValue("@Dates", Convert.ToDateTime(listView1.Items[i].SubItems[3].Text));
                        comm.Parameters.AddWithValue("@TimeS", Convert.ToDateTime(listView1.Items[i].SubItems[4].Text));
                        comm.Parameters.AddWithValue("@TimeEN", Convert.ToDateTime(listView1.Items[i].SubItems[5].Text));
                        //comm.Parameters.AddWithValue("@Time", Convert.ToString(listView1.Items[i].SubItems[3].Text));
                        comm.Parameters.AddWithValue("@KitnmonID", ID);


                       
                        //string sql3 = "insert into Usevehicless (KitnmonID,VehicleID) values(@KitnmonID,@VehicleID)";

                        //SqlCommand comm2 = new SqlCommand(sql3, conn, tr);




                        //comm.Parameters.AddWithValue("@KitnmonID", ID);
                        //comm.Parameters.AddWithValue("@UserID", Convert.ToInt16(lsvShow.Items[i].SubItems[0].Text));







                        comm.ExecuteNonQuery();


                    }

                    for (i = 0; i <= lsvShow.Items.Count - 1; i++)
                    {


                        string sql3 = "insert into Usevehicless (VehicleID,KitnmonID) values(@VehicleID, @KitnmonID)";

                        SqlCommand comm = new SqlCommand(sql3, conn, tr);


                        
                        comm.Parameters.AddWithValue("@VehicleID", Convert.ToInt16(lsvShow.Items[i].SubItems[0].Text));
                        comm.Parameters.AddWithValue("@KitnmonID", ID);




                        comm.ExecuteNonQuery();


                    }
                    

                }
                tr.Commit();
                MessageBox.Show("บันทึกรายการเรียบร้อยแล้ว", "ผลการทำงาน");
                datedd();
                btnClear.PerformClick();
                //lsvFormat();
            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            frmshowlistkin f = new frmshowlistkin();

            f.ShowDialog();
        }

        private void CboVehiclesname_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            //if (cboWorkname.Text.Trim() == "")
            //{
            //    cboWorkname.Focus(); return;
            //}

            //ListViewItem lvi2;
            //int i = 0;
            //string tmpProductID;
            //for (i = 0; i <= lsvShow.Items.Count - 1; i++)
            //{
            //    tmpProductID = (lsvShow.Items[i].SubItems[1].Text);

            //    if (cboVehiclesname.Text.Trim() == tmpProductID)
            //    {
            //        if (MessageBox.Show("มีรายการซ้ำอยู่ ต้องการเพิ่มอีกหรือไม่", "ผิดพลาด") == DialogResult.Yes)
            //        {
            //            cboVehiclesname.Focus();
            //            cboVehiclesname.SelectAll();
            //            break;
            //        }

            //        else
            //        {
            //            return;
            //        }

            //    }
            //}
            //string[] anyData;
            //anyData = new string[] { cboVehiclesname.SelectedValue.ToString(), cboVehiclesname.Text };
            //lvi2 = new ListViewItem(anyData);
            //lsvShow.Items.Add(lvi2);

            //btnseve.Enabled = true;
            //cboWorkname.Focus();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            frmshowSecular f = new frmshowSecular();

            f.ShowDialog();

            txtSecularID.Text = f.SecularID.ToString();
            txtPhone.Text = f.Phone.ToString();
            textname.Text = f.NameF.ToString();
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            //frmshowPersonnel f = new frmshowPersonnel();

            //f.ShowDialog();
            //textBoxID.Text = f.IDusername.ToString();
            //textBoxName.Text = f.nameaa.ToString();
        }

        private void ListView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void GroupBox5_Enter(object sender, EventArgs e)
        {

        }

        private void LsvShow_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Button6_Click(object sender, EventArgs e)
        {
            
           
        }

        private void DtpImportDate_EditValueChanged(object sender, EventArgs e)
        {
           
        }
        private void ShowData()
        {

            



        }

        private void TxtAddress_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void DtpImportDate_ValueChanged(object sender, EventArgs e)
        {
            
        }
        private void datedd()
        {
            string sql = "select UserID , Name from Personnels where Department='พระสงฆ์' AND UserID not in " +
               "(select kl.UserID from Kitnmons k " +
           "join Kitnimonlists kl ON(k.KitnmonID = kl.KitnmonID) " +
           "join Personnels p on (kl.UserID = p.UserID) " +
           "where kl.Dates = @datea )";
            SqlCommand com = new SqlCommand(sql, conn);
            Globalization.CultureInfo _cultureEnInfo = new Globalization.CultureInfo("en-US");
            DateTime dateEng = Convert.ToDateTime(this.dtpImportDate.Value, _cultureEnInfo);
            com.Parameters.AddWithValue("@datea", dateEng.ToString("yyyy/MM/dd", _cultureEnInfo));
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0];

           

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
           
            dataGridView1.Columns[0].HeaderCell.Value = "รหัส";
            dataGridView1.Columns[0].Width = 50;
            dataGridView1.Columns[1].HeaderCell.Value = "ชื่อสกุล";
        }
        private void DtpImportDate_ValueChanged_1(object sender, EventArgs e)
        {
            datedd();

        }

        private void Button6_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show(mtbS.Text+" "+mtben.Text);
            //string dateshowEng;
            //string dateshowTH;
            ////*** Thai Format
           
            //Globalization.CultureInfo _cultureTHInfo = new Globalization.CultureInfo("th-TH");        
            //DateTime dateThai = Convert.ToDateTime(this.dtpImportDate.Value, _cultureTHInfo);                  
            //dateshowTH = dateThai.ToString("dd MMM yyyy", _cultureTHInfo);
                       
            ////*** Eng Format
   
            //Globalization.CultureInfo _cultureEnInfo = new Globalization.CultureInfo("en-US");
                        
            //DateTime dateEng = Convert.ToDateTime(this.dtpImportDate.Value, _cultureEnInfo);
                        
            //dateshowEng = dateEng.ToString("dd MMM yyyy", _cultureEnInfo);

            //MessageBox.Show(dateshowTH + "||||||" + dateshowEng);
        }

        private void DtpImportDate_CloseUp(object sender, EventArgs e)
        {
           
           
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            if (cboWorkname.Text.Trim() == "")
            {
                cboWorkname.Focus(); return;
            }

            ListViewItem lvi2;
            int i = 0;
            string tmpProductID;
            for (i = 0; i <= lsvShow.Items.Count - 1; i++)
            {
                tmpProductID = (lsvShow.Items[i].SubItems[1].Text);

                if (cboVehiclesname.Text.Trim() == tmpProductID)
                {
                    if (MessageBox.Show("มีรายการซ้ำอยู่ ต้องการเพิ่มอีกหรือไม่", "ผิดพลาด") == DialogResult.Yes)
                    {
                        cboVehiclesname.Focus();
                        cboVehiclesname.SelectAll();
                        break;
                    }

                    else
                    {
                        return;
                    }

                }
            }
            string[] anyData;
            anyData = new string[] { cboVehiclesname.SelectedValue.ToString(), cboVehiclesname.Text };
            lvi2 = new ListViewItem(anyData);
            lsvShow.Items.Add(lvi2);

            btnseve.Enabled = true;
            cboWorkname.Focus();
        }

        private void DataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            var x = dataGridView1.CurrentRow.Cells;


            textBoxID.Text = x["UserID"].Value.ToString();

            textBoxName.Text = x["Name"].Value.ToString();
        }

        private void Button2_Click_1(object sender, EventArgs e)
        {
            if (cboWorkname.Text.Trim() == "")
            {
                cboWorkname.Focus(); return;
            }

            ListViewItem lvi2;
            int i = 0;
            string tmpProductID;
            for (i = 0; i <= lsvShow.Items.Count - 1; i++)
            {
                tmpProductID = (lsvShow.Items[i].SubItems[1].Text);

                if (cboVehiclesname.Text.Trim() == tmpProductID)
                {
                    if (MessageBox.Show("มีรายการซ้ำอยู่ ต้องการเพิ่มอีกหรือไม่", "ผิดพลาด") == DialogResult.Yes)
                    {
                        cboVehiclesname.Focus();
                        cboVehiclesname.SelectAll();
                        break;
                    }

                    else
                    {
                        return;
                    }

                }
            }
            string[] anyData;
            anyData = new string[] { cboVehiclesname.SelectedValue.ToString(), cboVehiclesname.Text };
            lvi2 = new ListViewItem(anyData);
            lsvShow.Items.Add(lvi2);

            btnseve.Enabled = true;
            cboWorkname.Focus();
        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
