﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Configuration;
using System.Xml;
namespace mork
{
    public partial class frmconfig : MetroFramework.Forms.MetroForm
    {
        public frmconfig()
        {
            InitializeComponent();
        }

        private void Frmconfig_Load(object sender, EventArgs e)
        {
            //string server = ConfigurationManager.AppSettings["server"].ToString();
            //string port = ConfigurationManager.AppSettings["port"].ToString();
            //string username = ConfigurationManager.AppSettings["username"].ToString();
            //string password = ConfigurationManager.AppSettings["password"].ToString();
            //string database = ConfigurationManager.AppSettings["database"].ToString();

            //txtIP.Text = server;
            //txtPort.Text = port;
            //txtuser.Text = username;
            //txtpass.Text = password;
            //txtname.Text = database;
            

        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            config.AppSettings.Settings["server"].Value = txtIP.Text.Trim();
            config.AppSettings.Settings["port"].Value = txtPort.Text.Trim();
            config.AppSettings.Settings["username"].Value = txtuser.Text.Trim();
            config.AppSettings.Settings["password"].Value = txtpass.Text.Trim();
            config.AppSettings.Settings["database"].Value = txtname.Text.Trim();
            config.Save(ConfigurationSaveMode.Modified,true);
            ConfigurationManager.RefreshSection("appSettings");
            //updateOriginal();
            MessageBox.Show("บันทึกค่าเริ่มต้นเรียบร้อยแล้ว","ผลการทำงาน");
            claerform();
            this.Close();
        }

        private void claerform()
        {
            txtIP.Text = "";
            txtPort.Text = "";
            txtname.Text = "";
            txtuser.Text = "";
            txtpass.Text = "";
        }

        private void updateOriginal()
        {
            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.Load(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

            foreach (XmlElement element in xmlDoc.DocumentElement)
            {
                if (element.Name.Equals("appSettings"))
                {
                    foreach (XmlNode node in element.ChildNodes)
                    {
                        if (node.Attributes[0].Value.Equals("server"))
                        {
                            node.Attributes[1].Value = txtIP.Text.Trim(); //"192.168.1.11";
                        }
                        if (node.Attributes[0].Value.Equals("port"))
                        {
                            node.Attributes[1].Value = txtPort.Text.Trim(); //"1433";
                        }
                        if (node.Attributes[0].Value.Equals("username"))
                        {
                            node.Attributes[1].Value = txtuser.Text.Trim();//"admin";
                        }
                        if (node.Attributes[0].Value.Equals("password"))
                        {
                            node.Attributes[1].Value = txtpass.Text.Trim();//"1234";
                        }
                        if (node.Attributes[0].Value.Equals("database"))
                        {
                            node.Attributes[1].Value = txtname.Text.Trim();//"monk2";
                        }
                    }
                }
            }

            xmlDoc.Save(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

            ConfigurationManager.RefreshSection("appSettings");
        }

        private void Panel1_Click(object sender, EventArgs e)
        {
            txtIP.Text = @"DESKTOP-GF7IG8J\SQLEXPRESS";
            txtname.Text = "monk2";
            //txtIP.Text = "192.168.1.11";
            //txtPort.Text = "1433";
            //txtname.Text = "monk2";
            //txtuser.Text = "admin";
            //txtpass.Text = "1234";
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
