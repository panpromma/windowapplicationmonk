﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraReports.UI;
namespace mork
{
    public partial class frmreponteq : MetroFramework.Forms.MetroForm
    {
        public frmreponteq()
        {
            InitializeComponent();
        }
        public int dID { get; set; }//รหัสกิจนิมนต์
        public string Address { get; set; }//ที่อยู่
        public string number { get; set; }//เวลา
        public string date { get; set; }//วันที่
        public string nameP { get; set; }//สถานะ
        public string nameS { get; set; }//เบอร์โทร
        SqlConnection conn;
        private void Frmreponteq_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();

            label1.Text = dID.ToString();

            label18.Text = Address.ToString();
           

            label3.Text = nameP.ToString();
            label8.Text = date.ToString();

            label4.Text = nameS.ToString();
            ShowData();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void ShowData()
        {
            string sql = "select DonateequipID , es.Equipmenname ,Number from DonateEMlists ds inner join Equipments es on ds.EquipmentID=es.EquipmentID  where ds.DonateequipID ='" + label1.Text + "'";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            dgvdatag.Columns[0].HeaderCell.Value = "รหัสการบริจาค";
            dgvdatag.Columns[1].HeaderCell.Value = "ชื่ออุปกรณ์";
            dgvdatag.Columns[2].HeaderCell.Value = "จำนวน";

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            DonatEQ f = new DonatEQ();
            f.ID = Convert.ToInt32(label1.Text);
         

            f.ShowPreviewDialog();
        }
    }
}
