﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


using System.Threading;

namespace mork
{
    public partial class frmmainlogin : Form

    {
        private bool bFullScreen;
        public frmmainlogin()
        {
            

            InitializeComponent();
            if (bFullScreen == false)
            {
                this.FormBorderStyle = FormBorderStyle.None;
                this.WindowState = FormWindowState.Maximized;
                bFullScreen = true;
            }
            else
            {
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.WindowState = FormWindowState.Maximized;//  .Maximized;
                bFullScreen = false;
            }
            //foreach (Control control in this.Controls)
            //{
            //    if (control is MdiClient)
            //    {
            //        control.BackgroundImage = System.Drawing.Image.FromFile(@"D:\mork\png\001.jpg");
            //        control.BackgroundImageLayout = ImageLayout.Stretch;
            //        //control.BackColor = Color.AliceBlue;
            //        //Properties.Resources.duk;
            //        //MessageBox.Show("MDI");
            //        break;
            //    }
            //}
        }

        

        private void CloseAllChildFrom()
        {
            foreach (Form L in this.MdiChildren)
            {
                L.Close();
            }
           

        }

       
        SqlConnection conn;
        private void Frmmainlogin_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            

           
            //this.Text = "ล็อคอินเด้อจ้า";
            EnableMenu(true, false, false, false, false,false);
            
        }

        
        public int UserID;
        public string name;
        public string username;
        public string password;
        public string Department;
        
        private void EnableMenu(bool login, bool Equipment , bool Kitnimon, bool admin, bool monk, bool godmonk)
        {
            //menuStrip1.Visible = login;
            //menuStrip2.Visible = Equipment;
            //menuStrip3.Visible = Kitnimon;
            //menuStrip4.Visible = admin;
            //menuStrip5.Visible = godmonk;
            //menuStrip6.Visible = monk;


        }

        private void LoginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            using (frmLogin f = new frmLogin())
            {
               
                f.ShowDialog();
                UserID = f.EmpID;
                name = f.Fritsname;
                //Lastname = f.Lastname;
                Department = f.Position;
                //Username = f.Username;

                this.Text = name + "  " + Department;

                //label1.Text = name.ToString();
                if (Department == "ฝ่ายกิจนิมนต์")
                {
                    EnableMenu(false, true, false, false, false, false);

                    
                }
                else if (Department == "ฝ่ายอุปกรณ์")
                {
                    EnableMenu(false, false, true, false, false, false);
                    
                }
                else if (Department == "ผู้ดูแลระบบ")
                {
                    EnableMenu(false, false, false, true, false, false);
                    
                }
                else if (Department == "เจ้าอาวาส")
                {
                    EnableMenu(false, false, false, false, false, true); 
                   
                }
                else if (Department == "พระสงฆ์")
                {
                    EnableMenu(false, false, false, false, true, false);
                   
                }

            }
        }
        private void LogOut() //สร้าง method ออกจากระบบ
        {
            if (MessageBox.Show("!!!!คณุต้องการจะออกจากรบบไหม?","แจ้งเตือน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                UserID = 0;
                name = "";
                Department = "";
                this.Text = "ระบบจัดการกิจนิมนต์";
                foreach (var item in this.MdiChildren) //สั่งปิดฟอร์มลูกที่เปิดอยู่
                {
                    item.Close();
                }
                EnableMenu(true, false, false, false, false, false);
                
            }
        }

        

        private void LogOutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            LogOut();
        }

        private void LogOutToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            LogOut();
        }

        private void LogOutToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            LogOut();
        }

        private void LogOutToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            LogOut();
        }

        private void LogOutToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            LogOut();
        }

        private void จดการขอมลผใชงานToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmReport f = new frmReport();
            f.MdiParent = this;
            f.Show();
        }
        
        private void จดการอปกรณToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmEquipment f = new frmEquipment();
            f.MdiParent = this;
            f.Show();
        }

        private void การรบกจนมนตToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmKitnimon f = new frmKitnimon();
         
                f.MdiParent = this;
                f.Show();
            
            
        }

        private void กจนมนตToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void LogOutToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void การขอใชสถานทToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmPlacerequest f = new frmPlacerequest();
            f.MdiParent = this;
            f.Show();
        }

        private void จดการประเภทกจนมนตToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
           frmCategorykitnimon f = new frmCategorykitnimon();
            f.MdiParent = this;
            f.Show();
        }

        private void จดการยานพาหนะToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmVehicle f = new frmVehicle();
                f.MdiParent = this;
            f.Show();
        }

        private void จดการสถานทToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmPlace f = new frmPlace();
            f.MdiParent = this;
            f.Show();
        }

       

       

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        
        
        private void PictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void BarraTitulo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void รายการรออนมตToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmapprove f = new frmapprove();
            f.MdiParent = this;
            f.Show();
        }

        private void อนมตการขอใชสถานทToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmapplace f = new frmapplace();
            f.MdiParent = this;
            f.Show();
        }

        private void การยมอปกรณToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmborrow f = new frmborrow();
            f.MdiParent = this;
            f.Show();
        }

        private void จดการขอมลประเภทอปกรณToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmCategoryequipments f = new frmCategoryequipments();
            f.MdiParent = this;
            f.Show();
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void กจทอนมตแลวToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowkitnimon f = new frmshowkitnimon();
            f.MdiParent = this;
            f.Show();
        }

        private void ABOUTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            frmshowPersonal f = new frmshowPersonal();
            
            f.Show();
        }

        private void กจนมตToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowkitnimon f= new frmshowkitnimon();
            f.MdiParent = this;
            f.Show();
        }

        private void อปกรณToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowOkPlacerequest f = new frmshowOkPlacerequest();
            f.MdiParent = this;
            f.Show();
            
        }

        private void พระสงตToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowmonk f = new frmshowmonk();
            f.MdiParent = this;
            f.Show();
        }

        private void ToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowBorrow f = new frmshowBorrow();
            f.MdiParent = this;
            f.Show();
        }

        private void อปกรณToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowOkBorrow f = new frmshowOkBorrow();
            f.MdiParent = this;
            f.Show();
        }

        private void ดรายการกจนมนตToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmkitnimonmonk f = new frmkitnimonmonk();
            f.MdiParent = this;
            f.Show();
            
        }

        private void MenuStrip4_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void ToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowOkPlacerequest f = new frmshowOkPlacerequest();
            f.MdiParent = this;
            f.Show();
        }

        private void ToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            frmshowOkBorrow f = new frmshowOkBorrow();
            f.MdiParent = this;
            f.Show();
        }

        private void ดรายการทอนมตแลวToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void Iconminimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void TxtCloseProgram_Click(object sender, EventArgs e)
        {

        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("ต้องการปิดโปรแกรมหรือไม่", "แจ้งเตือน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void MenuStrip3_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void MenuStrip2_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
