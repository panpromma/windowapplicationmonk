﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraReports.UI;
namespace mork
{
    public partial class frmEquipmantReturn : MetroFramework.Forms.MetroForm
    {
        
        SqlConnection conn;
       
        public frmEquipmantReturn()
        {
            InitializeComponent();
            
        }

        private void FrmEquipmantReturn_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            
            ShowData();
        }

        private void ShowData()
        {
            string sql = "select es.OrderKE ,e.EquipmentID, e.Equipmenname, es.BorrowID, es.Number , dbo.mydate(es.Date) , es.damaged , es.lost,s.name  from Equipmentlists es  INNER JOIN  Equipments e on es.EquipmentID=e.EquipmentID inner join Borrows b on es.BorrowID=b.BorrowID inner join Seculars s on b.SecularID=s.SecularID  where damaged is null or lost is null";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvdatag.Columns[0].HeaderCell.Value = "ลำดับที่";
            dgvdatag.Columns[1].HeaderCell.Value = "รหัสอุปกรณ์";
            dgvdatag.Columns[2].HeaderCell.Value = "ชื่ออุปกรณ์";
            dgvdatag.Columns[3].HeaderCell.Value = "รหัสการยืม";
            dgvdatag.Columns[4].HeaderCell.Value = "จำนวน";
            dgvdatag.Columns[5].HeaderCell.Value = "วันที่";
            dgvdatag.Columns[6].HeaderCell.Value = "ชำรุด";
             dgvdatag.Columns[7].HeaderCell.Value = "สูญหาย";
            dgvdatag.Columns[8].HeaderCell.Value = "ชื่อฆรวาส";


        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Dgvdatag_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {

            var x = dgvdatag.CurrentRow.Cells;



            txtOrderKE.Text = x["OrderKE"].Value.ToString();
            txtBorrowID.Text = x["BorrowID"].Value.ToString();
            txtNumber.Text = x["Number"].Value.ToString();
            txtdamaged.Text = x["damaged"].Value.ToString();
            txtlost.Text = x["lost"].Value.ToString();
            textBox1.Text = x["EquipmentID"].Value.ToString();
            textBox2.Text = x["Equipmenname"].Value.ToString();
            
        }

        private void Btnmodify_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("!!!!คณุต้องการจะแก้ไขไหม?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {


                string sql = "update Equipmentlists set    Number=@Number  , damaged=@damaged , lost=@lost  where OrderKE=@OrderKE and BorrowID=@BorrowID   ";
                SqlCommand comm = new SqlCommand(sql, conn);
                comm.Parameters
                    .AddWithValue("@OrderKE", txtOrderKE.Text.Trim());

                comm.Parameters
                    .AddWithValue("@BorrowID", txtBorrowID.Text.Trim());

                comm.Parameters
                    .AddWithValue("@Number", txtNumber.Text.Trim());

                comm.Parameters
                    .AddWithValue("@damaged", txtdamaged.Text.Trim());

                comm.Parameters
                   .AddWithValue("@lost", txtlost.Text.Trim());


                string sqlUpdateStock = " update Equipments set  Numbertreasury = Numbertreasury + @Qty " +
                                           " where EquipmentID like @EquipmentID ";
                SqlCommand comm1 = new SqlCommand(sqlUpdateStock, conn);
                comm1.Parameters.Add("@Qty", SqlDbType.Int).Value = txtNumber.Text;
                comm1.Parameters.Add("@EquipmentID", SqlDbType.Int).Value = textBox1.Text;

                string sqlUpdateStock1 = " update Equipments set  Numberdamaged = Numberdamaged + @dam " +
                                            " where EquipmentID like @EquipmentID ";
                SqlCommand comm2 = new SqlCommand(sqlUpdateStock1, conn);
                comm2.Parameters.Add("@dam", SqlDbType.Int).Value = txtdamaged.Text;
                comm2.Parameters.Add("@EquipmentID", SqlDbType.Int).Value = textBox1.Text;

                string sqlUpdateStock2 = " update Equipments set  Numberlost = Numberlost + @lost " +
                                            " where EquipmentID like @EquipmentID ";
                SqlCommand comm3 = new SqlCommand(sqlUpdateStock2, conn);
                comm3.Parameters.Add("@lost", SqlDbType.Int).Value = txtlost.Text;
                comm3.Parameters.Add("@EquipmentID", SqlDbType.Int).Value = textBox1.Text;


                try
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                    comm1.ExecuteNonQuery();
                    comm2.ExecuteNonQuery();
                    comm3.ExecuteNonQuery();
                    conn.Close();
                    clearfrom();
                    ShowData();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void clearfrom()
        {
            txtOrderKE.Text = "";
            txtBorrowID.Text = "";
            txtdamaged.Text = "";
            txtNumber.Text = "";
            txtlost.Text = "";
            textBox1.Text = "";
            textBox2.Text = "";
        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            ShowData1(txtID.Text);
        }
        private void ShowData1(string text)
        {
            string sql = "select es.OrderKE ,e.EquipmentID, e.Equipmenname, es.BorrowID, es.Number , dbo.mydate(es.Date) , es.damaged , es.lost,s.name  from Equipmentlists es  INNER JOIN  Equipments e on es.EquipmentID=e.EquipmentID inner join Borrows b on es.BorrowID=b.BorrowID inner join Seculars s on b.SecularID=s.SecularID " +
                " where name like '%' + @strName + '%'" +
                "  and damaged is null   ";
            SqlCommand com = new SqlCommand(sql, conn);
            com.Parameters.AddWithValue("@strName", text);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvdatag.Columns[0].HeaderCell.Value = "ลำดับที่";
            dgvdatag.Columns[1].HeaderCell.Value = "รหัสอุปกรณ์";
            dgvdatag.Columns[2].HeaderCell.Value = "ชื่ออุปกรณ์";
            dgvdatag.Columns[3].HeaderCell.Value = "รหัสการยืม";
            dgvdatag.Columns[4].HeaderCell.Value = "จำนวน";
            dgvdatag.Columns[5].HeaderCell.Value = "วันที่";
            dgvdatag.Columns[6].HeaderCell.Value = "ชำรุด";
            dgvdatag.Columns[7].HeaderCell.Value = "สูญหาย";
            dgvdatag.Columns[8].HeaderCell.Value = "ชื่อฆรวาส";
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            xrEquipmantReturn f = new xrEquipmantReturn();

            f.ShowPreviewDialog();
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            frmEq f = new frmEq();
            f.ShowDialog();

        }
    }
}
