﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
namespace mork
{
    public partial class frmCategoryequipments : MetroFramework.Forms.MetroForm
    {
       
        public frmCategoryequipments()
        {
            InitializeComponent();
            
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        SqlConnection conn;
        private void Categoryequipments_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            ShowData();
        }
        private void ShowData()
        {
            string sql = "select * from Categoryequipments";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
            dgvdatag.Columns[1].HeaderCell.Value = "ประเภท";
        }

        private void Add_Click(object sender, EventArgs e)
        {
            if (txtAddress.Text.Trim() == "" )
            {
                MessageBox.Show("กรุณาป้อนข้อมูล", "ผิดพลาด");


            }
            else
            {

           
            string sql = "insert into Categoryequipments values( @Description)";
            SqlCommand comm = new SqlCommand(sql, conn);





            

            comm.Parameters
                .AddWithValue("@Description", txtAddress.Text.Trim());







            if (conn.State == ConnectionState.Closed) conn.Open();
            comm.ExecuteNonQuery();
            conn.Close();
            clearfrom();
            ShowData();
            }
        }

        private void clearfrom()
        {
            TextID.Text = "";
            txtAddress.Text = "";
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("!!!!คณุต้องการจะลบใช่ไหม?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {

                string sql = "delete from Categoryequipments where  CategorymID = @CategorymID";
                SqlCommand comm = new SqlCommand(sql, conn);



                comm.Parameters

                    .AddWithValue("@CategorymID", TextID.Text.Trim());

                try
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                    clearfrom();
                    ShowData();

                }
                catch 
                {
                    MessageBox.Show("ไม่สามารถลบข้มูลได้", "Erorr");

                    return;
                }
            }
        }

        private void Modify_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("!!!!คณุต้องการจะแก้ไขไหม?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {


                string sql = "update Categoryequipments set   Description=@Description   where  CategorymID=@CategorymID";
                SqlCommand comm = new SqlCommand(sql, conn);


                comm.Parameters
                    .AddWithValue("@CategorymID", TextID.Text.Trim());

                

                comm.Parameters
                    .AddWithValue("@Description", txtAddress.Text.Trim());


                try
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                    clearfrom();
                    ShowData();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Datag_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            var x = dgvdatag.CurrentRow.Cells;


            TextID.Text = x["CategorymID"].Value.ToString();
       
            txtAddress.Text = x["Description"].Value.ToString();
        }

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void BarraTitulo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Label10_Click(object sender, EventArgs e)
        {

        }

        private void BtnClears_Click(object sender, EventArgs e)
        {
            clearfrom();
        }
    }
}
