﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
namespace mork
{
    public partial class frmPersonnel : Form
    {
       
        public frmPersonnel()
        {
            InitializeComponent();
        }
        public string status { get; set; }
        public int UserID { get; set; }
        public string Namef { get; set; }
        public string Department { get; set; }
        public string Phone { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int age { get; set; }
        public string Address { get; set; }
        public string Prefix { get; set; }
        SqlConnection conn;
        
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        public string ff { get; set; }
        private void Personnel_Load(object sender, EventArgs e)
        {
            setcbo();
            if (status == "เพิ่ม")
            {
                clearfrom();
            }
            else if (status == "แก้ไข")
            {
                setDataForUpdate(); //ใส่ข้อมูลจาก property ไปที่ฟอร์ม
            }
            conn = new ConnecDB().SqlStrCon();
            conn.Open();


        }
        private void setDataForUpdate()   //เซ็ตค่าสำหรับแก้ไข
        {
            TextID.Enabled = false;
            TextID.Text = this.UserID.ToString();
            txtNameF.Text = this.Namef;
            txtPrefix.Text = this.Prefix;
            txtAddress.Text = this.Address;
            txtAge.Text = this.age.ToString();
            txtUsernametext.Text = this.Username;
            txtPasswordtext.Text = this.Password;
            txtPhone.Text = this.Phone;



            cmbDepartment.SelectedItem = this.Department;

        }
        //combobox
        private void setcbo()
        {
            
            string[] Department = new string[]{ "","ฝ่ายกิจนิมนต์","ฝ่ายอุปกรณ์", "ผู้ดูแลระบบ", "เจ้าอาวาส", "พระสงฆ์", };

            
            cmbDepartment.Items.AddRange(Department);
        }

        

        

        
        //private bool checkData() //ถ้า ตรวจสอบผ่าน return true --ไม่ผ่าน เป็น false
        //{
           
        //    string sql = "select count(*) from Personnels where UserID = @UserID";
        //    if (status == "แก้ไข")
        //    {
        //        sql += " and UserID !=@UserID";   //ถ้าแก้ไขให้เชคตัวอื่นยกเว้นตัวเอง
        //    }
        //    SqlCommand comm = new SqlCommand(sql, conn);
        //    comm.Parameters.AddWithValue("@Username", txtUsernametext.Text);
        //    comm.Parameters.AddWithValue("@UserID", UserID);
        //    int result = Convert.ToInt16(comm.ExecuteScalar());
        //    if (result > 0)
        //    {
        //        MessageBox.Show("มีชื่อผู้ใช้ซ้ำในระบบ" + result.ToString(), "Error");
        //        return false;
        //    }
        //    return true;
        //}
        private void Add_Click(object sender, EventArgs e)
        {
            if (txtNameF.Text.Trim() == "" || txtPrefix.Text.Trim() == "" || txtAge.Text.Trim() == "" || txtPhone.Text.Trim() == "" || txtUsernametext.Text.Trim() == "" || txtPasswordtext.Text.Trim() == "" || txtAddress.Text.Trim() == "")
            {
                MessageBox.Show("กรุณาป้อนข้อมูล", "ผิดพลาด");
                
                txtNameF.Focus();
                return;
            }
            
            if (status == "เพิ่ม")
            {
                insertdata();
                
            }
            else if (status == "แก้ไข")
            {
                //MessageBox.Show(status);
                updatedata();
                
            }
            this.Close();

        }

        private void updatedata()
        {
            string sql = "update Personnels set  name=@name, Department=@Department, Phone=@Phone, Username=@Username, Password=@Password, Age=@Age,  Address=@Address,Prefix=@Prefix where  userID=@userID  ";
            SqlCommand comm = new SqlCommand(sql, conn);


            comm.Parameters
                .AddWithValue("@userID", TextID.Text.Trim());
            comm.Parameters
                .AddWithValue("@name", txtNameF.Text.Trim());
            comm.Parameters
                .AddWithValue("@Department", cmbDepartment.Text.Trim());
            comm.Parameters
                .AddWithValue("@Phone", txtPhone.Text.Trim());
            comm.Parameters
               .AddWithValue("@Username", txtUsernametext.Text.Trim());
            comm.Parameters
               .AddWithValue("@Password", txtPasswordtext.Text.Trim());
            comm.Parameters
               .AddWithValue("@Age", txtAge.Text.Trim());
            comm.Parameters
               .AddWithValue("@Address", txtAddress.Text.Trim());
            comm.Parameters
              .AddWithValue("@Prefix", txtPrefix.Text.Trim());
            comm.ExecuteNonQuery();
        }

        private void insertdata()
        {
            

           
             string sql = "Insert into Personnels( Name , Department , Phone , Username , Password , age , Address , Prefix )"
                        + " values(@Name , @Department , @Phone , @Username , @Password , @age , @Address , @Prefix )";
            SqlCommand comm = new SqlCommand(sql, conn);
            comm.Parameters.AddWithValue("@Name", txtNameF.Text.Trim());
            comm.Parameters.AddWithValue("@Department", cmbDepartment.Text.Trim());
            comm.Parameters.AddWithValue("@Phone", txtPhone.Text.Trim());
            comm.Parameters.AddWithValue("@Username", txtUsernametext.Text.Trim());
            comm.Parameters.AddWithValue("@Password", txtPasswordtext.Text.Trim());
            comm.Parameters.AddWithValue("@age", txtAge.Text.Trim());
            comm.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
            comm.Parameters.AddWithValue("@Prefix", txtPrefix.Text.Trim());
            comm.ExecuteNonQuery();
           

        }

        private void Clears_Click(object sender, EventArgs e)
        {
            clearfrom();
        }
        private void clearfrom()
        {
            TextID.Text = "";
            txtPrefix.Text = "";
            txtNameF.Text = "";
            cmbDepartment.Text = "";
            txtAge.Text = "";
            txtPhone.Text = "";
          
            txtUsernametext.Text = "";
            txtPasswordtext.Text = "";
            txtAddress.Text = "";
        }

        private void Modify_Click(object sender, EventArgs e)
        {
            if (txtNameF.Text.Trim() == ""||txtPrefix.Text.Trim() == "" || txtAge.Text.Trim() == "" || txtPhone.Text.Trim() == "" || txtUsernametext.Text.Trim() == "" || txtPasswordtext.Text.Trim() == "" || txtAddress.Text.Trim() == "")
            {
                MessageBox.Show("กรุณาเลือกข้อมูลที่จะแก้ไข", "ผิดพลาด");

               
            }
            //else
            //{


            //    





            //    try
            //    {
            //        conn.Open();
            //        comm.ExecuteNonQuery();
            //        conn.Close();
            //        clearfrom();
                    

            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }
            //}
        }

        private void Delete_Click(object sender, EventArgs e)
        {
           
        }

        private void CmbPrefix_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void Dgvdata_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            
        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
           
        }
        

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void Dgvdata_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                txtPasswordtext.UseSystemPasswordChar = true;
            }
        }

        private void TxtAddress_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label9_Click(object sender, EventArgs e)
        {

        }

        private void TxtNameF_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtPasswordtext_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
