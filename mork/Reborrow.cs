﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Linq;
namespace mork
{
    public partial class Reborrow : DevExpress.XtraReports.UI.XtraReport
    {
        public Reborrow()
        {
            InitializeComponent();
        }
        DataClasses1DataContext db;
        public int ID;
        int number;
        public string RedateB { get; set; }
        public string Redate { get; set; }
       
        private void Reborrow_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            db = new DataClasses1DataContext();
           
            var v = from k in db.Borrows
                    join p in db.Personnels on k.UserID equals p.UserID
                    join s in db.Seculars on k.SecularID equals s.SecularID
                    join bs in db.Equipmentlists on k.BorrowID equals bs.BorrowID
                    join es in db.Equipments on bs.EquipmentID equals es.EquipmentID

                    where k.BorrowID == ID
                    //orderby f.Name
                    select new
                    {
                        k.BorrowID,
                        Idate=String.Format("{0:dd MMMM yyy}",k.Redate),
                        Bdate = String.Format("{0:dd MMMM yyy}", k.RedateB),
                        k.Time,
                        k.Retundate,
                        p.Name,
                        s.name,
                        bs.Number,
                        es.Equipmenname,
                        es.EquipmentID
                        


                    };

            this.DataSource = v;
        }

        private void ReportHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel2.Text = Convert.ToString(this.GetCurrentColumnValue("BorrowID"));
            xrLabel12.Text = Convert.ToString(this.GetCurrentColumnValue("Name"));
            xrLabel14.Text = Convert.ToString(this.GetCurrentColumnValue("name"));
            xrLabel4.Text = Convert.ToString(this.GetCurrentColumnValue("Idate"));
            xrLabel8.Text = Convert.ToString(this.GetCurrentColumnValue("Bdate"));
            xrLabel10.Text = Convert.ToString(this.GetCurrentColumnValue("Time"));
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            number += 1;
            xrLabel19.Text = Convert.ToString(number);
            xrLabel20.Text = Convert.ToString(this.GetCurrentColumnValue("EquipmentID"));
            xrLabel21.Text = Convert.ToString(this.GetCurrentColumnValue("Equipmenname"));
            xrLabel22.Text = Convert.ToString(this.GetCurrentColumnValue("Number"));

        }

        private void ReportFooter_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel25.Text = Convert.ToString(this.GetCurrentColumnValue("Name"));
            xrLabel26.Text = Convert.ToString(this.GetCurrentColumnValue("name"));
        }
    }
}
