﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
namespace mork
{
    public partial class frmaddplace1 : MetroFramework.Forms.MetroForm
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        public int RequestID { get; set; }//รหัสกิจนิมนต์
        public string Date { get; set; }//ที่อยู่
        public string Time { get; set; }//เวลา
        public string Description { get; set; }//วันที่
        public string Phone { get; set; }//สถานะ
        public string PlaceName { get; set; }//เบอร์โทร
        public string Namep { get; set; }//ชื่อผู้รับ
        public string names { get; set; }//ชื่อฆราวาส
        SqlConnection conn;

        public frmaddplace1()
        {
            InitializeComponent();
        }

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Frmaddplace1_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            label1.Text = RequestID.ToString();
            label18.Text = Date.ToString();
            label7.Text = Time.ToString();
            label8.Text = Description.ToString();
            label6.Text = Phone.ToString();
            label3.Text = PlaceName.ToString();
            label4.Text = Namep.ToString();
            label5.Text = names.ToString();
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btnapprove_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("!!!!คณุต้องการจะอนุมัติหรือไม่?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {


                string sql = "update Placerequests set Approve=@Approve  where  RequestID=@RequestID  ";
                SqlCommand comm = new SqlCommand(sql, conn);

                comm.Parameters
                   .AddWithValue("@RequestID", label1.Text.Trim());

                comm.Parameters
                    .AddWithValue("@Approve", "1");



                try
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();


                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                return;
            }
            MessageBox.Show("อนุมัติเสร็จสิ้น");

            this.Close();
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Label9_Click(object sender, EventArgs e)
        {

        }
    }
}
