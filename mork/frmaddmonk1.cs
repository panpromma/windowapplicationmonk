﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
namespace mork
{
    public partial class frmaddmonk1 : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
      
        public string status { get; set; }
        public int UserID { get; set; }
        public string Namef { get; set; }
        public string Department { get; set; }
        public string Phone { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int age { get; set; }
        public string Address { get; set; }
        public string Prefix { get; set; }
        SqlConnection conn;
        public frmaddmonk1()
        {
            InitializeComponent();
        }

        private void BtnClears_Click(object sender, EventArgs e)
        {
            clearfrom();
        }
        private void clearfrom()
        {
            TextID.Text = "";
            txtPrefix.Text = "";
            txtNameF.Text = "";
            
            txtAge.Text = "";
            txtPhone.Text = "";

            txtUsernametext.Text = "";
            txtPasswordtext.Text = "";
            txtAddress.Text = "";
        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
            if (txtNameF.Text.Trim() == "" || txtPrefix.Text.Trim() == "" || txtAge.Text.Trim() == "" || txtPhone.Text.Trim() == "" || txtUsernametext.Text.Trim() == "" || txtPasswordtext.Text.Trim() == "" || txtAddress.Text.Trim() == "")
            {
                MessageBox.Show("กรุณาป้อนข้อมูล", "ผิดพลาด");

                txtNameF.Focus();
                return;
            }

            if (status == "เพิ่ม")
            {
                insertdata();
            }
            else if (status == "แก้ไข")
            {
                //MessageBox.Show(status);
                updatedata();
            }
            this.Close();
        }

        private void Frmaddmonk1_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            if (status == "เพิ่ม")
            {
                clearfrom();
            }
            else if (status == "แก้ไข")
            {
                setDataForUpdate(); //ใส่ข้อมูลจาก property ไปที่ฟอร์ม
            }
            conn = new ConnecDB().SqlStrCon();
            conn.Open();

        }
        private void setDataForUpdate()   //เซ็ตค่าสำหรับแก้ไข
        {
            TextID.Enabled = false;
            TextID.Text = this.UserID.ToString();
            txtNameF.Text = this.Namef;
            txtPrefix.Text = this.Prefix;
            txtAddress.Text = this.Address;
            txtAge.Text = this.age.ToString();
            txtUsernametext.Text = this.Username;
            txtPasswordtext.Text = this.Password;
            txtPhone.Text = this.Phone;



            

        }
        private void updatedata()
        {
            string sql = "update Personnels set  name=@name, Department=@Department, Phone=@Phone, Username=@Username, Password=@Password, Age=@Age,  Address=@Address,Prefix=@Prefix where  userID=@userID  ";
            SqlCommand comm = new SqlCommand(sql, conn);


            comm.Parameters
                .AddWithValue("@userID", TextID.Text.Trim());
            comm.Parameters
                .AddWithValue("@name", txtNameF.Text.Trim());
            comm.Parameters
                .AddWithValue("@Department", "พระสงฆ์");
            comm.Parameters
                .AddWithValue("@Phone", txtPhone.Text.Trim());
            comm.Parameters
               .AddWithValue("@Username", txtUsernametext.Text.Trim());
            comm.Parameters
               .AddWithValue("@Password", txtPasswordtext.Text.Trim());
            comm.Parameters
               .AddWithValue("@Age", txtAge.Text.Trim());
            comm.Parameters
               .AddWithValue("@Address", txtAddress.Text.Trim());
            comm.Parameters
              .AddWithValue("@Prefix", txtPrefix.Text.Trim());
            comm.ExecuteNonQuery();
        }

        private void insertdata()
        {
            try
            {
                string sql = "Insert into Personnels( Name , Department , Phone , Username , Password , age , Address , Prefix )"
                            + " values(@Name , @Department , @Phone , @Username , @Password , @age , @Address , @Prefix )";
                SqlCommand comm = new SqlCommand(sql, conn);
                comm.Parameters.AddWithValue("@Name", txtNameF.Text.Trim());
                comm.Parameters.AddWithValue("@Department", "พระสงฆ์");
                comm.Parameters.AddWithValue("@Phone", txtPhone.Text.Trim());
                comm.Parameters.AddWithValue("@Username", txtUsernametext.Text.Trim());
                comm.Parameters.AddWithValue("@Password", txtPasswordtext.Text.Trim());
                comm.Parameters.AddWithValue("@age", txtAge.Text.Trim());
                comm.Parameters.AddWithValue("@Address", txtAddress.Text.Trim());
                comm.Parameters.AddWithValue("@Prefix", txtPrefix.Text.Trim());
                comm.ExecuteNonQuery();
            }

            catch
            {
                MessageBox.Show("ชื่อผู้ใช้ซ้ำโปรใส่ข้อมูลใหม่");
                return;
                
            }
        }

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
    }
}
