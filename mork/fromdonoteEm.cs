﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace mork
{
    public partial class fromdonoteEm : MetroFramework.Forms.MetroForm
    {
        public fromdonoteEm()
        {
            InitializeComponent();
        }
        SqlConnection conn;
        public int dID { get; set; }//รหัสกิจนิมนต์
        public string Address { get; set; }//ที่อยู่
        public string number { get; set; }//เวลา
        public string date { get; set; }//วันที่
        public string nameP { get; set; }//สถานะ
        public string nameS { get; set; }//เบอร์โทร
        private void FromdonoteEm_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            ShowData();
        }
        private void ShowData()
        {
            string sql = " select DonateequipID as รหัส,dbo.mydate(datedonat) as วันที่,ds.description as รายละเอียด ,donor as ชื่อผู้บริจาค,ps.Name as ชื่อผู้รับ from Donateequipments ds inner join Personnels ps on ds.UserID=ps.UserID";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Dgvdatag_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var x = dgvdatag.CurrentRow.Cells;
            dID = Convert.ToInt16(x[0].Value.ToString());
            nameS = x[3].Value.ToString();
            date = x[1].Value.ToString();
            nameP = x[4].Value.ToString();
            Address = x[2].Value.ToString();

            frmreponteq f = new frmreponteq();
            f.dID = this.dID;
            f.nameS = this.nameS;
            
            f.date = this.date;

            f.nameP = this.nameP;
            f.Address = this.Address;

            f.ShowDialog();
            ShowData();
        }
    }
}
