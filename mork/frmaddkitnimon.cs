﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraReports.UI;
namespace mork
{
    public partial class frmaddkitnimon : MetroFramework.Forms.MetroForm
    {
        public frmaddkitnimon()
        {
            InitializeComponent();
        }
        
        SqlConnection conn;
        public string status { get; set; }
        public int KitnmonID { get; set; }//รหัสกิจนิมนต์
        public string Address { get; set; }//ที่อยู่
        public string Time { get; set; }//เวลา
        public string date { get; set; }//วันที่
        public string Approve { get; set; }//สถานะ
        public string Phone { get; set; }//เบอร์โทร
        public string Namep { get; set; }//ชื่อผู้รับ
        public string names { get; set; }//ชื่อฆราวาส
        public string WorkName { get; set; }//ชื่อ
        public int ID { get; set; }//รหัสกิจนิมนต์
        public string mon { get; set; }
        public string Time2 { get; set; }//เวลา
        public string namePP;
        private void Frmaddkitnimon_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            if (status == "แสดง")
            {
                btnapprove.Visible=false;
                button2.Visible = false;

            }
            else
            {
                btnapprove.Visible = true;
                button2.Visible = true;
            }

            label1.Text = KitnmonID.ToString();
            label2.Text = Address.ToString();
            label3.Text = Time.ToString();
            label4.Text = date;
            label5.Text = Approve.ToString();
            label6.Text = Phone.ToString();
            label7.Text = Namep.ToString();
            label8.Text = names.ToString();
            label18.Text = WorkName.ToString();
            ShowData();
        }

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();

          


        }
       
            
       
        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            
        }

        private void Btnapprove_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("!!!!คณุต้องการจะอนุมัติหรือไม่?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {


                string sql = "update Kitnmons set Approve=@Approve  where  KitnmonID=@KitnmonID  ";
                SqlCommand comm = new SqlCommand(sql, conn);

                comm.Parameters
                   .AddWithValue("@KitnmonID", label1.Text.Trim());

                comm.Parameters
                    .AddWithValue("@Approve", "1");



                try
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                   

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                return;
            }
            MessageBox.Show("อนุมัติเสร็จสิ้น");
            
            this.Close();
            
        }
        
        private void Button1_Click(object sender, EventArgs e)
        {
            XtraReport11 f = new XtraReport11();
            f.ID = Convert.ToInt32(label1.Text);
            f.mon= Convert.ToString(label5.Text);
            f.Time2 = Convert.ToString(label4.Text);
            f.namePP = Convert.ToString(label8.Text);
          

        f.ShowPreviewDialog();
        }
        private void ShowData()
        {
            string sql = "select  ks.UserID, p.Name,cs.WorkName ,ks.Dates ,ks.TimeS as เวลาเริ่ม,ks.TimeEN as เวลาสิ้นสุด from Kitnmons k  INNER JOIN Kitnimonlists ks on k.KitnmonID = ks.KitnmonID INNER JOIN Personnels p on ks.UserID = p.UserID INNER JOIN CategoryKitnimons cs on k.CategoryKitID = cs.CategoryKitID where ks.KitnmonID ='" + label1.Text + "'";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
            dgvdatag.Columns[1].HeaderCell.Value = "ชื่อพระสงฆ์";
            dgvdatag.Columns[2].HeaderCell.Value = "ชื่องาน";
            dgvdatag.Columns[3].HeaderCell.Value = "วันที่เริ่มงาน";

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BarraTitulo_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
