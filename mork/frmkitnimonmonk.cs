﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
namespace mork
{
    public partial class frmkitnimonmonk : MetroFramework.Forms.MetroForm
    {
        public string WorkName { get; set; }//ชื่อ
        public string status { get; set; }
        public int KitnmonID { get; set; }//รหัสกิจนิมนต์
        public string Time { get; set; }//เวลา
        public string date { get; set; }//วันที่
        public frmkitnimonmonk()
        {
            InitializeComponent();
            
        }
        SqlConnection conn;
     
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        public int UserID;
        public string name;
        private void Frmkitnimonmonk_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            string id = frmLogin.eID.ToString();



            string name = frmLogin.name;
            lblID.Text = id;
            lblName.Text = name;
            ShowData();
        }
        private void ShowData()
        {
            
                string sql = "select DISTINCT k.KitnmonID  ,cs.WorkName , ks.TimeD , dbo.mydate(ks.dateD) ,case  when ks.Approve = 0 then 'ยังไม่อนุมัติ' when ks.Approve = 1 then 'อนุมัติแล้ว' end  from Kitnimonlists k INNER JOIN  Personnels p on  k.UserID= p.UserID INNER JOIN  Kitnmons ks on k.KitnmonID = ks.KitnmonID INNER JOIN  CategoryKitnimons cs on  ks.CategoryKitID = cs.CategoryKitID where  p.UserID='" + lblID.Text + "'" +
                " order by KitnmonID desc";
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvdatag.DataSource = dt;
            dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
            dgvdatag.Columns[1].HeaderCell.Value = "ชื่องงาน";
            dgvdatag.Columns[2].HeaderCell.Value = "เวลา";
            dgvdatag.Columns[3].HeaderCell.Value = "วันที่"; 
                dgvdatag.Columns[4].HeaderCell.Value = "สถานะ";


        }

        


        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Txts_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void Dgvdatag_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Dgvdatag_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var x = dgvdatag.CurrentRow.Cells;
            KitnmonID = Convert.ToInt16(x[0].Value.ToString());
           
            Time = x[2].Value.ToString();
            date = x[3].Value.ToString();
            
            WorkName = x[1].Value.ToString();




            frmshowkit2 f = new frmshowkit2();
            f.KitnmonID = this.KitnmonID;
          
            f.Time = this.Time;
            f.date = this.date;
       
            f.WorkName = this.WorkName;
            f.status = "แสดง";
            f.ShowDialog();
            ShowData();
        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            ShowData(txtID.Text.ToString());
        }
        private void ShowData(string name)
        {
            if (txtID.Text != "")
            {
                string sql = "select k.KitnmonID  ,cs.WorkName , ks.Time , dbo.mydate(ks.date) from Kitnimonlists k INNER JOIN  Personnels p on  k.UserID= p.UserID INNER JOIN  Kitnmons ks on k.KitnmonID = ks.KitnmonID INNER JOIN  CategoryKitnimons cs on  ks.CategoryKitID = cs.CategoryKitID " +
                   " where cs.WorkName like '%' + @name + '%'" +
                    "or ks.date like '%' + @name + '%'" +
                      
                    "and  p.UserID='" + lblID.Text + "'" +
                    " order by KitnmonID desc";

                SqlCommand com = new SqlCommand(sql, conn);
                com.Parameters.AddWithValue("@name", name);
                SqlDataAdapter da = new SqlDataAdapter(com);
                DataSet ds = new DataSet();
                da.Fill(ds);
                dgvdatag.DataSource = ds.Tables[0];

                dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            }
            else
            {
                ShowData();
            }
        }
    }
    }
    
