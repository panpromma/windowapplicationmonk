﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using DevExpress.XtraReports.UI;
using MaterialSkin.Controls;
using MaterialSkin;

namespace mork
{
    public partial class frmReport : MetroFramework.Forms.MetroForm
    {
      
        public int UserID { get; set; }
        public string Namef { get; set; }
        public string Department { get; set; }
        public string Phone { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int age { get; set; }
        public string Address { get; set; }
        public string Prefix { get; set; }

        public frmReport()
        {
            InitializeComponent();
            

        }
        SqlConnection conn;
        
        private void FrmReport_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            showdata();
            UserID = -1;
        }

        private void showdata()
        {
            string sql = "select UserID,Prefix,Name ,Department,Phone,Age,Address,Username,Password  from Personnels ";   //คำสั่ง Query
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);  //สร้างการเชื่อมต่อผ่าน conn
            DataSet ds = new DataSet();    //สร้าง obj สำหรับเก็บข้อมูล
            da.Fill(ds);                   //คำสั่งให้นำข้อมูลที่เชื่อมไว้ไปใส่ไว้ใน ds
            dgvdatag.DataSource = ds.Tables[0]; //ดึงข้อมูลจาก ds ตารางที่ 0 ออกไปแสดง
            showskin();

        }
        private void showskin()
        {
            dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
            dgvdatag.Columns[1].HeaderCell.Value = "คำนำหน้า";
            dgvdatag.Columns[2].HeaderCell.Value = "ชื่อสกุล";
            dgvdatag.Columns[3].HeaderCell.Value = "ฝ่าย";
            dgvdatag.Columns[4].HeaderCell.Value = "เบอร์โทร";
            dgvdatag.Columns[5].HeaderCell.Value = "อายุ";
            dgvdatag.Columns[6].HeaderCell.Value = "ที่อยู่";
            dgvdatag.Columns[7].HeaderCell.Value = "ชื่อผู้ใช้";
            dgvdatag.Columns[8].HeaderCell.Value = "รหัสผ่าน";



            //dgvdatag.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            //dgvdatag.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            //dgvdatag.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            //dgvdatag.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            //dgvdatag.BackgroundColor = Color.White;

            //dgvdatag.EnableHeadersVisualStyles = false;
            //dgvdatag.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            //dgvdatag.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            //dgvdatag.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
        }
        private void Btnreport_Click(object sender, EventArgs e)
        {
            Xruser f = new Xruser();
            f.ShowPreviewDialog();
        }

        private void Button1_Click(object sender, EventArgs e)
        {

            frmPersonnel f = new frmPersonnel();
            f.status = "เพิ่ม";
            f.ShowDialog();
            showdata();

        }

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
           
        }

        private void Txtp_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            showdata(txtID.Text);
            
        }
        private void showdata(string text)
        {
            string sql = "select *from Personnels "
                        + "where name like '%' + @strName + '%' "
                        + "or UserID like '%' + @strName + '%' "
                        + "or Prefix like '%' + @strName + '%' "
                        + "or Username like '%' + @strName + '%' "
                        + "or Age like '%' + @strName + '%' "
                        + "or Prefix like '%' + @strName + '%'"
                        + "or Address like '%' + @strName + '%'"
                        + "or Department like '%'+@strName +'%'";
            SqlCommand com = new SqlCommand(sql, conn);
            com.Parameters.AddWithValue("@strName", text);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Btndelete_Click(object sender, EventArgs e)
        {
            if (UserID == -1)
            {
                MessageBox.Show("โปรดเลือกข้อมูลที่ต้องการลบ", "ERROR");
                return;
            }
            //ยืนยันการลบ
            if (MessageBox.Show("คุณต้องการจะลบข้อมูงของ"+"   "+Prefix +"  "+Namef.ToString()+"  "+"หรือไหม?", "โปรดยืนยัน", MessageBoxButtons.YesNo)
                == DialogResult.No)
            {
                return;
            }
            
            //คำสั่งลบข้อมูล
            string sql = "Delete from Personnels where UserID = @UserID";
            SqlCommand comm = new SqlCommand(sql, conn);
            comm.Parameters.AddWithValue("@UserID",UserID);
            try
            {
                conn.Open();
                comm.ExecuteNonQuery();
                conn.Close();
                   //ถ้าทำงานคำสั่งนี้ Error ให้ข้ามไปทำใน Catch
            }
            catch
            {
                MessageBox.Show("มีข้อมูลอื่นอ้างอิงข้อมูลนี้", "ไม่สามารถลบได้");
            }

            showdata();
        }
        
        private void Dgvdatag_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            var x = dgvdatag.CurrentRow.Cells;
            UserID = Convert.ToInt16(x[0].Value);
            Namef = x[2].Value.ToString();
            Department = x[3].Value.ToString();
            Phone = x[4].Value.ToString();
            Username = x[7].Value.ToString();
            Password = x[8].Value.ToString();
            age = Convert.ToInt16(x[5].Value);
            Address = x[6].Value.ToString();
            Prefix = x[1].Value.ToString();
            

        }

        private void Dgvdatag_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //ประกาศให้เปิดฟอร์ม Form2 สำหรับแก้ไข
            frmPersonnel f = new frmPersonnel();
            //นำข้อมูลในตัวแปร ส่งไปที่ Form2 เพื่อเก็บไว้แก้ไข
            f.UserID = this.UserID;
            f.Namef = this.Namef;
            f.Department = this.Department;
            f.Phone = this.Phone;
            f.Username = this.Username;
            f.Password = this.Password;
            f.age = this.age;
            f.Address = this.Address;
            f.Prefix = this.Prefix;
            f.status = "แก้ไข";
            //แสดง Form2
            f.ShowDialog();
            showdata();
        }

        private void Dgvdatag_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
    }
}
