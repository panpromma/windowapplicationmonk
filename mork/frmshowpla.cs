﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraReports.UI;

namespace mork
{
    public partial class frmshowpla : MetroFramework.Forms.MetroForm
    {
    
        public int RequestID { get; set; }//รหัสกิจนิมนต์
        public string Date { get; set; }
        public string Time { get; set; }
        public string Description { get; set; }
        public string Phone { get; set; }
        public string PlaceID { get; set; }
        public string UserID { get; set; }//ชื่อผู้รับ
        public string SecularID { get; set; }//ชื่อฆราวาส
        public int ID { get; set; }

        public frmshowpla()
        {
            InitializeComponent();
            
        }
        SqlConnection conn;
        private void Frmshowpla_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            cbo();
            showdata1("");
        }
        private void cbo()
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[] { new DataColumn("Id", typeof(int)), new DataColumn("Name", typeof(string)) });
            dt.Rows.Add(0, "ยังไม่อนุมัติ");
            dt.Rows.Add(1, "อุนิมัตแล้ว");


            //Insert the Default Item to DataTable.


            //Assign DataTable as DataSource.
            cboClass.DataSource = dt;
            cboClass.DisplayMember = "Name";
            cboClass.ValueMember = "Id";

        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
           
        }
        

        private void DateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void Dgvdatag_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var x = dgvdatag.CurrentRow.Cells;
            RequestID = Convert.ToInt16(x[0].Value.ToString());
            Date = x[1].Value.ToString();
            Time = x[2].Value.ToString();
            Description = x[3].Value.ToString();
            Phone = x[4].Value.ToString();
            PlaceID = x[5].Value.ToString();
            UserID = x[6].Value.ToString();
            SecularID = x[7].Value.ToString();

            frmshowdataPlacerequests f = new frmshowdataPlacerequests();
            f.RequestID = this.RequestID;
            f.Date = this.Date;
            f.Time = this.Time;
            f.Description = this.Description;
            f.Phone = this.Phone;
            f.PlaceID = this.PlaceID;
            f.UserID = this.UserID;
            f.SecularID = this.SecularID;


            f.ShowDialog();
          
        }

        private void Dgvdatag_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void CboClass_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        

        private void Button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(cboClass.SelectedValue.ToString());
            if (cboClass.SelectedIndex == 0)
            {
                showdata1("");
                showdata1(Convert.ToString(txtID.Text));
            }
            else if (cboClass.SelectedIndex == 1)
            {
                //showdata2("");
                //showdata2(Convert.ToString(txtID.Text));

            }

        }
        private void showdata1(string txt)
        {
            string sql = "select RequestID as รหัส , dbo.mydate(date) as วันที่ , Time as เวลา ,ps.Description as รายละเอียด , ps.Phone as เบอร์โทร , p.PlaceName as ชื่อสถานที่ , pp.Name as ชื่อผู้รับงาน , s.name as ชื่อฆราวาส from Placerequests ps inner join Places p on ps.PlaceID=p.PlaceID inner join Personnels pp on ps.UserID=pp.UserID inner join Seculars s on ps.SecularID=s.SecularID"+
           " where date like '%' + @name + '%'" +
             "or Time like '%' + @name + '%'" +
             "or p.PlaceName like '%' + @name + '%'" +
             "or pp.Name like '%' + @name + '%'" +
              "or  s.name like '%' + @name + '%'" +
            "order by RequestID desc";

            //"or s.name like '%' + @name + '%'" +
            //"or p.Name like '%' + @name + '%'" +
            //"order by KitnmonID desc";

            SqlCommand com = new SqlCommand(sql, conn);
            SqlDataAdapter da = new SqlDataAdapter(com);
            com.Parameters.AddWithValue("@name", txt);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvdatag.Columns[0].Width = 50;
        }
        //private void showdata2(string txt)
        //{
        //    string sql = "select RequestID as รหัส ,  dbo.mydate(Date) as วันที่, Time as เวลา ,ps.Description as รายละเอียด , ps.Phone as เบอร์โทร ,case when ps.Approve=0 then 'ไม่อนุมัติ' when ps.Approve=1 then 'อนุมัติ' end as สถานะการอนุมัติ, p.PlaceName as ชื่อสถานที่ , pp.Name as ชื่อผู้รับงาน , s.name as ชื่อฆราวาส from Placerequests ps inner join Places p on ps.PlaceID=p.PlaceID inner join Personnels pp on ps.UserID=pp.UserID inner join Seculars s on ps.SecularID=s.SecularID" +
        //            " where ps.Approve = '1'   " +
        //            "and Date like '%' + @name + '%'" +
        //            "or Time like '%' + @name + '%'" +
        //            "or p.PlaceName like '%' + @name + '%'" +
        //             "or pp.Name like '%' + @name + '%'" +
        //              "or  s.name like '%' + @name + '%'" +
        //            "order by RequestID desc";


        //    SqlCommand com = new SqlCommand(sql, conn);
        //    SqlDataAdapter da = new SqlDataAdapter(com);
        //    com.Parameters.AddWithValue("@name", txt);
        //    DataSet ds = new DataSet();
        //    da.Fill(ds);
        //    dgvdatag.DataSource = ds.Tables[0];
        //    dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        //}

        private void TxtID_TextChanged_1(object sender, EventArgs e)
        {
            showdata1(Convert.ToString(txtID.Text));
            //showdata2(Convert.ToString(txtID.Text));
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            xrEQd f = new xrEQd();
            f.ShowPreviewDialog();
        }
    }
}
