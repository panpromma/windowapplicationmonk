﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Linq;
namespace mork
{
    public partial class Xreeee : DevExpress.XtraReports.UI.XtraReport
    {
        public Xreeee()
        {
            InitializeComponent();
        }
        DataClasses1DataContext db;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel17.Text = Convert.ToString(this.GetCurrentColumnValue("BorrowID"));
            xrLabel16.Text = Convert.ToString(this.GetCurrentColumnValue("RedateB"));
            xrLabel15.Text = Convert.ToString(this.GetCurrentColumnValue("Redate"));
            xrLabel14.Text = Convert.ToString(this.GetCurrentColumnValue("Time"));
            xrLabel13.Text = Convert.ToString(this.GetCurrentColumnValue("Retundate"));
            xrLabel12.Text = Convert.ToString(this.GetCurrentColumnValue("Name"));
            xrLabel11.Text = Convert.ToString(this.GetCurrentColumnValue("name"));
        }

        private void Xreeee_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            db = new DataClasses1DataContext();

            var v = from b in db.Borrows
                    join ps in db.Personnels on b.UserID equals ps.UserID
                    join sc in db.Seculars on b.SecularID equals sc.SecularID




                    select new
                    {
                        b.BorrowID,
                        RedateB = String.Format("{0:dd MMMM yyy}", b.RedateB),
                        Redate = String.Format("{0:dd MMMM yyy}", b.Redate),

                        b.Time,
                        Retundate = String.Format("{0:dd MMMM yyy}", b.Retundate),

                        ps.Name,
                        sc.name



                    };

            this.DataSource = v;
        }
    }
}
