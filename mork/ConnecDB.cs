﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Configuration;
using System.Xml;
using System.Data.SqlClient;

namespace mork
{
    class ConnecDB
    {
        public SqlConnection SqlStrCon()
        {
            //string server = @"DESKTOP-GF7IG8J\SQLEXPRESS";
            //string db = "monk";
            //string strCon = string.Format("Data Source={0};" + "Initial Catalog={1};" + "Integrated Security=True", server, db);
            //return new SqlConnection(strCon);
            //string strCon = "Data Source= DESKTOP-GF7IG8J\\SQLEXPRESS;Initial Catalog=monk2;Integrated Security=True";
            //string strCon = (@"Data Source= 192.168.1.11,1433;Initial Catalog=monk2; User= admin; Password=1234;");


            //string server = @"192.168.1.11";   //ตรงนี้ชื่อ Server เครื่องตัวเอง
            //string port = "1433";
            //string databasename = "monk2";            //ชื่อฐานข้อมูล
            //string username = "admin";
            //string password = "1234";

            string server = ConfigurationManager.AppSettings["server"];
            string port = ConfigurationManager.AppSettings["port"];
            string databasename = ConfigurationManager.AppSettings["database"];         //ชื่อฐานข้อมูล
            string username = ConfigurationManager.AppSettings["username"];
            string password = ConfigurationManager.AppSettings["password"];

            string strCon = string.Format(@"Data Source= {0},{1};Initial Catalog={2}; User= {3}; Password={4};"
                                           , server,port,databasename,username,password);


            //return new SqlConnection(strConn);
            SqlConnection conn = new SqlConnection(strCon);
            
            //conn.Open();
            return conn;
        }
        //public static SqlConnection SqlStrCon()
        //{
        //    //string strCon = "Data Source=192.168.1.2;Initial Catalog=HousingFundOfNongKhonKwang1;User ID=sa;Password=12345678";
        //    //string strCon = "Data Source=kittiphop17.com;Initial Catalog=HousingFundOfNongKhonKwang1;User ID=user;Password=Kkpdata171155@";   
        //    string strCon = "Data Source= DESKTOP-GF7IG8J\\SQLEXPRESS;Initial Catalog=monk;Integrated Security=True";
        //    SqlConnection conn = new SqlConnection(strCon);
        //    conn.Open();
        //    return conn;
        //}
    }
}
