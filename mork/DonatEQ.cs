﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Linq;
namespace mork
{
    public partial class DonatEQ : DevExpress.XtraReports.UI.XtraReport
    {
        public DonatEQ()
        {
            InitializeComponent();
        }
        DataClasses1DataContext db;
        public int ID;
        private void DonatEQ_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            db = new DataClasses1DataContext();

            var v = from k in db.Donateequipments
                    join s in db.DonateEMlists on k.DonateequipID equals s.DonateequipID
                    join ee in db.Equipments on s.EquipmentID equals ee.EquipmentID
                    where k.DonateequipID == ID
                    //orderby f.Name
                    select new
                    {

                        k.description,
                        k.donor,
                       s.Number,
                       ee.Equipmenname,


                        Bdate = String.Format("{0:dd MMMM yyy}", k.datedonat),


                    };

            this.DataSource = v;
        }

        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel3.Text = Convert.ToString(this.GetCurrentColumnValue("donor"));
            xrLabel14.Text = Convert.ToString(this.GetCurrentColumnValue("Equipmenname"));
            xrLabel12.Text = Convert.ToString(this.GetCurrentColumnValue("Number"));
            xrLabel15.Text = Convert.ToString(this.GetCurrentColumnValue("description"));
            xrLabel11.Text = DateTime.Now.ToLongDateString();
        }
    }
}
