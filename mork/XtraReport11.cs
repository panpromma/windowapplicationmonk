﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Linq;
using System.Data.SqlClient;
namespace mork
{
    public partial class XtraReport11 : DevExpress.XtraReports.UI.XtraReport
    {
        public XtraReport11()
        {
            InitializeComponent();
        }
        
        //ConnecDB conn = new ConnecDB();
        DataClasses1DataContext db;
        //int number;
        //SqlConnection conn;
        public int ID;
        public string mon;
        public string Time2;
        public string namePP;
        private void XtraReport11_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            db = new DataClasses1DataContext();

            var v = from k in db.Kitnmons
                    join c in db.CategoryKitnimons on k.CategoryKitID equals c.CategoryKitID
                    join s in db.Seculars on k.SecularID equals s.SecularID
                    join ks in db.Kitnimonlists on k.KitnmonID equals ks.KitnmonID
                    join p in db.Personnels on ks.UserID equals p.UserID
                    join pss in db.Personnels on k.UserID equals pss.UserID
                    join vv in db.Usevehiclesses on k.KitnmonID equals vv.KitnmonID
                    join vvv in db.Vehicles on vv.VehicleID equals vvv.VehicleID

                    where k.KitnmonID ==ID
                    //orderby f.Name
                    select new
                    {
                        k.KitnmonID,
                       c.WorkName,
                       ks.UserID,
                        p.Name,
                        s.name,
                        ks.TimeS,
                        ks.TimeEN,
                        Idate = String.Format("{0:dd MMMM yyy}", ks.Dates),
                        k.Address,
                        vvv.Vehiclecategory,
                        k.Workname











                    };

            this.DataSource = v;



        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            
           

           
           
            xrLabel8.Text = Convert.ToString(this.GetCurrentColumnValue("UserID"));
            xrLabel9.Text = Convert.ToString(this.GetCurrentColumnValue("Name"));
            xrLabel20.Text = Convert.ToString(this.GetCurrentColumnValue("TimeS"));
            xrLabel22.Text = Convert.ToString(this.GetCurrentColumnValue("TimeEN"));
            xrLabel24.Text = Convert.ToString(this.GetCurrentColumnValue("Idate"));
           
            //number += 1;
            //xrLabel8.Text = Convert.ToString(number);
        }

        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            
        }

        private void ReportHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string name;
            string WorkName;
           
       

            name = Convert.ToString(this.GetCurrentColumnValue("KitnmonID"));
            
            WorkName = Convert.ToString(this.GetCurrentColumnValue("WorkName"));
            xrLabel26.Text = Convert.ToString(this.GetCurrentColumnValue("Address"));
            xrLabel28.Text = Convert.ToString(this.GetCurrentColumnValue("Vehiclecategory"));
            xrLabel30.Text = Convert.ToString(this.GetCurrentColumnValue("Workname"));
            xrLabel2.Text = name;
            xrLabel3.Text = WorkName;
            xrLabel12.Text = Time2;
            xrLabel14.Text = mon;
        }

        private void ReportFooter_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel17.Text = namePP;
            xrLabel16.Text = Convert.ToString(this.GetCurrentColumnValue("name"));
        }
    }
}
