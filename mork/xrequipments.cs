﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Linq;
namespace mork
{
    public partial class xrequipments : DevExpress.XtraReports.UI.XtraReport
    {
        public xrequipments()
        {
            InitializeComponent();
        }
        DataClasses1DataContext db;
        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel10.Text = Convert.ToString(this.GetCurrentColumnValue("EquipmentID"));
            xrLabel11.Text = Convert.ToString(this.GetCurrentColumnValue("Equipmenname"));
            xrLabel12.Text = Convert.ToString(this.GetCurrentColumnValue("Numbertreasury"));
            xrLabel13.Text = Convert.ToString(this.GetCurrentColumnValue("Numberdamaged"));
            //xrLabel14.Text = Convert.ToString(this.GetCurrentColumnValue("Numberlost"));
            xrLabel16.Text = Convert.ToString(this.GetCurrentColumnValue("Description"));
        }

        private void Xrequipments_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            db = new DataClasses1DataContext();

            var v = from k in db.Equipments
                    
                    //orderby f.Name
                    select new
                    {
                        k.EquipmentID,
                        k.Equipmenname,
                        k.Numbertreasury,
                        k.Numberdamaged,
                        k.Numberlost,
                        k.Description,
                        






                    };

            this.DataSource = v;

        }
    }
}
