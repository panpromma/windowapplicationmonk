﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace mork
{
    public partial class frmPlacerequest : MetroFramework.Forms.MetroForm
    {
        
        public int ID { get; set; }
        SqlConnection conn;
       
        public frmPlacerequest()
        {

            InitializeComponent();
            //this.dtpImportTime.CustomFormat = "HH:mm ";
            //this.dtpImportTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            //this.dtpImportTime.ShowUpDown = true;
            
        }
        public int UserID;
        public string name;
        private void FrmPlacerequest_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            string id = frmLogin.eID.ToString();
          
            setcboPlace();

            string name = frmLogin.name;
            lblID.Text = id;
            lblName.Text = name;
        }

        

        private void setcboPlace()
        {
            string sql = "select PlaceID , PlaceName from Places";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            
            cboname.DataSource = ds.Tables[0];
            cboname.DisplayMember = "PlaceName";
            cboname.ValueMember = "PlaceID";
           
        }
        
        private void Btna_Click(object sender, EventArgs e)
        {
            
        }

        private void Btnseve_Click(object sender, EventArgs e)
        {
            if (txtPhone.Text.Trim() == "" || txtDescription.Text.Trim() == "" || txtSecularID.Text.Trim() == "")
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน");
            }
           
            else
            { 
            string sql = "insert into Placerequests values(  @Date, @Time, @Description, @Phone, @Approve, @PlaceID, @UserID, @SecularID)";
            SqlCommand comm = new SqlCommand(sql, conn);





            comm.Parameters
                .AddWithValue("@Date", dtpImportDate.Value);

            comm.Parameters
                .AddWithValue("@Time", maskedTextBox1.Text.Trim());

            comm.Parameters
                .AddWithValue("@Description", txtDescription.Text.Trim());

            comm.Parameters
               .AddWithValue("@Phone", txtPhone.Text.Trim());

            comm.Parameters
               .AddWithValue("@Approve", "0");

            comm.Parameters
               .AddWithValue("@PlaceID", cboname.SelectedValue);

            comm.Parameters
               .AddWithValue("@UserID", frmLogin.eID);

            comm.Parameters
               .AddWithValue("@SecularID", txtSecularID.Text.Trim());




            if (conn.State == ConnectionState.Closed) conn.Open();
            comm.ExecuteNonQuery();
            conn.Close();
                MessageBox.Show("เสร็จสิ้น");
            clearfrom();
            
            }
        }

        private void clearfrom()
        {

            dtpImportDate.Text = "";
            maskedTextBox1.Text = "";
            txtDescription.Text = "";
            txtPhone.Text = "";
            textname.Text = "";
            cboname.Text = "";
          
            txtSecularID.Text = "";
        }

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            clearfrom();
        }

        private void GroupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            
            frmpint f = new frmpint();
            
            f.Show();
        }

        private void BtnSearch_Click(object sender, EventArgs e)
        {
            using (frmshowSecular f = new frmshowSecular())
            {
                f.ShowDialog();
                txtSecularID.Text = f.SecularID.ToString();
                txtPhone.Text = f.Phone.ToString();
                textname.Text = f.NameF.ToString();

            }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            using (frmshowSecular f = new frmshowSecular())
            {
                f.ShowDialog();
                txtSecularID.Text = f.SecularID.ToString();
                txtPhone.Text = f.Phone.ToString();
                textname.Text = f.NameF.ToString();

            }
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            frmpint f = new frmpint();

            f.ShowDialog();
        }
    }
}
