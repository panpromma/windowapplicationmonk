﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
namespace mork
{
    public partial class frmSecular : MetroFramework.Forms.MetroForm
    {
        
        public frmSecular()
        {
            InitializeComponent();
            

        }
        SqlConnection conn;
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        public int SecularID { get; set; }
        public string phones { get; set; }

        public string names;


        private void Prefix_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Label4_Click(object sender, EventArgs e)
        {

        }

        private void Add_Click(object sender, EventArgs e)
        {
            
            string sql = "insert into Seculars values( @name,@phone,@Address,@age)";
            SqlCommand comm = new SqlCommand(sql, conn);





            comm.Parameters
                .AddWithValue("@name", txtNameF.Text.Trim());

            comm.Parameters
                .AddWithValue("@phone", txtPhone.Text.Trim());

            comm.Parameters
                .AddWithValue("@Address", txtAddress.Text.Trim());

            comm.Parameters
               .AddWithValue("@age", txtAge.Text.Trim());

           



            

            if (conn.State == ConnectionState.Closed) conn.Open();
            comm.ExecuteNonQuery();
            conn.Close();
            clearfrom();
            ShowData();
        }

        private void clearfrom()
        {
            TextID.Text = "";
            txtPhone.Text = "";
            txtNameF.Text = "";
            txtAge.Text = "";
         
            txtAddress.Text = "";
        }

        private void Secular_Load(object sender, EventArgs e)
        {
          
            conn = new ConnecDB().SqlStrCon();
            ShowData();

        }
       
        private void ShowData()
        {
            string sql = "select SecularID , name ,phone , Address , age from Seculars";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            showskin();
        }
        private void showskin()
        {
            dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
            dgvdatag.Columns[1].HeaderCell.Value = "ชื่อสกุล";
            dgvdatag.Columns[2].HeaderCell.Value = "เบอร์โทร";
            dgvdatag.Columns[3].HeaderCell.Value = "ที่อยู่";
            dgvdatag.Columns[4].HeaderCell.Value = "อายุ";
            


            //dgvdatag.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            //dgvdatag.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            //dgvdatag.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            //dgvdatag.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            //dgvdatag.BackgroundColor = Color.White;

            //dgvdatag.EnableHeadersVisualStyles = false;
            //dgvdatag.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            //dgvdatag.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            //dgvdatag.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
        }
        private void Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("!!!!คณุต้องการจะลบใช่ไหม?"+"  "+names , Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {

                string sql = "delete from Seculars where SecularID = @SecularID";
                SqlCommand comm = new SqlCommand(sql, conn);



                comm.Parameters

                    .AddWithValue("@SecularID", TextID.Text.Trim());

                try
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                    clearfrom();
                    ShowData();

                }
                catch
                {
                    MessageBox.Show("ไม่สามารถลบข้มูลได้", "Erorr");

                    return;
                }
            }
        }

        private void Datag_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            var x = dgvdatag.CurrentRow.Cells;

          names = x[1].Value.ToString();


            TextID.Text = x["SecularID"].Value.ToString();
            
            txtNameF.Text = x["name"].Value.ToString();
            txtPhone.Text = x["phone"].Value.ToString();
            txtAddress.Text = x["Address"].Value.ToString();
            txtAge.Text = x["age"].Value.ToString();
           

        }

        private void Modify_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("!!!!คณุต้องการจะแก้ไขข้อมูลชุดนี้ไหมไหม?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {


                string sql = "update Seculars set    name=@name , phone=@phone , Address=@Address , age=@age   where  SecularID=@SecularID  ";
                SqlCommand comm = new SqlCommand(sql, conn);


                comm.Parameters
                   .AddWithValue("@SecularID", TextID.Text.Trim());

                comm.Parameters
                    .AddWithValue("@name", txtNameF.Text.Trim());

                comm.Parameters
                    .AddWithValue("@phone", txtPhone.Text.Trim());
                comm.Parameters
                   .AddWithValue("@Address", txtAddress.Text.Trim());
                comm.Parameters
                    .AddWithValue("@age", txtAge.Text.Trim());

          





                try
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                    clearfrom();
                    ShowData();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            clearfrom();

        }

        
        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            showdata(txtID.Text.ToString());
        }
        private void showdata(string text)
        {
            string sql = "select SecularID , name ,phone , Address , age from Seculars"
                + " where name like '%' + @strName + '%'";
            SqlCommand com = new SqlCommand(sql, conn);
            com.Parameters.AddWithValue("@strName", text);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            showskin();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
