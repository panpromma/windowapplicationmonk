﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
using DevExpress.XtraReports.UI;
namespace mork
{
    public partial class frmshowaddBorrow : MetroFramework.Forms.MetroForm
    {
        public frmshowaddBorrow()
        {
            InitializeComponent();
        }
    
        SqlConnection conn;
        public int BorrowID { get; set; }//รหัสกิจนิมนต์
        public string RedateB { get; set; }
        public string Redate { get; set; }
        public string Time { get; set; }
        public string Retundate { get; set; }
        public string Approve { get; set; }
        public string nameP { get; set; }//ชื่อผู้รับ
        public string names { get; set; }//ชื่อฆราวาส
        public string status { get; set; }
        private void FrmshowaddBorrow_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            if (status == "แสดง")
            {
                btnapprove.Visible = false;
                button2.Visible = false;

            }
            else
            {
                btnapprove.Visible = true;
                button2.Visible = true;
            }
            label1.Text = BorrowID.ToString();
            label2.Text = RedateB.ToString();
            label3.Text = Redate.ToString();
            label5.Text = Time.ToString();
            label6.Text = Retundate.ToString();
            label7.Text = Approve.ToString();
            label8.Text = nameP.ToString();
            label18.Text = names.ToString();
            ShowData();

        }

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btnapprove_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("!!!!คณุต้องการจะอนุมัติหรือไม่?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {


                string sql = "update Borrows set Approve=@Approve  where  BorrowID=@BorrowID  ";
                SqlCommand comm = new SqlCommand(sql, conn);

                comm.Parameters
                   .AddWithValue("@BorrowID", label1.Text.Trim());

                comm.Parameters
                    .AddWithValue("@Approve", "1");



                try
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();


                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            { return; }
            MessageBox.Show("อนุมัติเสร็จสิ้น");

            this.Close();

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void ShowData()
        {
            string sql = "select e.Equipmenname ,es.Number from Borrows b inner join Equipmentlists es on b.BorrowID =es.BorrowID  inner join Equipments e on es.EquipmentID = e.EquipmentID  where b.BorrowID ='" + label1.Text + "'";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            dgvdatag.Columns[0].HeaderCell.Value = "ชื่องาน";
            dgvdatag.Columns[1].HeaderCell.Value = "จำนวน";

        }
       
        private void Button1_Click(object sender, EventArgs e)
        {
            Reborrow f = new Reborrow();
            f.ID = Convert.ToInt32(label1.Text);
            f.RedateB = Convert.ToString(label2.Text);
            f.Redate = Convert.ToString(label3.Text);
            f.ShowPreviewDialog();
        }
    }
}
