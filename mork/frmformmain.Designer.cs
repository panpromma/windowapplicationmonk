﻿namespace mork
{
    partial class frmformmain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmformmain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.ฝายอปกรณToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip3 = new System.Windows.Forms.MenuStrip();
            this.ฝายอปกรณToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip4 = new System.Windows.Forms.MenuStrip();
            this.ผดแลระบบToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip5 = new System.Windows.Forms.MenuStrip();
            this.เจาอาวาสToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip6 = new System.Windows.Forms.MenuStrip();
            this.พระสงฆToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.ออกจากระบบToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.อนมตรายการกจนมนตToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.อนมตการขอใชสถานทToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.อนมนตการขอใชอปกรณToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.รายการการยมอกรณToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.ขอมลกจนมนตToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ขอมลการยมอปกรณToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ขอมลการขอใชสถานทToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.รายงานการขอใชสถานทToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ออกจากระบบToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.คนหากจนมนตToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ออกจากระบบToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.จดการขอมลผใชงานToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ออกจากระบบToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.จดการการยมอปกรณToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.จดการขอมลอปกรณToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.จดการประเภทอปกรณToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.จดการการคนอปกรณToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.พมพใบยมวสดอปกรณไดToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.เขาสระบบToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.จดการขอมลระบบToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ออกจากระบบToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.จดการขอมลฆราวาสToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.จดการการรบกจนมนตToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.จดการการขอใชสถานทToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.จดการขอมลToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.จดการขอมลสถานทToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.จดการขอมลประเภทกจจมนตToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.จดการยานพาหนะToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.จดการขอมลพระสงฆToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.บรจาคเงนToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.คนหาและดขอมลToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ขอมลผคำประกนToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.คนหาขอมลการขอใชสถานทToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.menuStrip3.SuspendLayout();
            this.menuStrip4.SuspendLayout();
            this.menuStrip5.SuspendLayout();
            this.menuStrip6.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem6,
            this.เขาสระบบToolStripMenuItem,
            this.จดการขอมลระบบToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(244, 60);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(6, 3, 0, 3);
            this.menuStrip1.Size = new System.Drawing.Size(224, 707);
            this.menuStrip1.TabIndex = 16;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.MenuStrip1_ItemClicked);
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold);
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(217, 34);
            this.toolStripMenuItem6.Text = "ยินดีต้อนรับ";
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 0;
            this.bunifuElipse1.TargetControl = this;
            // 
            // menuStrip2
            // 
            this.menuStrip2.AutoSize = false;
            this.menuStrip2.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip2.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ฝายอปกรณToolStripMenuItem1,
            this.ออกจากระบบToolStripMenuItem,
            this.toolStripMenuItem4,
            this.จดการการยมอปกรณToolStripMenuItem,
            this.จดการขอมลอปกรณToolStripMenuItem,
            this.จดการประเภทอปกรณToolStripMenuItem,
            this.จดการการคนอปกรณToolStripMenuItem,
            this.toolStripMenuItem5,
            this.พมพใบยมวสดอปกรณไดToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(468, 60);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Padding = new System.Windows.Forms.Padding(6, 3, 0, 3);
            this.menuStrip2.Size = new System.Drawing.Size(224, 707);
            this.menuStrip2.TabIndex = 17;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // ฝายอปกรณToolStripMenuItem1
            // 
            this.ฝายอปกรณToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold);
            this.ฝายอปกรณToolStripMenuItem1.Name = "ฝายอปกรณToolStripMenuItem1";
            this.ฝายอปกรณToolStripMenuItem1.Size = new System.Drawing.Size(217, 34);
            this.ฝายอปกรณToolStripMenuItem1.Text = "ฝ่ายอุปกรณ์";
            // 
            // menuStrip3
            // 
            this.menuStrip3.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip3.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ฝายอปกรณToolStripMenuItem,
            this.ออกจากระบบToolStripMenuItem1,
            this.จดการขอมลฆราวาสToolStripMenuItem,
            this.จดการการรบกจนมนตToolStripMenuItem,
            this.จดการการขอใชสถานทToolStripMenuItem,
            this.จดการขอมลToolStripMenuItem,
            this.จดการขอมลพระสงฆToolStripMenuItem,
            this.บรจาคเงนToolStripMenuItem,
            this.คนหาและดขอมลToolStripMenuItem1});
            this.menuStrip3.Location = new System.Drawing.Point(20, 60);
            this.menuStrip3.Name = "menuStrip3";
            this.menuStrip3.Padding = new System.Windows.Forms.Padding(6, 3, 0, 3);
            this.menuStrip3.Size = new System.Drawing.Size(224, 707);
            this.menuStrip3.TabIndex = 18;
            this.menuStrip3.Text = "menuStrip3";
            // 
            // ฝายอปกรณToolStripMenuItem
            // 
            this.ฝายอปกรณToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ฝายอปกรณToolStripMenuItem.Name = "ฝายอปกรณToolStripMenuItem";
            this.ฝายอปกรณToolStripMenuItem.Size = new System.Drawing.Size(211, 34);
            this.ฝายอปกรณToolStripMenuItem.Text = "ฝ่ายกิจนิมนต์";
            // 
            // menuStrip4
            // 
            this.menuStrip4.AutoSize = false;
            this.menuStrip4.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip4.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuStrip4.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.menuStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ผดแลระบบToolStripMenuItem,
            this.ออกจากระบบToolStripMenuItem2,
            this.จดการขอมลผใชงานToolStripMenuItem});
            this.menuStrip4.Location = new System.Drawing.Point(692, 60);
            this.menuStrip4.Name = "menuStrip4";
            this.menuStrip4.Padding = new System.Windows.Forms.Padding(6, 3, 0, 3);
            this.menuStrip4.Size = new System.Drawing.Size(224, 707);
            this.menuStrip4.TabIndex = 19;
            this.menuStrip4.Text = "menuStrip4";
            // 
            // ผดแลระบบToolStripMenuItem
            // 
            this.ผดแลระบบToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold);
            this.ผดแลระบบToolStripMenuItem.Name = "ผดแลระบบToolStripMenuItem";
            this.ผดแลระบบToolStripMenuItem.Size = new System.Drawing.Size(217, 34);
            this.ผดแลระบบToolStripMenuItem.Text = "ผู้ดูแลระบบ";
            // 
            // menuStrip5
            // 
            this.menuStrip5.AutoSize = false;
            this.menuStrip5.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip5.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.เจาอาวาสToolStripMenuItem,
            this.ออกจากระบบToolStripMenuItem3,
            this.อนมตรายการกจนมนตToolStripMenuItem,
            this.อนมตการขอใชสถานทToolStripMenuItem,
            this.อนมนตการขอใชอปกรณToolStripMenuItem,
            this.รายการการยมอกรณToolStripMenuItem,
            this.toolStripMenuItem10});
            this.menuStrip5.Location = new System.Drawing.Point(1140, 60);
            this.menuStrip5.Name = "menuStrip5";
            this.menuStrip5.Padding = new System.Windows.Forms.Padding(6, 3, 0, 3);
            this.menuStrip5.Size = new System.Drawing.Size(224, 674);
            this.menuStrip5.TabIndex = 20;
            this.menuStrip5.Text = "menuStrip5";
            // 
            // เจาอาวาสToolStripMenuItem
            // 
            this.เจาอาวาสToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold);
            this.เจาอาวาสToolStripMenuItem.Name = "เจาอาวาสToolStripMenuItem";
            this.เจาอาวาสToolStripMenuItem.Size = new System.Drawing.Size(217, 34);
            this.เจาอาวาสToolStripMenuItem.Text = "เจ้าอาวาส";
            // 
            // menuStrip6
            // 
            this.menuStrip6.AutoSize = false;
            this.menuStrip6.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip6.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuStrip6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.พระสงฆToolStripMenuItem,
            this.ออกจากระบบToolStripMenuItem4,
            this.คนหากจนมนตToolStripMenuItem});
            this.menuStrip6.Location = new System.Drawing.Point(916, 60);
            this.menuStrip6.Name = "menuStrip6";
            this.menuStrip6.Padding = new System.Windows.Forms.Padding(6, 3, 0, 3);
            this.menuStrip6.Size = new System.Drawing.Size(224, 707);
            this.menuStrip6.TabIndex = 21;
            this.menuStrip6.Text = "menuStrip6";
            // 
            // พระสงฆToolStripMenuItem
            // 
            this.พระสงฆToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold);
            this.พระสงฆToolStripMenuItem.Name = "พระสงฆToolStripMenuItem";
            this.พระสงฆToolStripMenuItem.Size = new System.Drawing.Size(217, 34);
            this.พระสงฆToolStripMenuItem.Text = "พระสงฆ์";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.lblDate);
            this.panel2.Controls.Add(this.lbltime);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(1140, 734);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(226, 33);
            this.panel2.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(124, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "เวลา";
            // 
            // lblDate
            // 
            this.lblDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.Location = new System.Drawing.Point(-20, 9);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(59, 20);
            this.lblDate.TabIndex = 0;
            this.lblDate.Text = "lblDate";
            // 
            // lbltime
            // 
            this.lbltime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lbltime.AutoSize = true;
            this.lbltime.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltime.Location = new System.Drawing.Point(161, 9);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(58, 20);
            this.lbltime.TabIndex = 0;
            this.lbltime.Text = "lbltime";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // ออกจากระบบToolStripMenuItem3
            // 
            this.ออกจากระบบToolStripMenuItem3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ออกจากระบบToolStripMenuItem3.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.ออกจากระบบToolStripMenuItem3.Image = global::mork.Properties.Resources.logout__1_;
            this.ออกจากระบบToolStripMenuItem3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ออกจากระบบToolStripMenuItem3.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ออกจากระบบToolStripMenuItem3.Name = "ออกจากระบบToolStripMenuItem3";
            this.ออกจากระบบToolStripMenuItem3.Size = new System.Drawing.Size(217, 68);
            this.ออกจากระบบToolStripMenuItem3.Text = "ออกจากระบบ";
            this.ออกจากระบบToolStripMenuItem3.Click += new System.EventHandler(this.ออกจากระบบToolStripMenuItem3_Click);
            // 
            // อนมตรายการกจนมนตToolStripMenuItem
            // 
            this.อนมตรายการกจนมนตToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.อนมตรายการกจนมนตToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("อนมตรายการกจนมนตToolStripMenuItem.Image")));
            this.อนมตรายการกจนมนตToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.อนมตรายการกจนมนตToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.อนมตรายการกจนมนตToolStripMenuItem.Name = "อนมตรายการกจนมนตToolStripMenuItem";
            this.อนมตรายการกจนมนตToolStripMenuItem.Size = new System.Drawing.Size(217, 68);
            this.อนมตรายการกจนมนตToolStripMenuItem.Text = "อนุมัติรายการกิจนิมนต์";
            this.อนมตรายการกจนมนตToolStripMenuItem.Click += new System.EventHandler(this.อนมตรายการกจนมนตToolStripMenuItem_Click);
            // 
            // อนมตการขอใชสถานทToolStripMenuItem
            // 
            this.อนมตการขอใชสถานทToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.อนมตการขอใชสถานทToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("อนมตการขอใชสถานทToolStripMenuItem.Image")));
            this.อนมตการขอใชสถานทToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.อนมตการขอใชสถานทToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.อนมตการขอใชสถานทToolStripMenuItem.Name = "อนมตการขอใชสถานทToolStripMenuItem";
            this.อนมตการขอใชสถานทToolStripMenuItem.Size = new System.Drawing.Size(217, 68);
            this.อนมตการขอใชสถานทToolStripMenuItem.Text = "อนุมัติการขอใช้สถานที่";
            this.อนมตการขอใชสถานทToolStripMenuItem.Click += new System.EventHandler(this.อนมตการขอใชสถานทToolStripMenuItem_Click);
            // 
            // อนมนตการขอใชอปกรณToolStripMenuItem
            // 
            this.อนมนตการขอใชอปกรณToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.อนมนตการขอใชอปกรณToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("อนมนตการขอใชอปกรณToolStripMenuItem.Image")));
            this.อนมนตการขอใชอปกรณToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.อนมนตการขอใชอปกรณToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.อนมนตการขอใชอปกรณToolStripMenuItem.Name = "อนมนตการขอใชอปกรณToolStripMenuItem";
            this.อนมนตการขอใชอปกรณToolStripMenuItem.Size = new System.Drawing.Size(217, 68);
            this.อนมนตการขอใชอปกรณToolStripMenuItem.Text = "อนุมันติการขอใช้อุปกรณ์";
            this.อนมนตการขอใชอปกรณToolStripMenuItem.Click += new System.EventHandler(this.อนมนตการขอใชอปกรณToolStripMenuItem_Click);
            // 
            // รายการการยมอกรณToolStripMenuItem
            // 
            this.รายการการยมอกรณToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.รายการการยมอกรณToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("รายการการยมอกรณToolStripMenuItem.Image")));
            this.รายการการยมอกรณToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.รายการการยมอกรณToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.รายการการยมอกรณToolStripMenuItem.Name = "รายการการยมอกรณToolStripMenuItem";
            this.รายการการยมอกรณToolStripMenuItem.Size = new System.Drawing.Size(217, 68);
            this.รายการการยมอกรณToolStripMenuItem.Text = "รายการการยืมอุกรณ์";
            this.รายการการยมอกรณToolStripMenuItem.Click += new System.EventHandler(this.รายการการยมอกรณToolStripMenuItem_Click);
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ขอมลกจนมนตToolStripMenuItem,
            this.ขอมลการยมอปกรณToolStripMenuItem,
            this.ขอมลการขอใชสถานทToolStripMenuItem,
            this.รายงานการขอใชสถานทToolStripMenuItem});
            this.toolStripMenuItem10.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.toolStripMenuItem10.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem10.Image")));
            this.toolStripMenuItem10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolStripMenuItem10.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(217, 72);
            this.toolStripMenuItem10.Text = "รายงาน";
            // 
            // ขอมลกจนมนตToolStripMenuItem
            // 
            this.ขอมลกจนมนตToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ขอมลกจนมนตToolStripMenuItem.Image")));
            this.ขอมลกจนมนตToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ขอมลกจนมนตToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ขอมลกจนมนตToolStripMenuItem.Name = "ขอมลกจนมนตToolStripMenuItem";
            this.ขอมลกจนมนตToolStripMenuItem.Size = new System.Drawing.Size(306, 70);
            this.ขอมลกจนมนตToolStripMenuItem.Text = "รายงานกิจนิมนต์พระสงฆ์";
            this.ขอมลกจนมนตToolStripMenuItem.Click += new System.EventHandler(this.ขอมลกจนมนตToolStripMenuItem_Click);
            // 
            // ขอมลการยมอปกรณToolStripMenuItem
            // 
            this.ขอมลการยมอปกรณToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ขอมลการยมอปกรณToolStripMenuItem.Image")));
            this.ขอมลการยมอปกรณToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ขอมลการยมอปกรณToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ขอมลการยมอปกรณToolStripMenuItem.Name = "ขอมลการยมอปกรณToolStripMenuItem";
            this.ขอมลการยมอปกรณToolStripMenuItem.Size = new System.Drawing.Size(306, 70);
            this.ขอมลการยมอปกรณToolStripMenuItem.Text = "รายงานชื่อพระสงฆ์ที่ไปกิจนิมนต์";
            this.ขอมลการยมอปกรณToolStripMenuItem.Click += new System.EventHandler(this.ขอมลการยมอปกรณToolStripMenuItem_Click);
            // 
            // ขอมลการขอใชสถานทToolStripMenuItem
            // 
            this.ขอมลการขอใชสถานทToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ขอมลการขอใชสถานทToolStripMenuItem.Image")));
            this.ขอมลการขอใชสถานทToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ขอมลการขอใชสถานทToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ขอมลการขอใชสถานทToolStripMenuItem.Name = "ขอมลการขอใชสถานทToolStripMenuItem";
            this.ขอมลการขอใชสถานทToolStripMenuItem.Size = new System.Drawing.Size(306, 70);
            this.ขอมลการขอใชสถานทToolStripMenuItem.Text = "รายงานสถานที่";
            this.ขอมลการขอใชสถานทToolStripMenuItem.Click += new System.EventHandler(this.ขอมลการขอใชสถานทToolStripMenuItem_Click);
            // 
            // รายงานการขอใชสถานทToolStripMenuItem
            // 
            this.รายงานการขอใชสถานทToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("รายงานการขอใชสถานทToolStripMenuItem.Image")));
            this.รายงานการขอใชสถานทToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.รายงานการขอใชสถานทToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.รายงานการขอใชสถานทToolStripMenuItem.Name = "รายงานการขอใชสถานทToolStripMenuItem";
            this.รายงานการขอใชสถานทToolStripMenuItem.Size = new System.Drawing.Size(306, 70);
            this.รายงานการขอใชสถานทToolStripMenuItem.Text = "รายงานการขอใช้สถานที่";
            this.รายงานการขอใชสถานทToolStripMenuItem.Click += new System.EventHandler(this.รายงานการขอใชสถานทToolStripMenuItem_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = global::mork.Properties.Resources.error;
            this.pictureBox3.Location = new System.Drawing.Point(1484, 14);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(41, 39);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 31;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.ปดโปรแกรมToolStripMenuItem_Click_1);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1565, 19);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(56, 53);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 23;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.PictureBox2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::mork.Properties.Resources.monk__1_;
            this.pictureBox1.Location = new System.Drawing.Point(278, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 34);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // ออกจากระบบToolStripMenuItem4
            // 
            this.ออกจากระบบToolStripMenuItem4.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ออกจากระบบToolStripMenuItem4.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.ออกจากระบบToolStripMenuItem4.Image = global::mork.Properties.Resources.logout__1_;
            this.ออกจากระบบToolStripMenuItem4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ออกจากระบบToolStripMenuItem4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ออกจากระบบToolStripMenuItem4.Name = "ออกจากระบบToolStripMenuItem4";
            this.ออกจากระบบToolStripMenuItem4.Size = new System.Drawing.Size(217, 68);
            this.ออกจากระบบToolStripMenuItem4.Text = "ออกจากระบบ";
            this.ออกจากระบบToolStripMenuItem4.Click += new System.EventHandler(this.ออกจากระบบToolStripMenuItem4_Click);
            // 
            // คนหากจนมนตToolStripMenuItem
            // 
            this.คนหากจนมนตToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.คนหากจนมนตToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("คนหากจนมนตToolStripMenuItem.Image")));
            this.คนหากจนมนตToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.คนหากจนมนตToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.คนหากจนมนตToolStripMenuItem.Name = "คนหากจนมนตToolStripMenuItem";
            this.คนหากจนมนตToolStripMenuItem.Size = new System.Drawing.Size(217, 68);
            this.คนหากจนมนตToolStripMenuItem.Text = "รายการกิจนิมนต์";
            this.คนหากจนมนตToolStripMenuItem.Click += new System.EventHandler(this.คนหากจนมนตToolStripMenuItem_Click);
            // 
            // ออกจากระบบToolStripMenuItem2
            // 
            this.ออกจากระบบToolStripMenuItem2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ออกจากระบบToolStripMenuItem2.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.ออกจากระบบToolStripMenuItem2.Image = global::mork.Properties.Resources.logout__1_;
            this.ออกจากระบบToolStripMenuItem2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ออกจากระบบToolStripMenuItem2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ออกจากระบบToolStripMenuItem2.Name = "ออกจากระบบToolStripMenuItem2";
            this.ออกจากระบบToolStripMenuItem2.Size = new System.Drawing.Size(217, 68);
            this.ออกจากระบบToolStripMenuItem2.Text = "ออกจากระบบ";
            this.ออกจากระบบToolStripMenuItem2.Click += new System.EventHandler(this.ออกจากระบบToolStripMenuItem2_Click);
            // 
            // จดการขอมลผใชงานToolStripMenuItem
            // 
            this.จดการขอมลผใชงานToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.จดการขอมลผใชงานToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("จดการขอมลผใชงานToolStripMenuItem.Image")));
            this.จดการขอมลผใชงานToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.จดการขอมลผใชงานToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.จดการขอมลผใชงานToolStripMenuItem.Name = "จดการขอมลผใชงานToolStripMenuItem";
            this.จดการขอมลผใชงานToolStripMenuItem.Size = new System.Drawing.Size(217, 68);
            this.จดการขอมลผใชงานToolStripMenuItem.Text = "จัดการข้อมูลผู้ใช้งาน";
            this.จดการขอมลผใชงานToolStripMenuItem.Click += new System.EventHandler(this.จดการขอมลผใชงานToolStripMenuItem_Click);
            // 
            // ออกจากระบบToolStripMenuItem
            // 
            this.ออกจากระบบToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ออกจากระบบToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.ออกจากระบบToolStripMenuItem.Image = global::mork.Properties.Resources.logout__1_;
            this.ออกจากระบบToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ออกจากระบบToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ออกจากระบบToolStripMenuItem.Name = "ออกจากระบบToolStripMenuItem";
            this.ออกจากระบบToolStripMenuItem.Size = new System.Drawing.Size(217, 68);
            this.ออกจากระบบToolStripMenuItem.Text = "ออกจากระบบ";
            this.ออกจากระบบToolStripMenuItem.Click += new System.EventHandler(this.ออกจากระบบToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.toolStripMenuItem4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem4.Image")));
            this.toolStripMenuItem4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolStripMenuItem4.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(217, 68);
            this.toolStripMenuItem4.Text = "จัดการข้อมูลฆราวาส";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.ToolStripMenuItem4_Click);
            // 
            // จดการการยมอปกรณToolStripMenuItem
            // 
            this.จดการการยมอปกรณToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.จดการการยมอปกรณToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("จดการการยมอปกรณToolStripMenuItem.Image")));
            this.จดการการยมอปกรณToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.จดการการยมอปกรณToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.จดการการยมอปกรณToolStripMenuItem.Name = "จดการการยมอปกรณToolStripMenuItem";
            this.จดการการยมอปกรณToolStripMenuItem.Size = new System.Drawing.Size(217, 68);
            this.จดการการยมอปกรณToolStripMenuItem.Text = "จัดการการยืมอุปกรณ์";
            this.จดการการยมอปกรณToolStripMenuItem.Click += new System.EventHandler(this.จดการการยมอปกรณToolStripMenuItem_Click);
            // 
            // จดการขอมลอปกรณToolStripMenuItem
            // 
            this.จดการขอมลอปกรณToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.จดการขอมลอปกรณToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.จดการขอมลอปกรณToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("จดการขอมลอปกรณToolStripMenuItem.Image")));
            this.จดการขอมลอปกรณToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.จดการขอมลอปกรณToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.จดการขอมลอปกรณToolStripMenuItem.Name = "จดการขอมลอปกรณToolStripMenuItem";
            this.จดการขอมลอปกรณToolStripMenuItem.Size = new System.Drawing.Size(217, 68);
            this.จดการขอมลอปกรณToolStripMenuItem.Text = "จัดการข้อมูลอุปกรณ์";
            this.จดการขอมลอปกรณToolStripMenuItem.Click += new System.EventHandler(this.จดการขอมลอปกรณToolStripMenuItem_Click);
            // 
            // จดการประเภทอปกรณToolStripMenuItem
            // 
            this.จดการประเภทอปกรณToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.จดการประเภทอปกรณToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.จดการประเภทอปกรณToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("จดการประเภทอปกรณToolStripMenuItem.Image")));
            this.จดการประเภทอปกรณToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.จดการประเภทอปกรณToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.จดการประเภทอปกรณToolStripMenuItem.Name = "จดการประเภทอปกรณToolStripMenuItem";
            this.จดการประเภทอปกรณToolStripMenuItem.Size = new System.Drawing.Size(217, 68);
            this.จดการประเภทอปกรณToolStripMenuItem.Text = "จัดการประเภทอุปกรณ์";
            this.จดการประเภทอปกรณToolStripMenuItem.Click += new System.EventHandler(this.จดการประเภทอปกรณToolStripMenuItem_Click);
            // 
            // จดการการคนอปกรณToolStripMenuItem
            // 
            this.จดการการคนอปกรณToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.จดการการคนอปกรณToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("จดการการคนอปกรณToolStripMenuItem.Image")));
            this.จดการการคนอปกรณToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.จดการการคนอปกรณToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.จดการการคนอปกรณToolStripMenuItem.Name = "จดการการคนอปกรณToolStripMenuItem";
            this.จดการการคนอปกรณToolStripMenuItem.Size = new System.Drawing.Size(217, 68);
            this.จดการการคนอปกรณToolStripMenuItem.Text = "จัดการการคืนอุปกรณ์";
            this.จดการการคนอปกรณToolStripMenuItem.Click += new System.EventHandler(this.จดการการคนอปกรณToolStripMenuItem_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.toolStripMenuItem5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem5.Image")));
            this.toolStripMenuItem5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolStripMenuItem5.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(217, 68);
            this.toolStripMenuItem5.Text = "การบริจาคอุปกรณ์";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.ToolStripMenuItem5_Click);
            // 
            // พมพใบยมวสดอปกรณไดToolStripMenuItem
            // 
            this.พมพใบยมวสดอปกรณไดToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.พมพใบยมวสดอปกรณไดToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("พมพใบยมวสดอปกรณไดToolStripMenuItem.Image")));
            this.พมพใบยมวสดอปกรณไดToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.พมพใบยมวสดอปกรณไดToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.พมพใบยมวสดอปกรณไดToolStripMenuItem.Name = "พมพใบยมวสดอปกรณไดToolStripMenuItem";
            this.พมพใบยมวสดอปกรณไดToolStripMenuItem.Size = new System.Drawing.Size(217, 68);
            this.พมพใบยมวสดอปกรณไดToolStripMenuItem.Text = "พิมพ์ใบยืมวัสดุอุปกรณ์ได้";
            this.พมพใบยมวสดอปกรณไดToolStripMenuItem.Click += new System.EventHandler(this.พมพใบยมวสดอปกรณไดToolStripMenuItem_Click);
            // 
            // เขาสระบบToolStripMenuItem
            // 
            this.เขาสระบบToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.เขาสระบบToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("เขาสระบบToolStripMenuItem.Image")));
            this.เขาสระบบToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.เขาสระบบToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.เขาสระบบToolStripMenuItem.Name = "เขาสระบบToolStripMenuItem";
            this.เขาสระบบToolStripMenuItem.Size = new System.Drawing.Size(217, 68);
            this.เขาสระบบToolStripMenuItem.Text = "เข้าสู่ระบบ";
            this.เขาสระบบToolStripMenuItem.Click += new System.EventHandler(this.เขาสระบบToolStripMenuItem_Click);
            // 
            // จดการขอมลระบบToolStripMenuItem
            // 
            this.จดการขอมลระบบToolStripMenuItem.Image = global::mork.Properties.Resources.icons8_web_address_64;
            this.จดการขอมลระบบToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.จดการขอมลระบบToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.จดการขอมลระบบToolStripMenuItem.Name = "จดการขอมลระบบToolStripMenuItem";
            this.จดการขอมลระบบToolStripMenuItem.Size = new System.Drawing.Size(217, 68);
            this.จดการขอมลระบบToolStripMenuItem.Text = "จัดการข้อมูลระบบ";
            this.จดการขอมลระบบToolStripMenuItem.Click += new System.EventHandler(this.จดการขอมลระบบToolStripMenuItem_Click);
            // 
            // ออกจากระบบToolStripMenuItem1
            // 
            this.ออกจากระบบToolStripMenuItem1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.ออกจากระบบToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.ออกจากระบบToolStripMenuItem1.Image = global::mork.Properties.Resources.logout__1_;
            this.ออกจากระบบToolStripMenuItem1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ออกจากระบบToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ออกจากระบบToolStripMenuItem1.Name = "ออกจากระบบToolStripMenuItem1";
            this.ออกจากระบบToolStripMenuItem1.Size = new System.Drawing.Size(211, 68);
            this.ออกจากระบบToolStripMenuItem1.Text = "ออกจากระบบ";
            this.ออกจากระบบToolStripMenuItem1.Click += new System.EventHandler(this.ออกจากระบบToolStripMenuItem1_Click);
            // 
            // จดการขอมลฆราวาสToolStripMenuItem
            // 
            this.จดการขอมลฆราวาสToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.จดการขอมลฆราวาสToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("จดการขอมลฆราวาสToolStripMenuItem.Image")));
            this.จดการขอมลฆราวาสToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.จดการขอมลฆราวาสToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.จดการขอมลฆราวาสToolStripMenuItem.Name = "จดการขอมลฆราวาสToolStripMenuItem";
            this.จดการขอมลฆราวาสToolStripMenuItem.Size = new System.Drawing.Size(211, 68);
            this.จดการขอมลฆราวาสToolStripMenuItem.Text = "จัดการข้อมูลฆราวาส";
            this.จดการขอมลฆราวาสToolStripMenuItem.Click += new System.EventHandler(this.จดการขอมลฆราวาสToolStripMenuItem_Click);
            // 
            // จดการการรบกจนมนตToolStripMenuItem
            // 
            this.จดการการรบกจนมนตToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.จดการการรบกจนมนตToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("จดการการรบกจนมนตToolStripMenuItem.Image")));
            this.จดการการรบกจนมนตToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.จดการการรบกจนมนตToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.จดการการรบกจนมนตToolStripMenuItem.Name = "จดการการรบกจนมนตToolStripMenuItem";
            this.จดการการรบกจนมนตToolStripMenuItem.Size = new System.Drawing.Size(211, 68);
            this.จดการการรบกจนมนตToolStripMenuItem.Text = "จัดการการรับกิจนิมนต์";
            this.จดการการรบกจนมนตToolStripMenuItem.Click += new System.EventHandler(this.จดการการรบกจนมนตToolStripMenuItem_Click);
            // 
            // จดการการขอใชสถานทToolStripMenuItem
            // 
            this.จดการการขอใชสถานทToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.จดการการขอใชสถานทToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("จดการการขอใชสถานทToolStripMenuItem.Image")));
            this.จดการการขอใชสถานทToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.จดการการขอใชสถานทToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.จดการการขอใชสถานทToolStripMenuItem.Name = "จดการการขอใชสถานทToolStripMenuItem";
            this.จดการการขอใชสถานทToolStripMenuItem.Size = new System.Drawing.Size(211, 68);
            this.จดการการขอใชสถานทToolStripMenuItem.Text = "จัดการการขอใช้สถานที่";
            this.จดการการขอใชสถานทToolStripMenuItem.Click += new System.EventHandler(this.จดการการขอใชสถานทToolStripMenuItem_Click);
            // 
            // จดการขอมลToolStripMenuItem
            // 
            this.จดการขอมลToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.จดการขอมลToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.จดการขอมลสถานทToolStripMenuItem1,
            this.จดการขอมลประเภทกจจมนตToolStripMenuItem,
            this.จดการยานพาหนะToolStripMenuItem});
            this.จดการขอมลToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.จดการขอมลToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("จดการขอมลToolStripMenuItem.Image")));
            this.จดการขอมลToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.จดการขอมลToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.จดการขอมลToolStripMenuItem.Name = "จดการขอมลToolStripMenuItem";
            this.จดการขอมลToolStripMenuItem.Size = new System.Drawing.Size(211, 68);
            this.จดการขอมลToolStripMenuItem.Text = "จัดการข้อมูล";
            this.จดการขอมลToolStripMenuItem.Click += new System.EventHandler(this.จดการขอมลToolStripMenuItem_Click);
            // 
            // จดการขอมลสถานทToolStripMenuItem1
            // 
            this.จดการขอมลสถานทToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("จดการขอมลสถานทToolStripMenuItem1.Image")));
            this.จดการขอมลสถานทToolStripMenuItem1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.จดการขอมลสถานทToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.จดการขอมลสถานทToolStripMenuItem1.Name = "จดการขอมลสถานทToolStripMenuItem1";
            this.จดการขอมลสถานทToolStripMenuItem1.Size = new System.Drawing.Size(291, 70);
            this.จดการขอมลสถานทToolStripMenuItem1.Text = "จัดการข้อมูลสถานที่";
            this.จดการขอมลสถานทToolStripMenuItem1.Click += new System.EventHandler(this.จดการขอมลสถานทToolStripMenuItem1_Click);
            // 
            // จดการขอมลประเภทกจจมนตToolStripMenuItem
            // 
            this.จดการขอมลประเภทกจจมนตToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("จดการขอมลประเภทกจจมนตToolStripMenuItem.Image")));
            this.จดการขอมลประเภทกจจมนตToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.จดการขอมลประเภทกจจมนตToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.จดการขอมลประเภทกจจมนตToolStripMenuItem.Name = "จดการขอมลประเภทกจจมนตToolStripMenuItem";
            this.จดการขอมลประเภทกจจมนตToolStripMenuItem.Size = new System.Drawing.Size(291, 70);
            this.จดการขอมลประเภทกจจมนตToolStripMenuItem.Text = "จัดการข้อมูลประเภทกิจจิมนต์";
            this.จดการขอมลประเภทกจจมนตToolStripMenuItem.Click += new System.EventHandler(this.จดการขอมลประเภทกจจมนตToolStripMenuItem_Click);
            // 
            // จดการยานพาหนะToolStripMenuItem
            // 
            this.จดการยานพาหนะToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("จดการยานพาหนะToolStripMenuItem.Image")));
            this.จดการยานพาหนะToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.จดการยานพาหนะToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.จดการยานพาหนะToolStripMenuItem.Name = "จดการยานพาหนะToolStripMenuItem";
            this.จดการยานพาหนะToolStripMenuItem.Size = new System.Drawing.Size(291, 70);
            this.จดการยานพาหนะToolStripMenuItem.Text = "จัดการยานพาหนะ";
            this.จดการยานพาหนะToolStripMenuItem.Click += new System.EventHandler(this.จดการยานพาหนะToolStripMenuItem_Click);
            // 
            // จดการขอมลพระสงฆToolStripMenuItem
            // 
            this.จดการขอมลพระสงฆToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.จดการขอมลพระสงฆToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.จดการขอมลพระสงฆToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("จดการขอมลพระสงฆToolStripMenuItem.Image")));
            this.จดการขอมลพระสงฆToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.จดการขอมลพระสงฆToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.จดการขอมลพระสงฆToolStripMenuItem.Name = "จดการขอมลพระสงฆToolStripMenuItem";
            this.จดการขอมลพระสงฆToolStripMenuItem.Size = new System.Drawing.Size(211, 68);
            this.จดการขอมลพระสงฆToolStripMenuItem.Text = "จัดการข้อมูลพระสงฆ์";
            this.จดการขอมลพระสงฆToolStripMenuItem.Click += new System.EventHandler(this.จดการขอมลพระสงฆToolStripMenuItem_Click);
            // 
            // บรจาคเงนToolStripMenuItem
            // 
            this.บรจาคเงนToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.บรจาคเงนToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("บรจาคเงนToolStripMenuItem.Image")));
            this.บรจาคเงนToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.บรจาคเงนToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.บรจาคเงนToolStripMenuItem.Name = "บรจาคเงนToolStripMenuItem";
            this.บรจาคเงนToolStripMenuItem.Size = new System.Drawing.Size(211, 68);
            this.บรจาคเงนToolStripMenuItem.Text = "การบริจาคเงิน";
            this.บรจาคเงนToolStripMenuItem.Click += new System.EventHandler(this.บรจาคเงนToolStripMenuItem_Click);
            // 
            // คนหาและดขอมลToolStripMenuItem1
            // 
            this.คนหาและดขอมลToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ขอมลผคำประกนToolStripMenuItem,
            this.toolStripMenuItem8,
            this.คนหาขอมลการขอใชสถานทToolStripMenuItem,
            this.toolStripMenuItem9});
            this.คนหาและดขอมลToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.คนหาและดขอมลToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("คนหาและดขอมลToolStripMenuItem1.Image")));
            this.คนหาและดขอมลToolStripMenuItem1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.คนหาและดขอมลToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.คนหาและดขอมลToolStripMenuItem1.Name = "คนหาและดขอมลToolStripMenuItem1";
            this.คนหาและดขอมลToolStripMenuItem1.Size = new System.Drawing.Size(211, 68);
            this.คนหาและดขอมลToolStripMenuItem1.Text = "ค้นหาและดูข้อมูล";
            this.คนหาและดขอมลToolStripMenuItem1.Click += new System.EventHandler(this.คนหาและดขอมลToolStripMenuItem1_Click);
            // 
            // ขอมลผคำประกนToolStripMenuItem
            // 
            this.ขอมลผคำประกนToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("ขอมลผคำประกนToolStripMenuItem.Image")));
            this.ขอมลผคำประกนToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.ขอมลผคำประกนToolStripMenuItem.Name = "ขอมลผคำประกนToolStripMenuItem";
            this.ขอมลผคำประกนToolStripMenuItem.Size = new System.Drawing.Size(490, 70);
            this.ขอมลผคำประกนToolStripMenuItem.Text = "รายงานกิจนิมนต์พระสงฆ์ในวัด(แยกตามบุคคล)";
            this.ขอมลผคำประกนToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ขอมลผคำประกนToolStripMenuItem.Click += new System.EventHandler(this.ขอมลผคำประกนToolStripMenuItem_Click);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.toolStripMenuItem8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem8.Image")));
            this.toolStripMenuItem8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolStripMenuItem8.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(490, 70);
            this.toolStripMenuItem8.Text = "รายงานรายชื่อพระสงฆ์ที่ไปกิจนิมนต์ในวัด(แยกตามงานกิจนิมนต์)";
            this.toolStripMenuItem8.Click += new System.EventHandler(this.ToolStripMenuItem8_Click_1);
            // 
            // คนหาขอมลการขอใชสถานทToolStripMenuItem
            // 
            this.คนหาขอมลการขอใชสถานทToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("คนหาขอมลการขอใชสถานทToolStripMenuItem.Image")));
            this.คนหาขอมลการขอใชสถานทToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.คนหาขอมลการขอใชสถานทToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.คนหาขอมลการขอใชสถานทToolStripMenuItem.Name = "คนหาขอมลการขอใชสถานทToolStripMenuItem";
            this.คนหาขอมลการขอใชสถานทToolStripMenuItem.Size = new System.Drawing.Size(490, 70);
            this.คนหาขอมลการขอใชสถานทToolStripMenuItem.Text = "รายงานข้อมูลการขอใช้สถานที่";
            this.คนหาขอมลการขอใชสถานทToolStripMenuItem.Click += new System.EventHandler(this.คนหาขอมลการขอใชสถานทToolStripMenuItem_Click);
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.toolStripMenuItem9.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem9.Image")));
            this.toolStripMenuItem9.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(490, 70);
            this.toolStripMenuItem9.Text = "ค้นหาข้อมูลสถานที่ในวัด";
            this.toolStripMenuItem9.Click += new System.EventHandler(this.ToolStripMenuItem9_Click);
            // 
            // frmformmain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1386, 787);
            this.Controls.Add(this.menuStrip5);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip6);
            this.Controls.Add(this.menuStrip4);
            this.Controls.Add(this.menuStrip2);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.menuStrip3);
            this.ImeMode = System.Windows.Forms.ImeMode.On;
            this.IsMdiContainer = true;
            this.Name = "frmformmain";
            this.Style = MetroFramework.MetroColorStyle.Yellow;
            this.Text = "ระบบจัดการสำหรับพระสงฆ์";
            this.TransparencyKey = System.Drawing.Color.Empty;
            this.Load += new System.EventHandler(this.Frmformmain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.menuStrip3.ResumeLayout(false);
            this.menuStrip3.PerformLayout();
            this.menuStrip4.ResumeLayout(false);
            this.menuStrip4.PerformLayout();
            this.menuStrip5.ResumeLayout(false);
            this.menuStrip5.PerformLayout();
            this.menuStrip6.ResumeLayout(false);
            this.menuStrip6.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem เขาสระบบToolStripMenuItem;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.MenuStrip menuStrip3;
        private System.Windows.Forms.MenuStrip menuStrip4;
        private System.Windows.Forms.MenuStrip menuStrip5;
        private System.Windows.Forms.MenuStrip menuStrip6;
        private System.Windows.Forms.ToolStripMenuItem ฝายอปกรณToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ฝายอปกรณToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ผดแลระบบToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem เจาอาวาสToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem พระสงฆToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ออกจากระบบToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ออกจากระบบToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ออกจากระบบToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem ออกจากระบบToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem ออกจากระบบToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem จดการขอมลผใชงานToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem จดการขอมลอปกรณToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem จดการการรบกจนมนตToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem จดการการขอใชสถานทToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem จดการการยมอปกรณToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem จดการประเภทอปกรณToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem อนมตรายการกจนมนตToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem อนมตการขอใชสถานทToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem อนมนตการขอใชอปกรณToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ToolStripMenuItem คนหาและดขอมลToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ขอมลผคำประกนToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem จดการการคนอปกรณToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem คนหากจนมนตToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem จดการขอมลฆราวาสToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem จดการขอมลพระสงฆToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem จดการขอมลToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem จดการขอมลสถานทToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem จดการขอมลประเภทกจจมนตToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem จดการยานพาหนะToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem บรจาคเงนToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem คนหาขอมลการขอใชสถานทToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem ขอมลกจนมนตToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ขอมลการยมอปกรณToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ขอมลการขอใชสถานทToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem รายงานการขอใชสถานทToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem พมพใบยมวสดอปกรณไดToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem รายการการยมอกรณToolStripMenuItem;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.ToolStripMenuItem จดการขอมลระบบToolStripMenuItem;
    }
}