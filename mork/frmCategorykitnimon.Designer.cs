﻿namespace mork
{
    partial class frmCategorykitnimon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCategorykitnimon));
            this.btnClears = new System.Windows.Forms.Button();
            this.btnmodify = new System.Windows.Forms.Button();
            this.btndelete = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.TextID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNameF = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.dgvdatag = new System.Windows.Forms.DataGridView();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatag)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClears
            // 
            this.btnClears.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClears.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnClears.Image = global::mork.Properties.Resources.broom;
            this.btnClears.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClears.Location = new System.Drawing.Point(768, 561);
            this.btnClears.Name = "btnClears";
            this.btnClears.Size = new System.Drawing.Size(107, 42);
            this.btnClears.TabIndex = 7;
            this.btnClears.Text = "เคลียร์";
            this.btnClears.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClears.UseVisualStyleBackColor = true;
            this.btnClears.Click += new System.EventHandler(this.Clears_Click);
            // 
            // btnmodify
            // 
            this.btnmodify.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnmodify.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnmodify.Image = global::mork.Properties.Resources.customer_support__1_;
            this.btnmodify.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnmodify.Location = new System.Drawing.Point(600, 561);
            this.btnmodify.Name = "btnmodify";
            this.btnmodify.Size = new System.Drawing.Size(107, 42);
            this.btnmodify.TabIndex = 6;
            this.btnmodify.Text = "แก้ไข";
            this.btnmodify.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnmodify.UseVisualStyleBackColor = true;
            this.btnmodify.Click += new System.EventHandler(this.Modify_Click);
            // 
            // btndelete
            // 
            this.btndelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btndelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btndelete.Image = global::mork.Properties.Resources.x_button__1_;
            this.btndelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btndelete.Location = new System.Drawing.Point(434, 561);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(107, 42);
            this.btndelete.TabIndex = 5;
            this.btndelete.Text = "ลบ";
            this.btndelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btndelete.UseVisualStyleBackColor = true;
            this.btndelete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // btnadd
            // 
            this.btnadd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnadd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btnadd.Image = global::mork.Properties.Resources.plus1;
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnadd.Location = new System.Drawing.Point(263, 561);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(107, 42);
            this.btnadd.TabIndex = 4;
            this.btnadd.Text = "เพิ่ม";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = true;
            this.btnadd.Click += new System.EventHandler(this.Add_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(35, 250);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 20);
            this.label9.TabIndex = 31;
            this.label9.Text = "รายละเอียด:";
            this.label9.Click += new System.EventHandler(this.Label9_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(91, 142);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 20);
            this.label5.TabIndex = 33;
            this.label5.Text = "ID:";
            // 
            // TextID
            // 
            this.TextID.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.TextID.Location = new System.Drawing.Point(132, 135);
            this.TextID.Name = "TextID";
            this.TextID.ReadOnly = true;
            this.TextID.Size = new System.Drawing.Size(79, 24);
            this.TextID.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(67, 190);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 20);
            this.label3.TabIndex = 34;
            this.label3.Text = "ชื่องาน:";
            // 
            // txtNameF
            // 
            this.txtNameF.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtNameF.Location = new System.Drawing.Point(132, 190);
            this.txtNameF.MaxLength = 30;
            this.txtNameF.Name = "txtNameF";
            this.txtNameF.Size = new System.Drawing.Size(264, 24);
            this.txtNameF.TabIndex = 2;
            // 
            // txtAddress
            // 
            this.txtAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtAddress.Location = new System.Drawing.Point(132, 250);
            this.txtAddress.MaxLength = 50;
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(272, 139);
            this.txtAddress.TabIndex = 3;
            // 
            // dgvdatag
            // 
            this.dgvdatag.AllowUserToAddRows = false;
            this.dgvdatag.AllowUserToDeleteRows = false;
            this.dgvdatag.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvdatag.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvdatag.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvdatag.Location = new System.Drawing.Point(24, 88);
            this.dgvdatag.Name = "dgvdatag";
            this.dgvdatag.ReadOnly = true;
            this.dgvdatag.RowHeadersVisible = false;
            this.dgvdatag.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvdatag.Size = new System.Drawing.Size(511, 274);
            this.dgvdatag.TabIndex = 38;
            this.dgvdatag.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Datag_CellMouseUp);
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 0;
            this.bunifuElipse1.TargetControl = this;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.txtID);
            this.groupBox2.Controls.Add(this.dgvdatag);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.groupBox2.Location = new System.Drawing.Point(501, 81);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(561, 403);
            this.groupBox2.TabIndex = 38;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "รายการ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(25, 40);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(36, 33);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 42;
            this.pictureBox1.TabStop = false;
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(74, 46);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(383, 24);
            this.txtID.TabIndex = 8;
            this.txtID.TextChanged += new System.EventHandler(this.TxtID_TextChanged);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("FC Lamoon", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(597, 540);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 18);
            this.label2.TabIndex = 40;
            this.label2.Text = "*เลือกข้อมูลที่จะแก้ไข";
            // 
            // frmCategorykitnimon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1117, 669);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtAddress);
            this.Controls.Add(this.txtNameF);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnClears);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnmodify);
            this.Controls.Add(this.TextID);
            this.Controls.Add(this.btndelete);
            this.Controls.Add(this.btnadd);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCategorykitnimon";
            this.Padding = new System.Windows.Forms.Padding(20, 60, 20, 30);
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Style = MetroFramework.MetroColorStyle.Yellow;
            this.Text = "จัดการประเภทข้อมูลกิจนิมนต์";
            this.Load += new System.EventHandler(this.Category_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatag)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClears;
        private System.Windows.Forms.Button btnmodify;
        private System.Windows.Forms.Button btndelete;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TextID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNameF;
        private System.Windows.Forms.DataGridView dgvdatag;
        private System.Windows.Forms.TextBox txtAddress;
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label label2;
    }
}