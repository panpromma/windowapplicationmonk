﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
namespace mork
{
    public partial class frmapprove : MetroFramework.Forms.MetroForm
    {
        public string status { get; set; }
        
        public frmapprove()
        {
            InitializeComponent();
            
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        SqlConnection conn;
        public int KitnmonID { get; set; }//รหัสกิจนิมนต์
        public string Address { get; set; }//ที่อยู่
        public string Time { get; set; }//เวลา
        public string date { get; set; }//วันที่
        public string Approve { get; set; }//สถานะ
        public string Phone { get; set; }//เบอร์โทร
        public string Namep { get; set; }//ชื่อผู้รับ
        public string names { get; set; }//ชื่อฆราวาส
        public string WorkName { get; set; }//ชื่อ

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void Frmapprove_Load(object sender, EventArgs e)
        {
            
            conn = new ConnecDB().SqlStrCon();
            ShowData();
        }
        private void ShowData()
        {
            string sql = "select k.KitnmonID as รหัสกิจนิมนต์ ,k.Address as ที่อยู่ ,c.Workname as ชื่องาน,TimeD as เวลา, dbo.mydate(dateD) as วันที่ , case  when k.Approve = 0 then 'ยังไม่อนุมัติ' when k.Approve = 1 then 'อนุมัติแล้ว' end as สถานะ ,k.Phone as เบอร์โทร ,p.Name as ชื่อผู้รับกิจนิมต์, s.name as ชื่อฆราวาส   ,c.WorkName as ชื่อกิจนิมนต์ from Kitnmons k inner join Personnels p on k.UserID = p.UserID inner join CategoryKitnimons c on k.CategoryKitID = c.CategoryKitID  inner join Seculars s on k.SecularID = s.SecularID where k.Approve=0 order by KitnmonID desc";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdata1.DataSource = ds.Tables[0];
            dgvdata1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }
        

        

       

        private void Dgvdata1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            var x = dgvdata1.CurrentRow.Cells;
            KitnmonID = Convert.ToInt16(x[0].Value.ToString());
            Address = x[1].Value.ToString();
            Time = x[2].Value.ToString();
            date = x[3].Value.ToString();
            Approve = x[4].Value.ToString();
            Phone = x[5].Value.ToString();
            Namep = x[6].Value.ToString();
            names = x[7].Value.ToString();
            WorkName = x[8].Value.ToString();







            frmaddkitnimon f = new frmaddkitnimon();
            f.KitnmonID = this.KitnmonID;
            f.Address = this.Address;
            f.Time = this.Time;
            f.date = this.date;
            f.Approve = this.Approve;
            f.Phone = this.Phone;
            f.Namep = this.Namep;
            f.names = this.names;
            f.WorkName = this.WorkName;
        
            f.ShowDialog();
            ShowData();
        }

        private void Btnre_Click(object sender, EventArgs e)
        {
          
        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            ShowData(txtID.Text.ToString());
        }
        private void ShowData(string name)
        {
            if (txtID.Text != "")
            {
                string sql = "select k.KitnmonID as รหัส ,k.Address as ที่อยู่,k.Time as เวลา,dbo.mydate(k.date) as วันเดือนปี , case  when k.Approve = 0 then 'ยังไม่อนุมัติ' when k.Approve = 1 then 'อนุมัติแล้ว' end as สถานะ ,k.Phone as เบอร์โทร ,p.Name as ชื่อผู้รับกิจนิมต์, s.name as ชื่อฆราวาส   ,c.WorkName as ชื่อกิจนิมนต์ from Kitnmons k inner join Personnels p on k.UserID = p.UserID inner join CategoryKitnimons c on k.CategoryKitID = c.CategoryKitID  inner join Seculars s on k.SecularID = s.SecularID" +
                    " where date like '%' + @name + '%'" +
                    "or s.name like '%' + @name + '%'" +
                    "or p.Name like '%' + @name + '%'" +
                    "or c.WorkName like '%' + @name + '%'" +
                    " order by KitnmonID desc";





                SqlCommand com = new SqlCommand(sql, conn);
                com.Parameters.AddWithValue("@name", name);
                SqlDataAdapter da = new SqlDataAdapter(com);
                DataSet ds = new DataSet();
                da.Fill(ds);
                dgvdata1.DataSource = ds.Tables[0];

                dgvdata1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            }
            else
            {
                ShowData();
            }
        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {

        }
    }
}
