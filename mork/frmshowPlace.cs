﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace mork
{
    public partial class frmshowPlace : MetroFramework.Forms.MetroForm
    {
      
        SqlConnection conn;
        public frmshowPlace()
        {
            InitializeComponent();
           
        }

        private void FrmshowPlace_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            ShowData();

        }
        private void ShowData()
        {
            string sql = "select  PlaceID , PlaceName , Width , Length , Description , case when Status=0 then 'ไม่ว่าง' when Status=1 then 'ว่าง' end as สถานะ  from Places";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
            dgvdatag.Columns[1].HeaderCell.Value = "ชื่อสถาที่";
            dgvdatag.Columns[2].HeaderCell.Value = "ความกว้าง";
            dgvdatag.Columns[3].HeaderCell.Value = "ความยาว";
            dgvdatag.Columns[4].HeaderCell.Value = "รายละเอียด";
        }

        private void Dgvdatag_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtID_TextChanged_1(object sender, EventArgs e)
        {
            ShowData1(txtID.Text.ToString());
        }
        
        public int PlaceID { get; set; }
        public string PlaceName { get; set; }
        public string Width1 { get; set; }
        public string Length1 { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        private void ShowData1(string name)
        {
            if (txtID.Text != "")
            {
                string sql = "select  PlaceID , PlaceName , Width , Length , Description , case when Status=0 then 'ไม่ว่าง' when Status=1 then 'ว่าง' end as สถานะ  from Places" +
                    " where PlaceName like '%' + @name + '%'";





                SqlCommand com = new SqlCommand(sql, conn);
                com.Parameters.AddWithValue("@name", name);
                SqlDataAdapter da = new SqlDataAdapter(com);
                DataSet ds = new DataSet();
                da.Fill(ds);
                dgvdatag.DataSource = ds.Tables[0];
                dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
                dgvdatag.Columns[1].HeaderCell.Value = "ชื่อสถาที่";
                dgvdatag.Columns[2].HeaderCell.Value = "ความกว้าง";
                dgvdatag.Columns[3].HeaderCell.Value = "ความยาว";
                dgvdatag.Columns[4].HeaderCell.Value = "รายละเอียด";
            }
            else
            {
                ShowData();
            }
        }

        private void Dgvdatag_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var x = dgvdatag.CurrentRow.Cells;
            PlaceID = Convert.ToInt16(x[0].Value.ToString());
            PlaceName = x[1].Value.ToString();
            Width1 = x[2].Value.ToString();
            Length1 = x[3].Value.ToString();
            Description = x[4].Value.ToString();
            Status = x[5].Value.ToString();








            frmshowworkPlace f = new frmshowworkPlace();
            f.PlaceID = this.PlaceID;
            f.PlaceName = this.PlaceName;
            f.Width1 = this.Width1;
            f.Length1 = this.Length1;
            f.Description = this.Description;
            f.Status = this.Status;
       
            f.ShowDialog();
            ShowData();
        }

        private void CboClass_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
