﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
namespace mork
{
    public partial class frmselectequipment : MetroFramework.Forms.MetroForm
    {
        public frmselectequipment()
        {
            InitializeComponent();
        }
        public int UserID { get; set; }
        public string namef { get; set; }
        public int number { get; set; }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        SqlConnection conn;
        private void Dgvdatag_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
           
            var x = dgvdatag.CurrentRow.Cells;
            UserID = Convert.ToInt16(x["EquipmentID"].Value);

            namef = Convert.ToString(x["Equipmenname"].Value);
            number = Convert.ToInt16(x["Numbertreasury"].Value);

            this.Close();
        }

        private void Frmselectequipment_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            ShowData();
        }
        private void ShowData()
        {
            string sql = "select EquipmentID ,case when Status=0 then 'ไม่มี' when Status=1 then 'มี' end as สถานะการอนุมัติ , Equipmenname ,Numbertreasury,e.Description ,cs.Description from Equipments e inner join Categoryequipments cs on e.CategorymID=cs.CategorymID";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
            dgvdatag.Columns[2].HeaderCell.Value = "ชื่ออุปกรณ์";
            dgvdatag.Columns[3].HeaderCell.Value = "จำนวน";
            dgvdatag.Columns[4].HeaderCell.Value = "รายละเอียด";
            dgvdatag.Columns[5].HeaderCell.Value = "ประเภท";
        }

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            namef = "";
            this.Close();
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
          
        }
        private void ShowData(string name)
        {
            string sql = "select EquipmentID ,case when Status=0 then 'ไม่มี' when Status=1 then 'มี' end as สถานะการอนุมัติ , Equipmenname ,Numbertreasury,e.Description ,cs.Description from Equipments e inner join Categoryequipments cs on e.CategorymID=cs.CategorymID where Equipmenname like @Equipmenname";
            SqlCommand comm = new SqlCommand(sql, conn);
            comm.Parameters.AddWithValue("@Equipmenname", name + "%");
            SqlDataAdapter da = new SqlDataAdapter(comm);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
            dgvdatag.Columns[2].HeaderCell.Value = "ชื่ออุปกรณ์";
            dgvdatag.Columns[3].HeaderCell.Value = "จำนวน";
            dgvdatag.Columns[4].HeaderCell.Value = "รายละเอียด";
            dgvdatag.Columns[5].HeaderCell.Value = "ประเภท";
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            namef = "";
            this.Close();
        }

        private void TxtID_TextChanged_1(object sender, EventArgs e)
        {
            ShowData(txtID.Text.ToString());
        }
    }
}
