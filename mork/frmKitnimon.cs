﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace mork
{
    public partial class frmKitnimon : MetroFramework.Forms.MetroForm
    {
        public int ID { get; set; }
        public string namef { get; set; }
        SqlConnection conn;
        SqlTransaction tr;
        SqlCommand comm;
        
        public frmKitnimon()
        {
            
            InitializeComponent();
           
            this.dtpImportTime.CustomFormat = "HH:mm ";
            this.dtpImportTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpImportTime.ShowUpDown = true;
           
            lsvFormat();
            lsvFormat1();


            //if (bFullScreen == false)
            //{
            //    this.FormBorderStyle = FormBorderStyle.None;
            //    this.WindowState = FormWindowState.Maximized;
                
            //    bFullScreen = true;
            //}
            //else
            //{
            //    this.FormBorderStyle = FormBorderStyle.Sizable;
            //    this.WindowState = FormWindowState.Maximized;//  .Maximized;
            //    bFullScreen = true;
            //}
       
        }



        private void DateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            //MessageBox.Show(dateTimePicker1.Value.TimeOfDay.ToString());
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(dateTimePicker1.Value.ToString("yyyy-MM-dd"));
        }

        private void DateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }
       
        public int UserID;
        public string name;
        private void FrmKitnimon_Load(object sender, EventArgs e)
        {

           
            conn = new ConnecDB().SqlStrCon();
            conn.Open();
            setcboCategorykitnimonm();
            ShowData();
            setcboVehicles();


            string id = frmLogin.eID.ToString();



            string name = frmLogin.name;
            lblID.Text = id;
            lblName.Text = name;









        }

        private void lsvFormat()
        {
            //int counter = 0;
            //ListViewItem item = lsvShow.Items.Add("ลำดับที่",(++counter).ToString(),70);

            
            lsvShow.Columns.Add("รหัส", 50, HorizontalAlignment.Right);
            lsvShow.Columns.Add("ชื่อพระ", 250, HorizontalAlignment.Left);
            
            
            lsvShow.View = View.Details;
            lsvShow.GridLines = true;
            lsvShow.FullRowSelect = true;
            


        }
        private void lsvFormat1()
        {
            //int counter = 0;
            //ListViewItem item = lsvShow.Items.Add("ลำดับที่",(++counter).ToString(),70);


            lsvVehicle.Columns.Add("รหัสรถ", 50, HorizontalAlignment.Right);
            lsvVehicle.Columns.Add("ยี่ห้อ", 250, HorizontalAlignment.Left);
           


            lsvVehicle.View = View.Details;
            lsvVehicle.GridLines = true;
            lsvVehicle.FullRowSelect = true;

            

        }
        private void setcboCategorykitnimonm()
        {
            string sql = "select CategoryKitID ,WorkName from CategoryKitnimons order by CategoryKitID";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);

            cboWorkname.DataSource = ds.Tables[0];
            cboWorkname.DisplayMember = "WorkName";
            cboWorkname.ValueMember = "CategoryKitID";
        }


       
        private void setcboVehicles()
        {
            string sql = "select Vehiclecategory,VehicleID from Vehicles";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);

            cboVehiclesname.DataSource = ds.Tables[0];
            cboVehiclesname.DisplayMember = "Vehiclecategory";
            cboVehiclesname.ValueMember = "VehicleID";

        }
        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            frmshowlistkin f = new frmshowlistkin();
            
            f.Show();
            

        }



        private void Btna_Click(object sender, EventArgs e)
        {
            frmshowSecular f = new frmshowSecular();

            f.ShowDialog();

            txtSecularID.Text = f.SecularID.ToString();
            txtPhone.Text = f.Phone.ToString();
            textname.Text = f.NameF.ToString();



        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
            //frmshowPersonnel f = new frmshowPersonnel();

          
            if (txtnamemonk.Text.Trim() == "")
            {
                MessageBox.Show("เลือกข้อมูล");
            }


            ListViewItem lvi;
            int i = 0 ;
            string tmpProductID;
            for (i = 0; i <= lsvShow.Items.Count - 1; i++)
            {
                tmpProductID =(lsvShow.Items[i].SubItems[1].Text);

                if (txtnamemonk.Text.Trim() == tmpProductID)
                {
                    if (MessageBox.Show("มีรายการซ้ำอยู่ ต้องการเพิ่มอีกหรือไม่", "ผิดพลาด") == DialogResult.Yes)
                    {
                        txtnamemonk.Focus();
                        txtnamemonk.SelectAll();
                        break;
                    }

                    else
                    {
                        return;
                    }

                }
                

            }

            string[] anyData;
            anyData = new string[] { txtIDmonk.Text.ToString(), txtnamemonk.Text.ToString()};
            lvi = new ListViewItem(anyData);
            lsvShow.Items.Add(lvi);

            btnseve.Enabled = true;
            cboWorkname.Focus();
          
        }

        private void TxtSecularID_TextChanged(object sender, EventArgs e)
        {

        }

        private void BtnClear_Click_1(object sender, EventArgs e)
        {
            lsvShow.Clear();
            lsvVehicle.Clear();

            clearfrom();
            lsvFormat();
            lsvFormat1();


        }
        private void clearfrom()
        {
            cboWorkname.Text = "";
            txtAddress.Text = "";
            txtPhone.Text = "";
            textname.Text = "";
            txtSecularID.Text = "";
            txtIDmonk.Text = "";
            cboVehiclesname.Text = "";

        }
        private void Btnseve_Click(object sender, EventArgs e)
        {
            int ID = 0;

            if (txtPhone.Text.Trim() == "" || txtAddress.Text.Trim() == "" || txtSecularID.Text.Trim() == "")
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน");
            }
           
           

            if (lsvShow.Items.Count > 0)
            {
                if (MessageBox.Show("ต้องการบันทึกรายการสั่งซื้อหรือไม่", "กรุณายินยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                 {
                    tr = conn.BeginTransaction();

                    string sqlEmpBills = "insert into Kitnmons (Address,Time,date,Approve,Phone,UserID,SecularID,CategoryKitID)"
                                       + " values (@Address,@Time,@date,@Approve,@Phone,@UserID,@SecularID,@CategoryKitID )";

                    comm = new SqlCommand(sqlEmpBills, conn, tr);

                    comm.Parameters.Add("@Address", SqlDbType.NVarChar).Value = txtAddress.Text.ToString();
                    comm.Parameters.AddWithValue("@Time", dtpImportTime.Value.TimeOfDay) ;
                    comm.Parameters.Add("@date", SqlDbType.Date).Value = dtpImportDate.Value;
                    comm.Parameters.Add("@Approve", SqlDbType.Int).Value = Convert.ToInt16("0");
                    comm.Parameters.Add("@Phone", SqlDbType.NVarChar).Value =txtPhone.Text.ToString();
                    comm.Parameters.Add("@UserID", SqlDbType.Int).Value = Convert.ToInt16(lblID.Text);
                    comm.Parameters.Add("@SecularID", SqlDbType.Int).Value = Convert.ToInt16(txtSecularID.Text);
                    comm.Parameters.Add("@CategoryKitID", SqlDbType.Int).Value = Convert.ToInt16(cboWorkname.SelectedValue);
                   



                    comm.ExecuteNonQuery();


                    string sql = "Select top 1 KitnmonID from Kitnmons order by KitnmonID desc";
                    SqlCommand comm1 = new SqlCommand(sql, conn, tr);
                    SqlDataReader dr;
                    dr = comm1.ExecuteReader();
                    if (dr.HasRows)
                    {
                        dr.Read();
                        ID = dr.GetInt32(dr.GetOrdinal("KitnmonID"));
                    }
                    else
                    {
                        ID = 1;
                    }
                    dr.Close();
                    int i = 0;
                    for (i = 0; i <= lsvShow.Items.Count - 1; i++)
                    {
                        if (lblName.Text.Trim() == "")
                        {
                            MessageBox.Show("กรุณาป้อนรหัสสินค้า", "ผิดพลาด");
                            lblName.Focus();
                        }

                        string sql2 = "insert into Kitnimonlists (UserID,KitnmonID) values(@UserID, @KitnmonID)";
                       
                        SqlCommand comm = new SqlCommand(sql2, conn , tr);

                        

                        comm.Parameters.AddWithValue("@UserID", Convert.ToInt16(lsvShow.Items[i].SubItems[0].Text));
                        comm.Parameters.AddWithValue("@KitnmonID", ID);

                        //string sql3 = "insert into Usevehicless (KitnmonID,VehicleID) values(@KitnmonID,@VehicleID)";

                        //SqlCommand comm2 = new SqlCommand(sql3, conn, tr);



                        
                        //comm.Parameters.AddWithValue("@KitnmonID", ID);
                        //comm.Parameters.AddWithValue("@UserID", Convert.ToInt16(lsvShow.Items[i].SubItems[0].Text));







                        comm.ExecuteNonQuery();


                    }
                   
                    for (i = 0; i <= lsvVehicle.Items.Count - 1; i++)
                    {
                        

                        string sql3 = "insert into Usevehicless (VehicleID,KitnmonID) values(@VehicleID, @KitnmonID)";
                      
                        SqlCommand comm = new SqlCommand(sql3, conn, tr);



                        comm.Parameters.AddWithValue("@VehicleID", Convert.ToInt16(lsvVehicle.Items[i].SubItems[0].Text));
                        comm.Parameters.AddWithValue("@KitnmonID", ID);

                       


                        comm.ExecuteNonQuery();


                    }
                   
                }
                tr.Commit();
                MessageBox.Show("บันทึกรายการเรียบร้อยแล้ว", "ผลการทำงาน");
                ShowData();
                btnClear.PerformClick();
                lsvFormat();
            }
        }

        private void Btnadd2_Click(object sender, EventArgs e)
        {
            if (cboWorkname.Text.Trim() == "")
            {
                cboWorkname.Focus(); return;
            }

            ListViewItem lvi2;
            int i = 0;
            string tmpProductID;
            for (i = 0; i <= lsvVehicle.Items.Count - 1; i++)
            {
                tmpProductID = (lsvVehicle.Items[i].SubItems[1].Text);

                if (cboVehiclesname.Text.Trim() == tmpProductID)
                {
                    if (MessageBox.Show("มีรายการซ้ำอยู่ ต้องการเพิ่มอีกหรือไม่", "ผิดพลาด") == DialogResult.Yes)
                    {
                        cboVehiclesname.Focus();
                        cboVehiclesname.SelectAll();
                        break;
                    }

                    else
                    {
                        return;
                    }

                }
            }
            string[] anyData;
            anyData = new string[] { cboVehiclesname.SelectedValue.ToString(), cboVehiclesname.Text };
            lvi2 = new ListViewItem(anyData);
            lsvVehicle.Items.Add(lvi2);

            btnseve.Enabled = true;
            cboWorkname.Focus();
           
        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show(dtpImportTime.Value.TimeOfDay.ToString());
            }

        private void Button1_Click_2(object sender, EventArgs e)
        {
            frmshowPersonnel f = new frmshowPersonnel();
            
                f.ShowDialog();
                //txtIDmonk.Text = f.UserID.ToString();
                //txtnamemonk.Text = f.namef.ToString();
          

            
        }

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        

        private void BarraTitulo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Label6_Click(object sender, EventArgs e)
        {

        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Button2_Click(object sender, EventArgs e)
        {

        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Txtnamemonk_TextChanged(object sender, EventArgs e)
        {

        }

        private void PictureBox1_Click_1(object sender, EventArgs e)
        {

        }
        private void ShowData()
        {
            string sql = "select p.UserID , p.name from   Personnels p full outer join Kitnimonlists ks on p.UserID=ks.UserID where ks.KitnmonID is null and Department='พระสงฆ์'";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
            dgvdatag.Columns[1].HeaderCell.Value = "ชื่อสกุล";
        }

        private void Dgvdatag_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            var x = dgvdatag.CurrentRow.Cells;


            txtIDmonk.Text = x["UserID"].Value.ToString();

            txtnamemonk.Text = x["Name"].Value.ToString();
        }

        private void LsvShow_DoubleClick(object sender, EventArgs e)
        {
            int i = 0;
            for (i=0;i<= lsvShow.SelectedItems.Count-1;i++)
            {
                ListViewItem lsv = lsvShow.SelectedItems[i];
                lsvShow.Items.Remove(lsv);

            }
        }

        private void LsvVehicle_DoubleClick(object sender, EventArgs e)
        {
            int i = 0;
            for (i = 0; i <= lsvVehicle.SelectedItems.Count - 1; i++)
            {
                ListViewItem lsv = lsvVehicle.SelectedItems[i];
                lsvVehicle.Items.Remove(lsv);

            }
        }

        private void Dgvdatag_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void GroupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void LsvVehicle_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
