﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Linq;
using System.Data.SqlClient;
namespace mork
{
    public partial class RePlacerequests : DevExpress.XtraReports.UI.XtraReport
    {
        public RePlacerequests()
        {
            InitializeComponent();
        }
        DataClasses1DataContext db;
        public int ID;
        public string Time1;
        public string Date { get; set; }

        private void RePlacerequests_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            db = new DataClasses1DataContext();

            var v = from k in db.Placerequests
                    join p in db.Places on k.PlaceID equals p.PlaceID
                    join ps in db.Personnels on k.UserID equals ps.UserID
                    join s in db.Seculars on k.SecularID equals s.SecularID
                    where k.RequestID == ID

                    //orderby f.Name
                    select new
                    {
                        k.RequestID,
                        //k.Date ,
                        //k.Time,
                        k.Description,
                        k.Phone,
                        p.PlaceName,
                        ps.Name,
                        s.name


                    };

            this.DataSource = v;
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //xrLabel7.Text = Convert.ToString(this.GetCurrentColumnValue("RequestID"));
            //xrLabel8.Text = Convert.ToString(this.GetCurrentColumnValue("Date"));
            //xrLabel9.Text = Time1;
            //xrLabel10.Text = Convert.ToString(this.GetCurrentColumnValue("Description"));
            //xrRichText5.Text = Convert.ToString(this.GetCurrentColumnValue("Phone"));
            //xrRichText6.Text = Convert.ToString(this.GetCurrentColumnValue("PlaceName"));
            //xrRichText7.Text = Convert.ToString(this.GetCurrentColumnValue("Name"));
            //xrRichText8.Text = Convert.ToString(this.GetCurrentColumnValue("name"));
        }

        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel7.Text = Convert.ToString(this.GetCurrentColumnValue("RequestID"));
            xrLabel8.Text = Date;
            xrLabel9.Text = Time1;
            xrLabel10.Text = Convert.ToString(this.GetCurrentColumnValue("Description"));
            xrRichText5.Text = Convert.ToString(this.GetCurrentColumnValue("Phone"));
            xrRichText6.Text = Convert.ToString(this.GetCurrentColumnValue("PlaceName"));
            xrRichText7.Text = Convert.ToString(this.GetCurrentColumnValue("Name"));
            xrRichText8.Text = Convert.ToString(this.GetCurrentColumnValue("name"));
        }
    }
}
