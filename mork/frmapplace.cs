﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
namespace mork
{
    public partial class frmapplace : MetroFramework.Forms.MetroForm
    {
      
        public int RequestID { get; set; }//รหัสกิจนิมนต์
        public string Date { get; set; }//ที่อยู่
        public string Time { get; set; }//เวลา
        public string Description { get; set; }//วันที่
        public string Phone { get; set; }//สถานะ
        public string PlaceName { get; set; }//เบอร์โทร
        public string Namep { get; set; }//ชื่อผู้รับ
        public string names { get; set; }//ชื่อฆราวาส
       
        public frmapplace()
        {
            InitializeComponent();
            
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        SqlConnection conn;
        private void Frmapplace_Load(object sender, EventArgs e)
        {
            
            conn = new ConnecDB().SqlStrCon();
            ShowData();
        }

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void ShowData()
        {
            string sql = "select ps.RequestID,dbo.mydate(ps.Date) , ps.Time ,ps.Description ,ps.Phone,case when Approve=0 then 'ยังไม่อนุมัติ' when Approve=1 then 'อนุมัติ ' end as สถานะการอนุมัติ  , p.PlaceName ,Personnels.Name ,s.name from Placerequests ps  inner join Places p on ps.PlaceID = p.PlaceID   inner join Personnels on ps.UserID = Personnels.UserID inner join Seculars s on ps.SecularID = s.SecularID where Approve =0 order by ps.RequestID desc";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
            dgvdatag.Columns[1].HeaderCell.Value = "วันที่";
            dgvdatag.Columns[2].HeaderCell.Value = "เวลา";
            dgvdatag.Columns[3].HeaderCell.Value = "รายละเอียด";
            dgvdatag.Columns[4].HeaderCell.Value = "เบอร์โทร";
            dgvdatag.Columns[6].HeaderCell.Value = "ชื่อสถานที่";
            dgvdatag.Columns[7].HeaderCell.Value = "ชื่อผู้รับงาน";
            dgvdatag.Columns[8].HeaderCell.Value = "ชื่อฆราวาส";
        }

        
        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void Dgvdata1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
           


            
        }

        private void Btnapprove_Click(object sender, EventArgs e)
        {
            
        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Dgvdata1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var x = dgvdatag.CurrentRow.Cells;
            RequestID = Convert.ToInt16(x[0].Value.ToString());
            Date = x[1].Value.ToString();
            Time = x[2].Value.ToString();
            Description = x[3].Value.ToString();
            Phone = x[4].Value.ToString();
            PlaceName = x[6].Value.ToString();
            Namep = x[7].Value.ToString();
            names = x[8].Value.ToString();

            frmaddplace1 f = new frmaddplace1();
            f.RequestID = this.RequestID;
            f.Date = this.Date;
            f.Time = this.Time;
            f.Description = this.Description;
            
            f.Phone = this.Phone;
            f.PlaceName = this.PlaceName;
            f.Namep = this.Namep;
            f.names = this.names;

            f.ShowDialog();
            ShowData();
        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            ShowData(txtID.Text.ToString());
        }
        private void ShowData(string name)
        {
            if (txtID.Text != "")
            {
                string sql = "select ps.RequestID,dbo.mydate(ps.Date) , ps.Time ,ps.Description ,ps.Phone,case when Approve=0 then 'ยังไม่อนุมัติ' when Approve=1 then 'อนุมัติ ' end as สถานะการอนุมัติ  , p.PlaceName ,Personnels.Name ,s.name from Placerequests ps  inner join Places p on ps.PlaceID = p.PlaceID   inner join Personnels on ps.UserID = Personnels.UserID inner join Seculars s on ps.SecularID = s.SecularID" +
                    " where Approve =0 " +
                    " and Date like '%' + @name + '%'" +
                    "or p.PlaceName like '%' + @name + '%'" +
                    "or Personnels.Name like '%' + @name + '%'" +
                    "or s.name like '%' + @name + '%'" +
                    " order by ps.RequestID desc";





                SqlCommand com = new SqlCommand(sql, conn);
                com.Parameters.AddWithValue("@name", name);
                SqlDataAdapter da = new SqlDataAdapter(com);
                DataSet ds = new DataSet();
                da.Fill(ds);
                dgvdatag.DataSource = ds.Tables[0];
                dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
                dgvdatag.Columns[1].HeaderCell.Value = "วันที่";
                dgvdatag.Columns[2].HeaderCell.Value = "เวลา";
                dgvdatag.Columns[3].HeaderCell.Value = "รายละเอียด";
                dgvdatag.Columns[4].HeaderCell.Value = "เบอร์โทร";
                dgvdatag.Columns[6].HeaderCell.Value = "ชื่อสถานที่";
                dgvdatag.Columns[7].HeaderCell.Value = "ชื่อผู้รับงาน";
                dgvdatag.Columns[8].HeaderCell.Value = "ชื่อฆราวาส";
                dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            }
            else
            {
                ShowData();
            }
        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {

        }
    }
}
