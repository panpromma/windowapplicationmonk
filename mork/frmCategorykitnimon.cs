﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace mork
{
    public partial class frmCategorykitnimon : MetroFramework.Forms.MetroForm
    {
        
        public frmCategorykitnimon()
        {
            InitializeComponent();

           
        }
        SqlConnection conn;
        
        private void Label9_Click(object sender, EventArgs e)
        {

        }

        private void Category_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            
            ShowData();
           
        }

        private void ShowData()
        {

            string sql = "select * from CategoryKitnimons";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            showskin();
        }
        private void showskin()
        {
            dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
            dgvdatag.Columns[1].HeaderCell.Value = "ชื่องาน";
            dgvdatag.Columns[2].HeaderCell.Value = "รายละเอียด";
           

            
            //dgvdatag.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            //dgvdatag.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            //dgvdatag.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            //dgvdatag.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            //dgvdatag.BackgroundColor = Color.White;

            //dgvdatag.EnableHeadersVisualStyles = false;
            //dgvdatag.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            //dgvdatag.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            //dgvdatag.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
        }

        private void Add_Click(object sender, EventArgs e)
        {
            if (txtNameF.Text.Trim() == "" || txtAddress.Text.Trim() == "" )
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน");
            }

            else
            {

            
            string sql = "insert into CategoryKitnimons values( @WorkName,@Description)";
            SqlCommand comm = new SqlCommand(sql, conn);





            comm.Parameters
                .AddWithValue("@WorkName", txtNameF.Text.Trim());

            comm.Parameters
                .AddWithValue("@Description", txtAddress.Text.Trim());


            if (conn.State == ConnectionState.Closed) conn.Open();
            comm.ExecuteNonQuery();
            conn.Close();
            clearfrom();
            ShowData();
            }
        }

        private void clearfrom()
        {
            TextID.Text = "";
            txtNameF.Text = "";
            txtAddress.Text = "";
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("!!!!คณุต้องการจะลบใช่ไหม?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {

                string sql = "delete from CategoryKitnimons where  CategoryKitID = @CategoryKitID";
                SqlCommand comm = new SqlCommand(sql, conn);



                comm.Parameters

                    .AddWithValue("@CategoryKitID", TextID.Text.Trim());

                try
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                    clearfrom();
                    ShowData();

                }
                catch 
                {
                    MessageBox.Show("ไม่สามารถลบข้มูลได้", "Erorr");

                    return;
                }
            }
        }

        private void Datag_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            var x = dgvdatag.CurrentRow.Cells;


            TextID.Text = x["CategoryKitID"].Value.ToString();
            txtNameF.Text = x["WorkName"].Value.ToString();
            txtAddress.Text = x["Description"].Value.ToString();
      
        }

        private void Clears_Click(object sender, EventArgs e)
        {
            txtNameF.Text = "";
            txtAddress.Text = "";
            TextID.Text = "";
        }

        private void Modify_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("!!!!คณุต้องการจะแก้ไขไหม?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {


                string sql = "update CategoryKitnimons set   WorkName=@WorkName,Description=@Description   where  CategoryKitID=@CategoryKitID";
                SqlCommand comm = new SqlCommand(sql, conn);


                comm.Parameters
                    .AddWithValue("@CategoryKitID", TextID.Text.Trim());

                comm.Parameters
                    .AddWithValue("@WorkName", txtNameF.Text.Trim());

                comm.Parameters
                    .AddWithValue("@Description", txtAddress.Text.Trim());
               

                try
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                    clearfrom();
                    ShowData();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            showdata(txtID.Text.ToString());
        }
        private void showdata(string text)
        {
            string sql = "select CategoryKitID , WorkName , Description from CategoryKitnimons"
                + " where WorkName like '%' + @strName + '%'";
            SqlCommand com = new SqlCommand(sql, conn);
            com.Parameters.AddWithValue("@strName", text);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            showskin();
        }
    }
}
