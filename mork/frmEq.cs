﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraReports.UI;
namespace mork
{
    public partial class frmEq : MetroFramework.Forms.MetroForm

    {
        public frmEq()
        {
            InitializeComponent();
        }
        SqlConnection conn;
        private void FrmEq_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            ShowData();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void ShowData()
        {
            string sql = "select es.OrderKE ,e.EquipmentID, e.Equipmenname, es.BorrowID, es.Number , dbo.mydate(es.Date) , es.damaged , es.lost,s.name  from Equipmentlists es  INNER JOIN  Equipments e on es.EquipmentID=e.EquipmentID inner join Borrows b on es.BorrowID=b.BorrowID inner join Seculars s on b.SecularID=s.SecularID  where damaged is not null or lost is not null";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            datag.DataSource = ds.Tables[0];
            datag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            datag.Columns[0].HeaderCell.Value = "ลำดับที่";
            datag.Columns[1].HeaderCell.Value = "รหัสอุปกรณ์";
            datag.Columns[2].HeaderCell.Value = "ชื่ออุปกรณ์";
            datag.Columns[3].HeaderCell.Value = "รหัสการยืม";
            datag.Columns[4].HeaderCell.Value = "จำนวน";
            datag.Columns[5].HeaderCell.Value = "วันที่";
            datag.Columns[6].HeaderCell.Value = "ชำรุด";
            datag.Columns[7].HeaderCell.Value = "สูญหาย";
            datag.Columns[8].HeaderCell.Value = "ชื่อฆรวาส";


        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            ShowData1(txtID.Text);
        }
        private void ShowData1(string text)
        {
            string sql = "select es.OrderKE ,e.EquipmentID, e.Equipmenname, es.BorrowID, es.Number , dbo.mydate(es.Date) , es.damaged , es.lost,s.name  from Equipmentlists es  INNER JOIN  Equipments e on es.EquipmentID=e.EquipmentID inner join Borrows b on es.BorrowID=b.BorrowID inner join Seculars s on b.SecularID=s.SecularID " +
                " where name like '%' + @strName + '%'" +
                "  and damaged is not null   ";
         
            

            SqlCommand com = new SqlCommand(sql, conn);
            com.Parameters.AddWithValue("@strName", text);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            datag.DataSource = ds.Tables[0];
            datag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            datag.Columns[0].HeaderCell.Value = "ลำดับที่";
            datag.Columns[1].HeaderCell.Value = "รหัสอุปกรณ์";
            datag.Columns[2].HeaderCell.Value = "ชื่ออุปกรณ์";
            datag.Columns[3].HeaderCell.Value = "รหัสการยืม";
            datag.Columns[4].HeaderCell.Value = "จำนวน";
            datag.Columns[5].HeaderCell.Value = "วันที่";
            datag.Columns[6].HeaderCell.Value = "ชำรุด";
            datag.Columns[7].HeaderCell.Value = "สูญหาย";
            datag.Columns[8].HeaderCell.Value = "ชื่อฆรวาส";
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            demo11 f = new demo11();

            f.ShowPreviewDialog();
        }
    }
}
