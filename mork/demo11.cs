﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Linq;
namespace mork
{
    public partial class demo11 : DevExpress.XtraReports.UI.XtraReport
    {
        public demo11()
        {
            InitializeComponent();
        }
        DataClasses1DataContext db;
        private void Demo11_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            db = new DataClasses1DataContext();

            var v = from es in db.Equipmentlists
                    join ee in db.Equipments on es.EquipmentID equals ee.EquipmentID
                    join b in db.Borrows on es.BorrowID equals b.BorrowID
                    join s in db.Seculars on b.SecularID equals s.SecularID
                    where es.damaged != null && es.lost != null


                    select new
                    {

                        ee.EquipmentID,
                        ee.Equipmenname,
                        es.BorrowID,
                        es.Number,
                        Idate = String.Format("{0:dd MMMM yyy}", es.Date),
                        s.name,
                        es.damaged,
                        es.lost

                    };

            this.DataSource = v;
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel15.Text = Convert.ToString(this.GetCurrentColumnValue("EquipmentID"));
            xrLabel16.Text = Convert.ToString(this.GetCurrentColumnValue("Equipmenname"));
            xrLabel17.Text = Convert.ToString(this.GetCurrentColumnValue("BorrowID"));
            xrLabel18.Text = Convert.ToString(this.GetCurrentColumnValue("Number"));
            xrLabel2.Text = Convert.ToString(this.GetCurrentColumnValue("Idate"));
            xrLabel3.Text = Convert.ToString(this.GetCurrentColumnValue("name"));
            xrLabel12.Text = Convert.ToString(this.GetCurrentColumnValue("damaged"));
            xrLabel14.Text = Convert.ToString(this.GetCurrentColumnValue("lost"));
        }
    }
}
