﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace mork
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
            
        }
        public int EmpID { get; private set; }
        public string Fritsname { get; private set; }
        // public string Lastname { get; private set; }
        public string Position { get; private set; }
        //public string Username { get; private set; }
        SqlConnection conn = new ConnecDB().SqlStrCon();
        public static int eID;
        public static string name;

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            try
            {
                if (conn.State == ConnectionState.Open) conn.Close();
                conn.Open();
            }
            
           
            catch (Exception )
            {
                string str = "กรุณาตรวจสอบการตั้งค่าของฐานข้อมูล " + Environment.NewLine
                    + "IP address ของ database server" + Environment.NewLine
                    + "หมายเลข Port" + Environment.NewLine
                    + "ชื่อ database name" + Environment.NewLine
                    + "ชื่อผู้ใช่ และ รหัสผ่าน ให้ถูกต้อง" + Environment.NewLine
                    +"กรุณากลับไปแก้ไข เมนูจัดการข้อมูลระบบ";

                MessageBox.Show(str,"เกิดข้อผิดพลาดกรุณาตรวจสอบข้อมูล");
            }
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            try
            {

            
            string sql = "select UserID ,Name  ,Department  from Personnels where Username = @Username and Password = @Password";


            SqlCommand comm = new SqlCommand(sql, conn);
            var x = comm.Parameters;
            x.AddWithValue("@Username", txtID.Text);
            x.AddWithValue("@Password", txtPass.Text);
            SqlDataReader dr = comm.ExecuteReader();

            if (dr.Read())
            {
                eID = Convert.ToInt16(dr[0]);
                EmpID = Convert.ToInt16(dr[0]);

                name = dr[1].ToString();
                
                //MessageBox.Show(EmpID.ToString());

                Fritsname = dr[1].ToString();
                Position = dr[2].ToString();
                this.Close();
            }
            else
            {
                MessageBox.Show("กรุณาป้อนข้อมูลให้ถูกต้อง เพื่อเข้าสู่ระบบ", "ผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtID.Text = "";
                txtPass.Text = "";
                txtID.Focus();
            }
            dr.Close();
                }
            catch (Exception )
            {

                MessageBox.Show("Error");

            }
        }

       

        

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TxtPass_TextChanged(object sender, EventArgs e)
        {
            
              
            
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                txtPass.UseSystemPasswordChar = true;
            }
           
        }

       

        private void Panel1_Click(object sender, EventArgs e)
        {
            txtID.Text = "nonthawat";
            txtPass.Text = "n1900";
        }

       

        private void Panel2_Click_1(object sender, EventArgs e)
        {
            txtID.Text = "admin1";
            txtPass.Text = "123";
        }

        private void Panel3_Click(object sender, EventArgs e)
        {
            txtID.Text = "adminEm1234";
            txtPass.Text = "123";
        }

        private void Panel4_Click(object sender, EventArgs e)
        {
            txtID.Text = "ID123";
            txtPass.Text = "123";
        }

        private void Panel5_Click(object sender, EventArgs e)
        {
            txtID.Text = "adminAd1234";
            txtPass.Text = "123";
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void TxtPass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string sql = "select UserID ,Name  ,Department  from Personnels where Username = @Username and Password = @Password";


                SqlCommand comm = new SqlCommand(sql, conn);
                var x = comm.Parameters;
                x.AddWithValue("@Username", txtID.Text);
                x.AddWithValue("@Password", txtPass.Text);
                SqlDataReader dr = comm.ExecuteReader();

                if (dr.Read())
                {
                    eID = Convert.ToInt16(dr[0]);
                    EmpID = Convert.ToInt16(dr[0]);

                    name = dr[1].ToString();

                    //MessageBox.Show(EmpID.ToString());

                    Fritsname = dr[1].ToString();
                    Position = dr[2].ToString();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("กรุณาป้อนข้อมูลให้ถูกต้อง เพื่อเข้าสู่ระบบ", "ผิดพลาด", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtID.Text = "";
                    txtPass.Text = "";
                    txtID.Focus();
                }
                dr.Close();
            }
        }

        private void TxtPass_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void TxtPass_KeyUp(object sender, KeyEventArgs e)
        {

        }
    }
}
