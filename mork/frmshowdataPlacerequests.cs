﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using DevExpress.XtraReports.UI;
namespace mork
{
    public partial class frmshowdataPlacerequests : MetroFramework.Forms.MetroForm
    {
        public int RequestID { get; set; }//รหัสกิจนิมนต์
        public string Date { get; set; }
        public string Time { get; set; }
        public string Time1;
        public string Description { get; set; }
        public string Phone { get; set; }
        public string PlaceID { get; set; }
        public string UserID { get; set; }//ชื่อผู้รับ
        public string SecularID { get; set; }//ชื่อฆราวาส
        public int ID;
        SqlConnection conn;
        public frmshowdataPlacerequests()
        {
            InitializeComponent();
        }

        private void FrmshowdataPlacerequests_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();

            label1.Text = RequestID.ToString();
            label2.Text = Date.ToString();
            label3.Text = Time.ToString();
            label5.Text = Description.ToString();
            label6.Text = Phone.ToString();
            label7.Text = PlaceID.ToString();
            label8.Text = UserID.ToString();
            label18.Text = SecularID.ToString();
            
        }

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btnreport_Click(object sender, EventArgs e)
        {
            RePlacerequests f = new RePlacerequests();
            f.ID = Convert.ToInt32(label1.Text);
            f.Time1 = Convert.ToString(label3.Text);
            f.Date = Convert.ToString(label2.Text);

            f.ShowPreviewDialog();
        }
    }
}
