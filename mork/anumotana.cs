﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Linq;
using System.Data.SqlClient;
namespace mork
{
    public partial class anumotana : DevExpress.XtraReports.UI.XtraReport
    {
        public anumotana()
        {
            InitializeComponent();
        }
        
        DataClasses1DataContext db;
 
        public int ID;
        private void Anumotana_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            
            db = new DataClasses1DataContext();

            var v = from k in db.Donates
                    join s in db.Seculars on k.SecularID equals s.SecularID
                    where k.Donate1 == ID
                    //orderby f.Name
                    select new
                    {
                        
                        k.Donate1,
                        s.name,
                        k.Number,
                        k.Description,
                        Bdate = String.Format("{0:dd MMMM yyy}", k.Date),


                    };

            this.DataSource = v;
        }

        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel3.Text = Convert.ToString(this.GetCurrentColumnValue("name"));
            xrLabel14.Text = Convert.ToString(this.GetCurrentColumnValue("Number"));
            xrLabel15.Text = Convert.ToString(this.GetCurrentColumnValue("Description"));
            xrLabel11.Text = DateTime.Now.ToLongDateString();
        }
    }
}
