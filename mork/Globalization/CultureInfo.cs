﻿using System.Globalization;

namespace Globalization
{
    internal class CultureInfo : System.Globalization.CultureInfo
    {
        public CultureInfo(string name) : base(name)
        {
        }
    }
}