﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraReports.UI;
namespace mork
{
    public partial class frmformmain : MetroFramework.Forms .MetroForm
    {
        public frmformmain()
        {
            InitializeComponent();
            timer1.Start();
            lbltime.Text= DateTime.Now.ToLongTimeString();
            lblDate.Text = DateTime.Now.ToLongDateString();
        }
        private void CloseAllChildFrom()
        {
            foreach (Form L in this.MdiChildren)
            {
                L.Close();
            }


        }
        SqlConnection conn;
        public int UserID;
        public string name;
        public string username;
        public string password;
        public string Department;
        private void Frmformmain_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            
            //this.Text = "ล็อคอินเด้อจ้า";
            EnableMenu(true, false, false, false, false, false);
        }
        private void EnableMenu(bool login, bool Equipment, bool Kitnimon, bool admin, bool monk, bool godmonk)
        {
            menuStrip1.Visible = login;
            menuStrip3.Visible = Equipment;
            menuStrip2.Visible = Kitnimon;
            menuStrip4.Visible = admin;
            menuStrip5.Visible = godmonk;
            menuStrip6.Visible = monk;


        }
        
        private void เขาสระบบToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            using (frmLogin f = new frmLogin())
            {

                f.ShowDialog();
                UserID = f.EmpID;
                name = f.Fritsname;
                //Lastname = f.Lastname;
                Department = f.Position;
                //Username = f.Username;

                //this.Text = name + "  " + Department;

                //label1.Text = name.ToString();
                if (Department == "ฝ่ายกิจนิมนต์")
                {
                    EnableMenu(false, true, false, false, false, false);


                }
                else if (Department == "ฝ่ายอุปกรณ์")
                {
                    EnableMenu(false, false, true, false, false, false);

                }
                else if (Department == "ผู้ดูแลระบบ")
                {
                    EnableMenu(false, false, false, true, false, false);

                }
                else if (Department == "เจ้าอาวาส")
                {
                    EnableMenu(false, false, false, false, false, true);

                }
                else if (Department == "พระสงฆ์")
                {
                    EnableMenu(false, false, false, false, true, false);

                }

            }
        }

        private void ปดโปรแกรมToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("ต้องการปิดโปรแกรมหรือไม่", "แจ้งเตือน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void LogOut() //สร้าง method ออกจากระบบ
        {
            if (MessageBox.Show("!!!!คณุต้องการจะออกจากรบบไหม?", "แจ้งเตือน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                UserID = 0;
                name = "";
                Department = "";
                this.Text = "ระบบจัดการกิจนิมนต์";
                foreach (var item in this.MdiChildren) //สั่งปิดฟอร์มลูกที่เปิดอยู่
                {
                    item.Close();
                }
                EnableMenu(true, false, false, false, false, false);
                CloseAllChildFrom();
                using (frmLogin f = new frmLogin())
                {

                    f.ShowDialog();
                    UserID = f.EmpID;
                    name = f.Fritsname;
                    //Lastname = f.Lastname;
                    Department = f.Position;
                    //Username = f.Username;

                    //this.Text = name + "  " + Department;

                    //label1.Text = name.ToString();
                    if (Department == "ฝ่ายกิจนิมนต์")
                    {
                        EnableMenu(false, true, false, false, false, false);


                    }
                    else if (Department == "ฝ่ายอุปกรณ์")
                    {
                        EnableMenu(false, false, true, false, false, false);

                    }
                    else if (Department == "ผู้ดูแลระบบ")
                    {
                        EnableMenu(false, false, false, true, false, false);

                    }
                    else if (Department == "เจ้าอาวาส")
                    {
                        EnableMenu(false, false, false, false, false, true);

                    }
                    else if (Department == "พระสงฆ์")
                    {
                        EnableMenu(false, false, false, false, true, false);

                    }

                }

            }
        }

        private void ออกจากระบบToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LogOut();
        }

        private void ออกจากระบบToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            LogOut();
        }

        private void ออกจากระบบToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            LogOut();
        }

        private void ออกจากระบบToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            LogOut();
        }

        private void ออกจากระบบToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            LogOut();
        }

        private void จดการขอมลผใชงานToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmReport f = new frmReport();
            f.MdiParent = this;
            f.Show();
            //foreach (Form CurrentFrom in this.MdiChildren)
            //{
            //    if (CurrentFrom is frmReport)
            //    {
            //        CurrentFrom.MdiParent = this;
            //        CurrentFrom.Show();
            //        return;
            //    }
            //}

            //frmReport f = new frmReport();
            
            //f.MdiParent = this;
            f.Show();

        }
       
        private void จดการขอมลอปกรณToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmEquipment f = new frmEquipment();
            f.MdiParent = this;
            f.Show();
        }

        private void จดการการรบกจนมนตToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //CloseAllChildFrom();
            //frmKitnimon f = new frmKitnimon();

            //f.MdiParent = this;
            //f.Show();
            foreach (Form CurrentFrom in this.MdiChildren)
            {
                if (CurrentFrom is frmtestkit)
                {
                    CurrentFrom.MdiParent = this;
                    CurrentFrom.Show();
                    return;
                }
            }
            foreach (Form form in this.MdiChildren)
            {
                form.Close();
            }
            frmtestkit f = new frmtestkit();
            
            f.MdiParent = this;
            f.Show();
        }

        private void จดการการขอใชสถานทToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmPlacerequest f = new frmPlacerequest();
            f.MdiParent = this;
            f.Show();
        }

        private void จดการประเภทกจนมนตToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmCategorykitnimon f = new frmCategorykitnimon();
            f.MdiParent = this;
            f.Show();
        }

        private void จดการยานพหToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmVehicle f = new frmVehicle();
            f.MdiParent = this;
            f.Show();
        }

        private void จดการขอมลสถานทToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmPlace f = new frmPlace();
            f.MdiParent = this;
            f.Show();
        }

        private void จดการการยมอปกรณToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmborrow f = new frmborrow();
            f.MdiParent = this;
            f.Show();
        }

        private void จดการประเภทอปกรณToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmCategoryequipments f = new frmCategoryequipments();
            f.MdiParent = this;
            f.Show();
        }

        private void อนมตรายการกจนมนตToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
                CloseAllChildFrom();
            frmapprove f = new frmapprove();
            f.MdiParent = this;
            f.Show();
        }

        private void อนมตการขอใชสถานทToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
                 CloseAllChildFrom();
            frmapplace f = new frmapplace();
            f.MdiParent = this;
            f.Show();
        }

        private void อนมนตการขอใชอปกรณToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
                CloseAllChildFrom();
            frmshowBorrow f = new frmshowBorrow();
            f.MdiParent = this;
            f.Show();
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            
        }

        private void MenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void ขอมลสมาชกToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowmonk frmshowPersonnel = new frmshowmonk();
            frmshowPersonnel.MdiParent = this;
            frmshowPersonnel.Show();
        }

        private void ขอมลพนกงานToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowkitnimon f = new frmshowkitnimon();
            f.MdiParent = this;
            f.Show();
        }

        private void ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowkitnimon f = new frmshowkitnimon();
            f.MdiParent = this;
            f.Show();
        }

        private void ToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowOkBorrow f = new frmshowOkBorrow();
            f.MdiParent = this;
            f.Show();
        }

        private void ToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowOkPlacerequest f = new frmshowOkPlacerequest();
            f.MdiParent = this;
            f.Show();
        }

        private void จดการการคนอปกรณToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmEquipmantReturn f = new frmEquipmantReturn();
            f.MdiParent = this;
            f.Show();
        }

        private void คนหากจนมนตToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmkitnimonmonk f = new frmkitnimonmonk();
            f.MdiParent = this;
            f.Show();
        }

        private void จดการขอมลฆราวาสToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmSecular f = new frmSecular();
            f.MdiParent = this;
            f.Show();
        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void จดการขอมลพระสงฆToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmmanagemonk f = new frmmanagemonk();
            f.MdiParent = this;
            f.Show();
        }

        private void จดการขอมลสถานทToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmPlace f = new frmPlace();
            f.MdiParent = this;
            f.Show();
        }

        private void จดการขอมลประเภทกจจมนตToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmCategorykitnimon f = new frmCategorykitnimon();
            f.MdiParent = this;
            f.Show();
        }

        private void จดการยานพหToolStripMenuItem_Click_1(object sender, EventArgs e)
        {

        }

        private void จดการยานพาหนะToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmVehicle f = new frmVehicle();
            f.MdiParent = this;
            f.Show();
        }

        private void Label1_Click(object sender, EventArgs e)
        {
            
        }

        private void ขอมลผคำประกนToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowmonk frmshowPersonnel = new frmshowmonk();
            frmshowPersonnel.MdiParent = this;
            frmshowPersonnel.Show();
        }

        private void คนหาขอมลสถานะในวดToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowPlace frmshowPersonnel = new frmshowPlace();
            frmshowPersonnel.MdiParent = this;
            frmshowPersonnel.Show();
        }

        private void ToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmSecular f = new frmSecular();
            f.MdiParent = this;
            f.Show();
        }

        private void ToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmdonat f = new frmdonat();
            f.MdiParent = this;
            f.Show();
        }

        private void ปดโปรแกรมToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("ต้องการปิดโปรแกรมหรือไม่", "แจ้งเตือน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {

                //this.Text = "ระบบจัดการกิจนิมนต์";
                foreach (var item in this.MdiChildren) //สั่งปิดฟอร์มลูกที่เปิดอยู่
                {
                    item.Close();
                }
                Application.Exit();
            }
        }

        private void ToolStripMenuItem7_Click(object sender, EventArgs e)
        {
           
        }

        private void ToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            
        }

        private void ToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowPlace frmshowPersonnel = new frmshowPlace();
            frmshowPersonnel.MdiParent = this;
            frmshowPersonnel.Show();
        }

        private void ToolStripMenuItem8_Click_1(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowkitnimon f = new frmshowkitnimon();
            f.MdiParent = this;
            f.Show();
        }

        private void บรจาคเงนToolStripMenuItem_Click(object sender, EventArgs e)
        {

            CloseAllChildFrom();
            donatb f = new donatb();
            f.MdiParent = this;
            f.Show();
        }

        private void ToolStripMenuItem7_Click_1(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            donatb f = new donatb();
            f.MdiParent = this;
            f.Show();
        }

        private void คนหาขอมลการขอใชสถานทToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowpla f = new frmshowpla();
            f.MdiParent = this;
            f.Show();
        }

        private void ขอมลกจนมนตToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowmonk frmshowPersonnel = new frmshowmonk();
            frmshowPersonnel.MdiParent = this;
            frmshowPersonnel.Show();
        }

        private void ขอมลการยมอปกรณToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowkitnimon f = new frmshowkitnimon();
            f.MdiParent = this;
            f.Show();
        }

        private void ขอมลการขอใชสถานทToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowPlace frmshowPersonnel = new frmshowPlace();
            frmshowPersonnel.MdiParent = this;
            frmshowPersonnel.Show();
        }

        private void คนหาและดขอมลToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void จดการขอมลToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void รายงานการขอใชสถานทToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseAllChildFrom();
            frmshowpla f = new frmshowpla();
            f.MdiParent = this;
            f.Show();
        }

        private void พมพใบยมวสดอปกรณไดToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reportinput f = new Reportinput();
            f.ShowPreviewDialog();
        }

        private void รายการการยมอกรณToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmshowlistborrow f = new frmshowlistborrow();
            CloseAllChildFrom();
            f.MdiParent = this;
            f.Show();
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
        }

        private void จดการขอมลระบบToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmconfig f = new frmconfig();
            CloseAllChildFrom();
            f.MdiParent = this;
            f.Show();
        }
    }
}
