﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
using DevExpress.XtraReports.UI;
namespace mork
{
    public partial class frmborrow : MetroFramework.Forms.MetroForm
    {
       
        public int ID { get; set; }
        public int UserID { get; set; }
        public string namef { get; set; }
        SqlConnection conn;
        SqlTransaction tr;
        SqlCommand comm;
        public frmborrow()
        {
            InitializeComponent();
            //this.dtpImportTime.CustomFormat = "HH:mm ";
            //this.dtpImportTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            //this.dtpImportTime.ShowUpDown = true;
            

        }
        
        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BarraTitulo_MouseMove(object sender, MouseEventArgs e)
        {
            
        }
        private void lsvFormat()
        {
           
            lsvShow.Columns.Add("ลำดับที่", 120, HorizontalAlignment.Right);
            lsvShow.Columns.Add("รหัสอุปกรณ์", 120, HorizontalAlignment.Right);
            lsvShow.Columns.Add("ชื่ออุปกรณ์", 120, HorizontalAlignment.Right);
            lsvShow.Columns.Add("จำนวน", 120, HorizontalAlignment.Right);
            lsvShow.Columns.Add("วันที่", 205, HorizontalAlignment.Right);


            lsvShow.View = View.Details;
            lsvShow.GridLines = true;
            lsvShow.FullRowSelect = true;



        }
        private void Frmborrow_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            conn.Open();
            string id = frmLogin.eID.ToString();
            lsvFormat();

            string name = frmLogin.name;
            
            lblID.Text = id;
            lblName.Text = name;
        }

        private void Btna_Click(object sender, EventArgs e)
        {
            using (frmshowSecular f = new frmshowSecular())
            {
                f.ShowDialog();
                txtSecularID.Text = f.SecularID.ToString();
                txtnames.Text = f.NameF.ToString();
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            frmselectequipment f = new frmselectequipment();

            f.ShowDialog();
            txtIDmonk.Text = f.UserID.ToString();
            txtnumber.Text = f.number.ToString();
            txtnamemonk.Text = f.namef.ToString();

        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
            if (txtnumber.Text.Trim() == "")
            {
                txtnumber.Focus(); return;
            }

            ListViewItem lvi;
            int i = 0;
            string tmpProductID;
            for (i = 0; i <= lsvShow.Items.Count - 1; i++)
            {
                tmpProductID = (lsvShow.Items[i].SubItems[1].Text);

                if (txtIDmonk.Text.Trim() == tmpProductID)
                {
                    if (MessageBox.Show("มีรายการซ้ำอยู่ ต้องการเพิ่มอีกหรือไม่", "ผิดพลาด") == DialogResult.Yes)
                    {
                        txtIDmonk.Focus();
                        txtIDmonk.SelectAll();
                        break;
                    }

                    else
                    {
                        return;
                    }

                }


            }
            int j = 0;
            int No = 0;
            for (j = 0; j <= lsvShow.Items.Count; j++)
            {
                No = No + 1;
            }
            string[] anyData;
            anyData = new string[] { No.ToString(), txtIDmonk.Text.ToString(), txtnamemonk.Text , txtnumber.Text, DateTime.Now.ToLongDateString() };
            lvi = new ListViewItem(anyData);
            lsvShow.Items.Add(lvi);

            btnseve.Enabled = true;
            txtnamemonk.Focus();
            
        }

        private void Btnseve_Click(object sender, EventArgs e)
        {
            int ID = 0;
            if (txtSecularID.Text.Trim() == "" || txtnumber.Text.Trim() == "" || txtIDmonk.Text.Trim() == "")
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน");
            }
            if (lsvShow.Items.Count > 0)
            {

                if (MessageBox.Show("ต้องการบันทึกรายการสั่งซื้อหรือไม่", "กรุณายินยัน", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    tr = conn.BeginTransaction();

                    string sqlEmpBills = "insert into Borrows (RedateB,Redate,Time,Retundate,Approve,UserID,SecularID)"
                                       + " values (@RedateB,@Redate,@Time,@Retundate,@Approve,@UserID,@SecularID)";

                    comm = new SqlCommand(sqlEmpBills, conn, tr);

                    comm.Parameters.Add("@RedateB", SqlDbType.Date).Value = dtpbringback.Value;
                 
                    comm.Parameters.Add("@Time", SqlDbType.Time).Value = DateTime.Now.ToLongTimeString();
                    comm.Parameters.Add("@Redate", SqlDbType.Date).Value = DateTime.Now.ToLongDateString();
                    comm.Parameters.Add("@Retundate", SqlDbType.Date).Value = btpbringback1.Value;
                    comm.Parameters.Add("@Approve", SqlDbType.Int).Value = Convert.ToInt16("0");
                    comm.Parameters.Add("@UserID", SqlDbType.Int).Value = Convert.ToInt16(lblID.Text);
                    comm.Parameters.Add("@SecularID", SqlDbType.Int).Value = Convert.ToInt16(txtSecularID.Text);
                   

                    comm.ExecuteNonQuery();


                    string sql = "Select top 1 BorrowID from Borrows order by BorrowID desc";
                    SqlCommand comm1 = new SqlCommand(sql, conn, tr);
                    SqlDataReader dr;
                    dr = comm1.ExecuteReader();
                    if (dr.HasRows)
                    {
                        dr.Read();
                        ID = dr.GetInt32(dr.GetOrdinal("BorrowID"));
                    }
                    else
                    {
                        ID = 1;
                    }
                    dr.Close();
                    int i = 0;


                    for (i = 0; i <= lsvShow.Items.Count - 1; i++)
                    {


                        string sql3 = "insert into Equipmentlists (OrderKE , BorrowID , Number ,Date ,EquipmentID) values(@OrderKE , @BorrowID , @Number ,@Date ,@EquipmentID)";

                        SqlCommand comm = new SqlCommand(sql3, conn, tr);

                        comm.Parameters.AddWithValue("@OrderKE", SqlDbType.Int).Value = Convert.ToInt16(lsvShow.Items[i].SubItems[0].Text);

                        comm.Parameters.AddWithValue("@BorrowID", ID);

                        comm.Parameters.AddWithValue("@Number", SqlDbType.Int).Value = Convert.ToInt16(lsvShow.Items[i].SubItems[3].Text);

                        comm.Parameters.AddWithValue("@Date", Convert.ToDateTime(lsvShow.Items[i].SubItems[4].Text));

                        comm.Parameters.AddWithValue("@EquipmentID", SqlDbType.Int).Value = Convert.ToInt16(lsvShow.Items[i].SubItems[1].Text);

                        comm.ExecuteNonQuery();

                        //ลบสินค้า
                        string sqlUpdateStock = " update Equipments set "
                                          + " Numbertreasury = Numbertreasury - @Qty "
                                          + " where EquipmentID like @EquipmentID ";
                        comm = new SqlCommand(sqlUpdateStock, conn, tr);
                        comm.Parameters.Add("@Qty", SqlDbType.Int).Value = Convert.ToInt16(lsvShow.Items[i].SubItems[3].Text);
                        comm.Parameters.Add("@EquipmentID", SqlDbType.Int).Value = Convert.ToInt16(lsvShow.Items[i].SubItems[1].Text);
                        comm.ExecuteNonQuery();




                    }

                }
                tr.Commit();
                MessageBox.Show("บันทึกรายการเรียบร้อยแล้ว", "ผลการทำงาน");
                btnClear.PerformClick();
                lsvShow.Clear();
                
                clearfrom();

                Reborrow f = new Reborrow();
                f.ID = ID;
               
                f.ShowPreviewDialog();


            }
        }

        private void clearfrom()
        {
            txtIDmonk.Text = "";
            txtnamemonk.Text = "";
            txtnumber.Text = "";
            txtSecularID.Text = "";
            lsvShow.Clear();
            lsvFormat();

        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            clearfrom();
        }

        private void LsvShow_DoubleClick(object sender, EventArgs e)
        {
            int i = 0;
            for (i = 0; i <= lsvShow.SelectedItems.Count - 1; i++)
            {
                ListViewItem lsv = lsvShow.SelectedItems[i];
                lsvShow.Items.Remove(lsv);

            }
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            frmshowlistborrow f = new frmshowlistborrow();

            f.ShowDialog();

        }

        private void Btna_Click_1(object sender, EventArgs e)
        {

        }

        private void Button3_Click(object sender, EventArgs e)
        {
            using (frmshowSecular f = new frmshowSecular())
            {
                f.ShowDialog();
                txtSecularID.Text = f.SecularID.ToString();
                txtnames.Text = f.NameF.ToString();
            }
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            frmshowlistborrow f = new frmshowlistborrow();

            f.ShowDialog();
        }

        private void Label13_Click(object sender, EventArgs e)
        {

        }
    }
}
