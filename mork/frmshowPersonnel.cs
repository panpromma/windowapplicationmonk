﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
namespace mork
{
    public partial class frmshowPersonnel : MetroFramework.Forms.MetroForm
    {
        public frmshowPersonnel()
        {
            InitializeComponent();
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        public int IDusername { get; set; }

        public string nameaa { get; set; }

        SqlConnection conn;
        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            
          
            this.Close();
           
        }

        private void FrmshowPersonnel_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            ShowData();
        }
        private void ShowData()
        {
            string sql = "select p.UserID  , p.Name  from Personnels p full JOIN Kitnimonlists k on p.UserID=k.UserID where Department = 'พระสงฆ์' and k.KitnmonID IS  NULL";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            showskin();
        }
        
        private void showskin()
        {
            dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
            dgvdatag.Columns[1].HeaderCell.Value = "ชื่อสกุล";
         


            //dgvdatag.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            //dgvdatag.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            //dgvdatag.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            //dgvdatag.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            //dgvdatag.BackgroundColor = Color.White;

            //dgvdatag.EnableHeadersVisualStyles = false;
            //dgvdatag.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            //dgvdatag.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            //dgvdatag.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
        }


        private void Dgvdatag_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var x = dgvdatag.CurrentRow.Cells;
            IDusername = Convert.ToInt16(x["UserID"].Value);
            nameaa = Convert.ToString(x["Name"].Value);

            this.Close();

        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
           
        }
       

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            //showdata(txtID.Text);
        }
        //private void showdata(string text)
        //{
        //    string sql = "select p.UserID  , p.Name , k.KitnmonID from Personnels p full JOIN Kitnimonlists k on p.UserID=k.UserID where Department = 'พระสงฆ์' and k.KitnmonID IS  NULL"
        //                + "or Name like '%' + @strName + '%' ";

        //    SqlCommand com = new SqlCommand(sql, conn);
        //    com.Parameters.AddWithValue("@strName", text);
        //    SqlDataAdapter da = new SqlDataAdapter(com);
        //    DataSet ds = new DataSet();
        //    da.Fill(ds);
        //    dgvdatag.DataSource = ds.Tables[0];
        //    showskin();
        //}

        private void PictureBox1_Click(object sender, EventArgs e)
        {
           
            nameaa = "";
            this.Close();
        }
    }
}
