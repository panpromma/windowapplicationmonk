﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
namespace mork
{
    public partial class frmaddupPersonnel : Form
    {
        SqlConnection conn;
        
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        public frmaddupPersonnel()
        {
            InitializeComponent();
        }

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            ShowData(textBox1.Text.ToString());
        }
        private void ShowData(string name)
        {
            string sql = "select *from Personnels where name like @name";
            SqlCommand comm = new SqlCommand(sql, conn);
            comm.Parameters.AddWithValue("@name", name + "%");
            SqlDataAdapter da = new SqlDataAdapter(comm);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdata.DataSource = ds.Tables[0];
            dgvdata.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void FrmaddupPersonnel_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            setcbo();
            ShowData();
        }
        private void ShowData()
        {
            string sql = "select *from Personnels where Department ='พระสงฆ์'";
            
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdata.DataSource = ds.Tables[0];
            dgvdata.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

        }
        private void clearfrom()
        {
            TextID.Text = "";
            cmbPrefix.Text = "";
            txtNameF.Text = "";
            cmbDepartment.Text = "";
            txtAge.Text = "";
            txtPhone.Text = "";
           
            txtUsernametext.Text = "";
            txtPasswordtext.Text = "";
            txtAddress.Text = "";
        }
        private void setcbo()
        {
            string[] prefix = new string[] { "", "นาย", "นาง", "นางสาว", "เณร", "หลวงพี่" };

            string[] Department = new string[] { "",  "พระสงฆ์" };

            cmbPrefix.Items.AddRange(prefix);
            cmbDepartment.Items.AddRange(Department);
        }

        private void BtnClears_Click(object sender, EventArgs e)
        {
            clearfrom();
        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
            if (txtNameF.Text.Trim() == "" || cmbDepartment.Text.Trim() == "" || txtAge.Text.Trim() == "" || txtUsernametext.Text.Trim() == "" || txtPasswordtext.Text.Trim() == "" || txtAddress.Text.Trim() == "")
            {
                MessageBox.Show("กรุณาป้อนข้อมูล", "ผิดพลาด");

              
            }
            else
            {


                string sql = "insert into Personnels (Name,Department,Phone,Username,Password,Age,sex,Address,Prefix) values( @Name,@Department,@Phone,@Username,@Password,@Age,@sex,@Address,@Prefix)";
                SqlCommand comm = new SqlCommand(sql, conn);


                comm.Parameters
                    .AddWithValue("@name", txtNameF.Text.Trim());

                comm.Parameters
                    .AddWithValue("@Department", cmbDepartment.Text.Trim());

                comm.Parameters
                    .AddWithValue("@Phone", txtPhone.Text.Trim());

                comm.Parameters
                   .AddWithValue("@Username", txtUsernametext.Text.Trim());

                comm.Parameters
                   .AddWithValue("@Password", txtPasswordtext.Text.Trim());

                comm.Parameters
                   .AddWithValue("@Age", txtAge.Text.Trim());

                comm.Parameters
                   .AddWithValue("@sex", "ชาย");

                comm.Parameters
                   .AddWithValue("@Address", txtAddress.Text.Trim());

                comm.Parameters
                  .AddWithValue("@Prefix", cmbPrefix.Text.Trim());



                //MessageBox.Show(txtNameF.Text);
                //MessageBox.Show(cmbPrefix.Text);
                //MessageBox.Show(cmbDepartment.Text);
                //MessageBox.Show(txtPhone.Text);
                //MessageBox.Show(txtUsernametext.Text);
                //MessageBox.Show(txtPasswordtext.Text);
                //MessageBox.Show(txtAge.Text);
                //MessageBox.Show(sex);
                //MessageBox.Show(txtAddress.Text);


                if (conn.State == ConnectionState.Closed) conn.Open();
                comm.ExecuteNonQuery();
                conn.Close();
                clearfrom();
                ShowData();
            }
        }

        private void Btndelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("!!!!คณุต้องการจะลบใช่ไหม?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {

                string sql = "delete from Personnels where userID = @userID";
                SqlCommand comm = new SqlCommand(sql, conn);



                comm.Parameters

                    .AddWithValue("@userID", TextID.Text.Trim());

                try
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                    clearfrom();
                    ShowData();

                }
                catch
                {
                    MessageBox.Show("ไม่สามารถลบข้มูลได้", "Erorr");

                    return;
                }
            }
        }

        private void Btnmodify_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("!!!!คณุต้องการจะแก้ไขไหม?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {


                string sql = "update Personnels set  name=@name, Department=@Department, Phone=@Phone, Username=@Username, Password=@Password, Age=@Age, sex=@sex, Address=@Address,Prefix=@Prefix where  userID=@userID  ";
                SqlCommand comm = new SqlCommand(sql, conn);


                comm.Parameters
                    .AddWithValue("@userID", TextID.Text.Trim());

                comm.Parameters
                    .AddWithValue("@name", txtNameF.Text.Trim());

                comm.Parameters
                    .AddWithValue("@Department", cmbDepartment.Text.Trim());

                comm.Parameters
                    .AddWithValue("@Phone", txtPhone.Text.Trim());

                comm.Parameters
                   .AddWithValue("@Username", txtUsernametext.Text.Trim());

                comm.Parameters
                   .AddWithValue("@Password", txtPasswordtext.Text.Trim());

                comm.Parameters
                   .AddWithValue("@Age", txtAge.Text.Trim());

                comm.Parameters
                   .AddWithValue("@sex", "ชาย");

                comm.Parameters
                   .AddWithValue("@Address", txtAddress.Text.Trim());

                comm.Parameters
                  .AddWithValue("@Prefix", cmbPrefix.Text.Trim());





                try
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                    clearfrom();
                    ShowData();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Dgvdata_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            var x = dgvdata.CurrentRow.Cells;



            TextID.Text = x["userID"].Value.ToString();
            cmbPrefix.Text = x["Prefix"].Value.ToString();
            txtNameF.Text = x["Name"].Value.ToString();
            cmbDepartment.Text = x["Department"].Value.ToString();
            txtAge.Text = x["Age"].Value.ToString();
            txtPhone.Text = x["Phone"].Value.ToString();
            txtUsernametext.Text = x["Username"].Value.ToString();
            txtPasswordtext.Text = x["Password"].Value.ToString();
            txtAddress.Text = x["Address"].Value.ToString();
        }

        private void Dgvdata_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }
    }
}
