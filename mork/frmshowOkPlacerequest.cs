﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
namespace mork
{
    public partial class frmshowOkPlacerequest : Form
    {
        private bool bFullScreen;
        SqlConnection conn;
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        public frmshowOkPlacerequest()
        {
            InitializeComponent();
            if (bFullScreen == false)
            {
                this.FormBorderStyle = FormBorderStyle.None;
                this.WindowState = FormWindowState.Maximized;

                bFullScreen = true;
            }
            else
            {
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.WindowState = FormWindowState.Maximized;//  .Maximized;
                bFullScreen = true;
            }
        }

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmshowOkkitnimon_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            ShowData2();
        }
        private void ShowData2()
        {
            string sql = "select RequestID as รหัสการชอใช้สถานที่ , Date  as วันที่, Time as เวลา, p.Description as รายละเอียด , ps.Phone as สถานะ , case  when ps.Approve = 0 then 'ยังไม่อนุมัติ' when ps.Approve = 1 then 'อนุมัติแล้ว' end as สถานะ ,p.PlaceName as ชื่อสถานที่ ,pss.Name as ชื่อคนที่รับงาน , ss.name as ชื่อฆราวาส from Placerequests ps INNER JOIN  Places p on  ps.PlaceID=p.PlaceID INNER JOIN  Personnels pss on ps.UserID=pss.UserID INNER JOIN  Seculars ss on ps.SecularID=ss.SecularID  ";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }
        private void ShowData(string name)
        {
            if (txtID.Text != "")
            {
                string sql = "select RequestID as รหัสการชอใช้สถานที่ , Date  as วันที่, Time as เวลา, p.Description as รายละเอียด , ps.Phone as สถานะ , case  when ps.Approve = 0 then 'ยังไม่อนุมัติ' when ps.Approve = 1 then 'อนุมัติแล้ว' end as สถานะ ,p.PlaceName as ชื่อสถานที่ ,pss.Name as ชื่อคนที่รับงาน , ss.name as ชื่อฆราวาส from Placerequests ps INNER JOIN  Places p on  ps.PlaceID=p.PlaceID INNER JOIN  Personnels pss on ps.UserID=pss.UserID INNER JOIN  Seculars ss on ps.SecularID=ss.SecularID" +
                " where Date like '%' + @name + '%'" +
                "or PlaceName like '%'+@name+'%'";
                

            SqlCommand com = new SqlCommand(sql, conn);
            com.Parameters.AddWithValue("@name", name);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
           
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            }
            else
            {
                ShowData2();
            }
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            ShowData(txtID.Text.ToString());
        }
    }
}
