﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using DevExpress.XtraReports.UI;
namespace mork
{
    public partial class frmshowmonkrokin : Form
    {
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        SqlConnection conn;
        public int UserID { get; set; }
        public frmshowmonkrokin()
        {
            InitializeComponent();
        }

        private void Frmshowmonkrokin_Load(object sender, EventArgs e)
        {
            label2.Text = this.UserID.ToString();
            lxl2.Text = this.UserID.ToString();
            conn = new ConnecDB().SqlStrCon();
            conn.Open();
            ShowData();
        }
        private void ShowData()
        {
            string sql = "select ks.UserID, p.Name,cs.WorkName from Kitnmons k  INNER JOIN Kitnimonlists ks on k.KitnmonID = ks.KitnmonID INNER JOIN Personnels p on ks.UserID = p.UserID INNER JOIN CategoryKitnimons cs on k.CategoryKitID = cs.CategoryKitID where ks.KitnmonID='" + label2.Text + "'";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
            dgvdatag.Columns[1].HeaderCell.Value = "ชื่อพระสงฆ์";
            dgvdatag.Columns[2].HeaderCell.Value = "ชื่องาน";
           
        }
        private void Panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btnreport_Click(object sender, EventArgs e)
        {
            XtraReport11 f = new XtraReport11();
            f.ID = Convert.ToInt32(lxl2.Text);
            f.ShowPreviewDialog();
        }
    }
}
