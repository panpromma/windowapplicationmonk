﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraReports.UI;
namespace mork
{
    public partial class frmshowmonk : MetroFramework.Forms.MetroForm
    {
        public int UserID { get; set; }
        
        public frmshowmonk()
        {
            InitializeComponent();
            
        }
        SqlConnection conn;
        
        private void Btnadd_Click(object sender, EventArgs e)
        {
        }

        private void Showmonk_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            ShowData("");
        }
        //private void ShowData()
        //{
        //    string sql = "select UserID as รหัส , Name as ชื่อสกุล , Department as ฝ่าย , Phone as เบอร์โทร ,Age as อายุ  ,Prefix as คำนำหน้า from Personnels where Department ='พระสงฆ์'";
        //    SqlDataAdapter da = new SqlDataAdapter(sql, conn);
        //    DataSet ds = new DataSet();
        //    da.Fill(ds);
        //    dgvdatag.DataSource = ds.Tables[0];
        //    dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        //}

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            //if (txtID.Text != "")
            //{
            //    string Searching = txtID.Text;
            //    ShowData(Searching);
            //}
            ShowData(txtID.Text.ToString());
        }
        private void ShowData(string name)
        {
            //if (txtID.Text != "")
            //{
                string sql = "select  UserID,Prefix +  Name ,Department,Phone,Age from Personnels " +
                    "where Name like '%' +@name+ '%' " +
                    "and Department='พระสงฆ์' ";

                SqlCommand com = new SqlCommand(sql, conn);
                com.Parameters.AddWithValue("@name", name);
                SqlDataAdapter da = new SqlDataAdapter(com);
                DataSet ds = new DataSet();
                da.Fill(ds);
                dgvdatag.DataSource = ds.Tables[0];
                
                dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
                dgvdatag.Columns[1].HeaderCell.Value = "ชื่อสกุล";
                dgvdatag.Columns[2].HeaderCell.Value = "ฝ่าย";
                dgvdatag.Columns[3].HeaderCell.Value = "เบอร์โทร";
                dgvdatag.Columns[4].HeaderCell.Value = "อายุ";
                
            //}
            //else
            //{
            //    ShowData("");
            //}
        }

        private void Dgvdatag_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Dgvdatag_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
             var x = dgvdatag.CurrentRow.Cells;
            UserID = Convert.ToInt16(x[0].Value);
        }

        private void Dgvdatag_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //ประกาศให้เปิดฟอร์ม Form2 สำหรับแก้ไข
            frmshowkit1cs f = new frmshowkit1cs();
            //นำข้อมูลในตัวแปร ส่งไปที่ Form2 เพื่อเก็บไว้แก้ไข
            f.UserID = this.UserID;
            f.ShowDialog();
            ShowData("");
        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
           
        }
    }
}
