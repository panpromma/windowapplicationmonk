﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using DevExpress.XtraReports.UI;
namespace mork
{
    public partial class frmEquipment : MetroFramework.Forms.MetroForm
    {
      
        public frmEquipment()
        {
            InitializeComponent();
            
        }
        SqlConnection conn;
        
        private void FrmEquipment_Load(object sender, EventArgs e)
        {
           
            conn = new ConnecDB().SqlStrCon();
            ShowData();
            setcboCategoryem();
        }
        
       
        private void ShowData()
        {
            string sql = "select EquipmentID ,case when Status=0 then 'ไม่มี' when Status=1 then 'มี' end as สถานะการอนุมัติ , Equipmenname ,Numbertreasury,Numberdamaged,Numberlost,e.Description ,cs.CategorymID,cs.Description  from  Equipments e inner join Categoryequipments cs on e.CategorymID=cs.CategorymID ";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
            dgvdatag.Columns[2].HeaderCell.Value = "ชื่ออุปกรณ์";
            dgvdatag.Columns[3].HeaderCell.Value = "จำนวน";
            dgvdatag.Columns[4].HeaderCell.Value = "ชำรุด";
            dgvdatag.Columns[5].HeaderCell.Value = "สูญหาย";

            dgvdatag.Columns[6].HeaderCell.Value = "รายละเอียด";
            dgvdatag.Columns[7].HeaderCell.Value = "ประเภท";
            dgvdatag.Columns[8].HeaderCell.Value = "ชื่อประเภท";

        }
        private void setcboCategoryem()
        {
            string sql = "select CategorymID,Description from Categoryequipments order by CategorymID";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            cmbCategoryem.DataSource = ds.Tables[0];
            cmbCategoryem.DisplayMember = "Description";
            cmbCategoryem.ValueMember = "CategorymID";
        }

        private void Dgvdatag_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            var x = dgvdatag.CurrentRow.Cells;
            txtID.Text = x["EquipmentID"].Value.ToString();
            txtname.Text = x["Equipmenname"].Value.ToString();
            txtNumbertreasury.Text = x["Numbertreasury"].Value.ToString();
            txtDescription.Text = x["Description"].Value.ToString();
            cmbCategoryem.SelectedValue = x["CategorymID"].Value.ToString();
            
        }

        private void Btnadd_Click(object sender, EventArgs e)
        {
            if (txtname.Text.Trim() == "" || txtNumbertreasury.Text.Trim() == "" || txtDescription.Text.Trim() == "")
            {
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบถ้วน");
            }
            else
            {
                try
                {
                    string sql = "insert into Equipments (Status,Equipmenname,Numbertreasury,Numberdamaged,Numberlost,Description,CategorymID) values(@Status,@Equipmenname,@Numbertreasury,@Numberdamaged,@Numberlost,@Description,@CategorymID)";
                    SqlCommand comm = new SqlCommand(sql, conn);




                    comm.Parameters
                        .AddWithValue("@Status", "1");

                    comm.Parameters
                        .AddWithValue("@Equipmenname", txtname.Text.Trim());



                    comm.Parameters
                       .AddWithValue("@Numbertreasury", txtNumbertreasury.Text.Trim());
                    comm.Parameters
                       .AddWithValue("@Numberdamaged", "0");

                    comm.Parameters
                       .AddWithValue("@Numberlost", "0");

                    comm.Parameters
                      .AddWithValue("@Description", txtDescription.Text.Trim());

                    comm.Parameters
                       .AddWithValue("@CategorymID", cmbCategoryem.SelectedValue);




                    if (conn.State == ConnectionState.Closed) conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                    clearfrom();
                    ShowData();
                }
                catch
                {
                    MessageBox.Show("ชื่ออุปกรณ์ซ้ำ");
                    return;
                }
            }
        }
        

        private void clearfrom()
        {
            txtID.Text = "";
            txtname.Text = "";
         
           
            txtNumbertreasury.Text = "";
           
            cmbCategoryem.Text = "";
            txtDescription.Text = "";
           

          
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            clearfrom();
        }

        private void Btndelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("!!!!คณุต้องการจะลบใช่ไหม?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {

                string sql = "delete from Equipments where EquipmentID = @EquipmentID";
                SqlCommand comm = new SqlCommand(sql, conn);



                comm.Parameters

                    .AddWithValue("@EquipmentID", txtID.Text.Trim());

                try
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                    clearfrom();
                    ShowData();

                }
                catch
                {
                    MessageBox.Show("ไม่สามารถลบข้มูลได้", "Erorr");

                    return;
                }
            }
        }

        private void Btnmodify_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("!!!!คณุต้องการจะแก้ไขไหม?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {


                string sql = "update Equipments set  Status=@Status,Equipmenname=@Equipmenname,Numbertreasury=@Numbertreasury,Description=@Description,CategorymID=@CategorymID where  EquipmentID=@EquipmentID  ";
                SqlCommand comm = new SqlCommand(sql, conn);

                comm.Parameters
               .AddWithValue("@EquipmentID", txtID.Text.Trim());

                comm.Parameters
               .AddWithValue("@Status", "1");

                comm.Parameters
                    .AddWithValue("@Equipmenname", txtname.Text.Trim());

               

                comm.Parameters
                   .AddWithValue("@Numbertreasury", txtNumbertreasury.Text.Trim());

            

                comm.Parameters
                   .AddWithValue("@CategorymID", cmbCategoryem.SelectedValue);

                comm.Parameters
                  .AddWithValue("@Description", txtDescription.Text.Trim());





                try
                {
                    conn.Open();
                    comm.ExecuteNonQuery();
                    conn.Close();
                    clearfrom();
                    ShowData();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            
        }

        private void TxtNumbertreasury_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label4_Click(object sender, EventArgs e)
        {

        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void Label7_Click(object sender, EventArgs e)
        {

        }

        private void Label9_Click(object sender, EventArgs e)
        {

        }

        private void Label4_Click_1(object sender, EventArgs e)
        {

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            showdata(textBox1.Text.ToString());
        }
        private void showdata(string text)
        {
            string sql = "select EquipmentID ,case when Status=0 then 'ไม่มี' when Status=1 then 'มี' end as สถานะ , Equipmenname ,Numbertreasury,Numberdamaged,Numberlost,e.Description ,cs.CategorymID,cs.Description  from  Equipments e inner join Categoryequipments cs on e.CategorymID=cs.CategorymID" +
                " where e.Equipmenname like '%' + @strName + '%'";

            SqlCommand com = new SqlCommand(sql, conn);
            com.Parameters.AddWithValue("@strName", text);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];

            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.Columns[0].HeaderCell.Value = "รหัส";
            dgvdatag.Columns[2].HeaderCell.Value = "ชื่ออุปกรณ์";
            dgvdatag.Columns[3].HeaderCell.Value = "จำนวน";
            dgvdatag.Columns[4].HeaderCell.Value = "ชำรุด";
            dgvdatag.Columns[5].HeaderCell.Value = "สูญหาย";

            dgvdatag.Columns[6].HeaderCell.Value = "รายละเอียด";
            dgvdatag.Columns[7].HeaderCell.Value = "ประเภท";
            dgvdatag.Columns[8].HeaderCell.Value = "ชื่อประเภท";
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            xrequipments f = new xrequipments();
           
            f.ShowPreviewDialog();
        }

        private void Button1_Click(object sender, EventArgs e)
        {

            xrNumberdamaged f = new xrNumberdamaged();

            f.ShowPreviewDialog();
        }
    }
}
