﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
namespace mork
{
    public partial class frmshowkitnimon : MetroFramework.Forms.MetroForm
    {
       
        public frmshowkitnimon()
        {
            InitializeComponent();
            
        }
        SqlConnection conn;
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        public int UserID { get; set; }
        public string status { get; set; }
        public int KitnmonID { get; set; }//รหัสกิจนิมนต์
        public string Address { get; set; }//ที่อยู่
        public string Time { get; set; }//เวลา
        public string date { get; set; }//วันที่
        public string Approve { get; set; }//สถานะ
        public string Phone { get; set; }//เบอร์โทร
        public string Namep { get; set; }//ชื่อผู้รับ
        public string names { get; set; }//ชื่อฆราวาส
        public string WorkName { get; set; }//ชื่อ
        private void Showkitnimon_Load(object sender, EventArgs e)
        {
           
            conn = new ConnecDB().SqlStrCon();
            cbo();
            showdata1("");
           
        }
       private void  combo()
        {
           
        }
        private void ShowData(string name)
        {
            
           
        }
        //private void ShowData(string name)
        //{
        //   if (txtID.Text != "")
        //    {
        //        string sql = "select k.KitnmonID as รหัส ,k.Address as ที่อยู่,k.Time as เวลา,dbo.mydate(k.date) as วันเดือนปี , case  when k.Approve = 0 then 'ยังไม่อนุมัติ' when k.Approve = 1 then 'อนุมัติแล้ว' end as สถานะ ,k.Phone as เบอร์โทร ,p.Name as ชื่อผู้รับกิจนิมต์, s.name as ชื่อฆราวาส   ,c.WorkName as ชื่อกิจนิมนต์ from Kitnmons k inner join Personnels p on k.UserID = p.UserID inner join CategoryKitnimons c on k.CategoryKitID = c.CategoryKitID  inner join Seculars s on k.SecularID = s.SecularID" +
        //            " where date like '%' + @name + '%'" +
        //            "or s.name like '%' + @name + '%'" +
        //            "or p.Name like '%' + @name + '%'" +
        //            //"and k.Approve like '"+cboClass.SelectedValue.ToString()+ "'" +
        //            "order by KitnmonID desc";

        //        SqlCommand com = new SqlCommand(sql, conn);
        //        com.Parameters.AddWithValue("@name", name);
        //        SqlDataAdapter da = new SqlDataAdapter(com);
        //        DataSet ds = new DataSet();
        //        da.Fill(ds);
        //        dgvdatag.DataSource = ds.Tables[0];
        //        dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

        //    }
        //    else
        //    {
        //        ShowData();
        //    }
            
         
        //}
        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Dgvdatag_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Dgvdatag_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            var x = dgvdatag.CurrentRow.Cells;
            UserID = Convert.ToInt16(x[0].Value);

        }

        private void Dgvdatag_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            var x = dgvdatag.CurrentRow.Cells;
            KitnmonID = Convert.ToInt16(x[0].Value.ToString());
            Address = x[1].Value.ToString();
            Time = x[2].Value.ToString();
            date = x[3].Value.ToString();
            Approve = x[4].Value.ToString();
            Phone = x[5].Value.ToString();
            Namep = x[6].Value.ToString();
            names = x[7].Value.ToString();
            WorkName = x[8].Value.ToString();







            frmaddkitnimon f = new frmaddkitnimon();
            f.KitnmonID = this.KitnmonID;
            f.Address = this.Address;
            f.Time = this.Time;
            f.date = this.date;
            f.Approve = this.Approve;
            f.Phone = this.Phone;
            f.Namep = this.Namep;
            f.names = this.names;
            f.WorkName = this.WorkName;
            f.status = "แสดง";
            f.ShowDialog();
          
        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            showdata1(Convert.ToString(txtID.Text));
            showdata2(Convert.ToString(txtID.Text));
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void CboClass_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
        private void cbo()
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[] { new DataColumn("Id", typeof(int)), new DataColumn("Name", typeof(string)) });
            dt.Rows.Add(0, "ยังไม่อนุมัติ");
            dt.Rows.Add(1, "อุนิมัตแล้ว");
           

            //Insert the Default Item to DataTable.
            

            //Assign DataTable as DataSource.
            cboClass.DataSource = dt;
            cboClass.DisplayMember = "Name";
            cboClass.ValueMember = "Id";
          
        }
        private void showdata1(string txt)
        {
            string sql = "select k.KitnmonID as รหัส ,k.Address as ที่อยู่ ,c.WorkName as ชื่องาน ,k.TimeD as เวลา,dbo.mydate(k.DateD) as วันเดือนปี , case  when k.Approve = 0 then 'ยังไม่อนุมัติ' when k.Approve = 1 then 'อนุมัติแล้ว' end as สถานะ ,k.Phone as เบอร์โทร ,p.Name as ชื่อผู้รับกิจนิมต์, s.name as ชื่อฆราวาส   ,c.WorkName as ชื่อกิจนิมนต์ from Kitnmons k inner join Personnels p on k.UserID = p.UserID inner join CategoryKitnimons c on k.CategoryKitID = c.CategoryKitID  inner join Seculars s on k.SecularID = s.SecularID  inner join Kitnimonlists ks on k.KitnmonID=ks.KitnmonID " +
                "where s.name like '%' + @name + '%'" +
                "or k.TimeD like '%' + @name + '%'" +
                 "or p.Name like '%' + @name + '%'" +
                 "or c.WorkName like '%' + @name + '%'" +
                  "or k.Address like '%' + @name + '%'" +
                "order by k.KitnmonID desc ";

            //"or s.name like '%' + @name + '%'" +
            //"or p.Name like '%' + @name + '%'" +
            //"order by KitnmonID desc";

            SqlCommand com = new SqlCommand(sql, conn);
            SqlDataAdapter da = new SqlDataAdapter(com);
            com.Parameters.AddWithValue("@name", txt);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvdatag.Columns[0].Width = 50;
        }
        private void showdata2(string txt)
        {
            string sql = "select k.KitnmonID as รหัส ,k.Address as ที่อยู่ ,c.WorkName as ชื่องาน ,k.TimeD as เวลา,dbo.mydate(k.DateD) as วันเดือนปี , case  when k.Approve = 0 then 'ยังไม่อนุมัติ' when k.Approve = 1 then 'อนุมัติแล้ว' end as สถานะ ,k.Phone as เบอร์โทร ,p.Name as ชื่อผู้รับกิจนิมต์, s.name as ชื่อฆราวาส   ,c.WorkName as ชื่อกิจนิมนต์ from Kitnmons k inner join Personnels p on k.UserID = p.UserID inner join CategoryKitnimons c on k.CategoryKitID = c.CategoryKitID  inner join Seculars s on k.SecularID = s.SecularID inner join Kitnimonlists ks on k.KitnmonID=ks.KitnmonID " +
                " where k.Approve = '1' " +
                 "and s.name like '%' + @name + '%'" +
                "or ks.TimeS like '%' + @name + '%'" +
                "or ks.TimeEN like '%' + @name + '%'" +
                 "or p.Name like '%' + @name + '%'" +
                "or c.WorkName like '%' + @name + '%'" +
                 "or k.Address like '%' + @name + '%'" +
                "order by k.KitnmonID desc";


            SqlCommand com = new SqlCommand(sql, conn);
            SqlDataAdapter da = new SqlDataAdapter(com);
            com.Parameters.AddWithValue("@name", txt);
            DataSet ds = new DataSet();
            da.Fill(ds);
            dgvdatag.DataSource = ds.Tables[0];
            dgvdatag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(cboClass.SelectedValue.ToString());
            if (cboClass.SelectedIndex == 0)
            {
                showdata1("");
                showdata1(Convert.ToString(txtID.Text));
            }
            else if (cboClass.SelectedIndex == 1)
            {
                showdata2("");
                showdata2(Convert.ToString(txtID.Text));

            }
        }
    }
}
