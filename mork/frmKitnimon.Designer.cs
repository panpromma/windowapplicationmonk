﻿namespace mork
{
    partial class frmKitnimon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bunifuElipse1 = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cboWorkname = new System.Windows.Forms.ComboBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.dtpImportTime = new System.Windows.Forms.DateTimePicker();
            this.dtpImportDate = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btna = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtPhone = new System.Windows.Forms.Label();
            this.textname = new System.Windows.Forms.Label();
            this.txtSecularID = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cboVehiclesname = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lsvVehicle = new System.Windows.Forms.ListView();
            this.btnadd2 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.dgvdatag = new System.Windows.Forms.DataGridView();
            this.label14 = new System.Windows.Forms.Label();
            this.lsvShow = new System.Windows.Forms.ListView();
            this.btnadd = new System.Windows.Forms.Button();
            this.txtnamemonk = new System.Windows.Forms.TextBox();
            this.txtIDmonk = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lblID = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnseve = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatag)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 0;
            this.bunifuElipse1.TargetControl = this;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(88, 207);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 26);
            this.label2.TabIndex = 0;
            this.label2.Text = "ชื่องาน";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(490, 221);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 26);
            this.label3.TabIndex = 0;
            this.label3.Text = "ที่อยู่";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(84, 281);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 26);
            this.label4.TabIndex = 0;
            this.label4.Text = "เวลา";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(221, 281);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 26);
            this.label5.TabIndex = 0;
            this.label5.Text = "วันที่";
            // 
            // cboWorkname
            // 
            this.cboWorkname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboWorkname.FormattingEnabled = true;
            this.cboWorkname.Location = new System.Drawing.Point(82, 234);
            this.cboWorkname.Name = "cboWorkname";
            this.cboWorkname.Size = new System.Drawing.Size(204, 34);
            this.cboWorkname.TabIndex = 1;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(493, 249);
            this.txtAddress.MaxLength = 30;
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(247, 95);
            this.txtAddress.TabIndex = 2;
            // 
            // dtpImportTime
            // 
            this.dtpImportTime.Location = new System.Drawing.Point(82, 311);
            this.dtpImportTime.Name = "dtpImportTime";
            this.dtpImportTime.Size = new System.Drawing.Size(110, 33);
            this.dtpImportTime.TabIndex = 3;
            this.dtpImportTime.Value = new System.DateTime(2019, 6, 9, 18, 49, 0, 0);
            this.dtpImportTime.ValueChanged += new System.EventHandler(this.DateTimePicker1_ValueChanged);
            // 
            // dtpImportDate
            // 
            this.dtpImportDate.Location = new System.Drawing.Point(218, 311);
            this.dtpImportDate.Name = "dtpImportDate";
            this.dtpImportDate.Size = new System.Drawing.Size(220, 33);
            this.dtpImportDate.TabIndex = 4;
            this.dtpImportDate.ValueChanged += new System.EventHandler(this.DateTimePicker2_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(107, 26);
            this.label9.TabIndex = 6;
            this.label9.Text = "รหัสฆราวาส";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(330, 45);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 26);
            this.label7.TabIndex = 42;
            this.label7.Text = "เบอร์โทร";
            // 
            // btna
            // 
            this.btna.Location = new System.Drawing.Point(756, 94);
            this.btna.Name = "btna";
            this.btna.Size = new System.Drawing.Size(124, 58);
            this.btna.TabIndex = 7;
            this.btna.Text = "เลือกฆราวาส";
            this.btna.UseVisualStyleBackColor = true;
            this.btna.Click += new System.EventHandler(this.Btna_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.dtpImportDate);
            this.groupBox1.Controls.Add(this.dtpImportTime);
            this.groupBox1.Controls.Add(this.btna);
            this.groupBox1.Controls.Add(this.txtAddress);
            this.groupBox1.Controls.Add(this.cboWorkname);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("FC Lamoon", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(23, 77);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(979, 370);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "การรับกิจนิมนต์";
            this.groupBox1.Enter += new System.EventHandler(this.GroupBox1_Enter);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.txtPhone);
            this.groupBox5.Controls.Add(this.textname);
            this.groupBox5.Controls.Add(this.txtSecularID);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Location = new System.Drawing.Point(45, 50);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(633, 132);
            this.groupBox5.TabIndex = 44;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "ฆราวาส";
            // 
            // txtPhone
            // 
            this.txtPhone.AutoSize = true;
            this.txtPhone.Location = new System.Drawing.Point(340, 80);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(28, 26);
            this.txtPhone.TabIndex = 45;
            this.txtPhone.Text = "....";
            // 
            // textname
            // 
            this.textname.AutoSize = true;
            this.textname.Location = new System.Drawing.Point(151, 80);
            this.textname.Name = "textname";
            this.textname.Size = new System.Drawing.Size(48, 26);
            this.textname.TabIndex = 45;
            this.textname.Text = ".........";
            // 
            // txtSecularID
            // 
            this.txtSecularID.AutoSize = true;
            this.txtSecularID.Location = new System.Drawing.Point(18, 80);
            this.txtSecularID.Name = "txtSecularID";
            this.txtSecularID.Size = new System.Drawing.Size(28, 26);
            this.txtSecularID.TabIndex = 45;
            this.txtSecularID.Text = "....";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(149, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 26);
            this.label6.TabIndex = 42;
            this.label6.Text = "ชื่อฆราวาส";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox3.Controls.Add(this.cboVehiclesname);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.lsvVehicle);
            this.groupBox3.Controls.Add(this.btnadd2);
            this.groupBox3.Font = new System.Drawing.Font("FC Lamoon", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(475, 456);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(487, 319);
            this.groupBox3.TabIndex = 60;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "เพิ่มรายชื่อรถ";
            // 
            // cboVehiclesname
            // 
            this.cboVehiclesname.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboVehiclesname.FormattingEnabled = true;
            this.cboVehiclesname.Location = new System.Drawing.Point(249, 70);
            this.cboVehiclesname.Name = "cboVehiclesname";
            this.cboVehiclesname.Size = new System.Drawing.Size(124, 34);
            this.cboVehiclesname.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(277, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(151, 26);
            this.label12.TabIndex = 42;
            this.label12.Text = "พาหนะที่ใช้เดินทาง";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(139, 70);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 26);
            this.label13.TabIndex = 58;
            this.label13.Text = "รายชื่อรถ";
            // 
            // lsvVehicle
            // 
            this.lsvVehicle.HideSelection = false;
            this.lsvVehicle.Location = new System.Drawing.Point(59, 110);
            this.lsvVehicle.Name = "lsvVehicle";
            this.lsvVehicle.Size = new System.Drawing.Size(314, 172);
            this.lsvVehicle.TabIndex = 56;
            this.lsvVehicle.UseCompatibleStateImageBehavior = false;
            this.lsvVehicle.SelectedIndexChanged += new System.EventHandler(this.LsvVehicle_SelectedIndexChanged);
            this.lsvVehicle.DoubleClick += new System.EventHandler(this.LsvVehicle_DoubleClick);
            // 
            // btnadd2
            // 
            this.btnadd2.Image = global::mork.Properties.Resources.plus1;
            this.btnadd2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnadd2.Location = new System.Drawing.Point(378, 110);
            this.btnadd2.Name = "btnadd2";
            this.btnadd2.Size = new System.Drawing.Size(85, 44);
            this.btnadd2.TabIndex = 13;
            this.btnadd2.Text = "เพิ่ม";
            this.btnadd2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd2.UseVisualStyleBackColor = true;
            this.btnadd2.Click += new System.EventHandler(this.Btnadd2_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.dgvdatag);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.lsvShow);
            this.groupBox2.Controls.Add(this.btnadd);
            this.groupBox2.Controls.Add(this.txtnamemonk);
            this.groupBox2.Controls.Add(this.txtIDmonk);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("FC Lamoon", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(44, 462);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(697, 319);
            this.groupBox2.TabIndex = 59;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "เพิ่มรายชื่อพระ";
            this.groupBox2.Enter += new System.EventHandler(this.GroupBox2_Enter);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(22, 53);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(167, 26);
            this.label15.TabIndex = 61;
            this.label15.Text = "รายชื่อพระสงฆ์ที่ว่าง";
            // 
            // dgvdatag
            // 
            this.dgvdatag.AllowUserToAddRows = false;
            this.dgvdatag.AllowUserToDeleteRows = false;
            this.dgvdatag.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvdatag.BackgroundColor = System.Drawing.Color.White;
            this.dgvdatag.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvdatag.Location = new System.Drawing.Point(27, 89);
            this.dgvdatag.Name = "dgvdatag";
            this.dgvdatag.ReadOnly = true;
            this.dgvdatag.RowHeadersVisible = false;
            this.dgvdatag.Size = new System.Drawing.Size(275, 209);
            this.dgvdatag.TabIndex = 60;
            this.dgvdatag.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dgvdatag_CellContentClick);
            this.dgvdatag.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.Dgvdatag_CellMouseUp);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(329, 233);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(33, 26);
            this.label14.TabIndex = 59;
            this.label14.Text = "ชื่อ";
            this.label14.Visible = false;
            // 
            // lsvShow
            // 
            this.lsvShow.HideSelection = false;
            this.lsvShow.Location = new System.Drawing.Point(431, 70);
            this.lsvShow.Name = "lsvShow";
            this.lsvShow.Size = new System.Drawing.Size(223, 228);
            this.lsvShow.TabIndex = 10;
            this.lsvShow.UseCompatibleStateImageBehavior = false;
            this.lsvShow.DoubleClick += new System.EventHandler(this.LsvShow_DoubleClick);
            // 
            // btnadd
            // 
            this.btnadd.Image = global::mork.Properties.Resources.plus1;
            this.btnadd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnadd.Location = new System.Drawing.Point(320, 110);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(90, 44);
            this.btnadd.TabIndex = 11;
            this.btnadd.Text = "เพิ่ม";
            this.btnadd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnadd.UseVisualStyleBackColor = true;
            this.btnadd.Click += new System.EventHandler(this.Btnadd_Click);
            // 
            // txtnamemonk
            // 
            this.txtnamemonk.Location = new System.Drawing.Point(324, 262);
            this.txtnamemonk.Name = "txtnamemonk";
            this.txtnamemonk.ReadOnly = true;
            this.txtnamemonk.Size = new System.Drawing.Size(82, 33);
            this.txtnamemonk.TabIndex = 9;
            this.txtnamemonk.Visible = false;
            this.txtnamemonk.TextChanged += new System.EventHandler(this.Txtnamemonk_TextChanged);
            // 
            // txtIDmonk
            // 
            this.txtIDmonk.Location = new System.Drawing.Point(334, 197);
            this.txtIDmonk.Name = "txtIDmonk";
            this.txtIDmonk.ReadOnly = true;
            this.txtIDmonk.Size = new System.Drawing.Size(57, 33);
            this.txtIDmonk.TabIndex = 8;
            this.txtIDmonk.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(350, 168);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 26);
            this.label1.TabIndex = 55;
            this.label1.Text = "รหัส";
            this.label1.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.lblID);
            this.groupBox4.Controls.Add(this.lblName);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Font = new System.Drawing.Font("FC Lamoon", 20.25F);
            this.groupBox4.Location = new System.Drawing.Point(556, 14);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(369, 76);
            this.groupBox4.TabIndex = 61;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "ข้อมูลพนักงาน";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("FC Lamoon", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(76, 34);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 26);
            this.label10.TabIndex = 53;
            this.label10.Text = "ชื่อ-สกุล";
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.BackColor = System.Drawing.Color.Transparent;
            this.lblID.Font = new System.Drawing.Font("FC Lamoon", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblID.Location = new System.Drawing.Point(39, 34);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(29, 26);
            this.lblID.TabIndex = 9;
            this.lblID.Text = "ID";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.Font = new System.Drawing.Font("FC Lamoon", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(160, 34);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(24, 26);
            this.lblName.TabIndex = 51;
            this.lblName.Text = "...";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("FC Lamoon", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(15, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 26);
            this.label8.TabIndex = 52;
            this.label8.Text = "ID";
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Font = new System.Drawing.Font("FC Lamoon", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Image = global::mork.Properties.Resources.broom;
            this.btnClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClear.Location = new System.Drawing.Point(999, 150);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(107, 51);
            this.btnClear.TabIndex = 16;
            this.btnClear.Text = "เคลียร์";
            this.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.BtnClear_Click_1);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.Font = new System.Drawing.Font("FC Lamoon", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.Image = global::mork.Properties.Resources.Print_48px;
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.Location = new System.Drawing.Point(998, 89);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(107, 51);
            this.btnPrint.TabIndex = 15;
            this.btnPrint.Text = "พิมพ์";
            this.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // btnseve
            // 
            this.btnseve.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnseve.Font = new System.Drawing.Font("FC Lamoon", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnseve.Image = global::mork.Properties.Resources.save;
            this.btnseve.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnseve.Location = new System.Drawing.Point(997, 82);
            this.btnseve.Name = "btnseve";
            this.btnseve.Size = new System.Drawing.Size(107, 51);
            this.btnseve.TabIndex = 14;
            this.btnseve.Text = "ทันทึก";
            this.btnseve.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnseve.UseVisualStyleBackColor = true;
            this.btnseve.Click += new System.EventHandler(this.Btnseve_Click);
            // 
            // frmKitnimon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1117, 832);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnseve);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmKitnimon";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Style = MetroFramework.MetroColorStyle.Yellow;
            this.Text = "กาารรับกิตนิมนต์";
            this.Load += new System.EventHandler(this.FrmKitnimon_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvdatag)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Bunifu.Framework.UI.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cboVehiclesname;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ListView lsvVehicle;
        private System.Windows.Forms.Button btnadd2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtnamemonk;
        private System.Windows.Forms.TextBox txtIDmonk;
        private System.Windows.Forms.ListView lsvShow;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnseve;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btna;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtpImportDate;
        private System.Windows.Forms.DateTimePicker dtpImportTime;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.ComboBox cboWorkname;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label txtPhone;
        private System.Windows.Forms.Label textname;
        private System.Windows.Forms.Label txtSecularID;
        private System.Windows.Forms.DataGridView dgvdatag;
        private System.Windows.Forms.Label label15;
    }
}