﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DevExpress.XtraReports.UI;

namespace mork
{
    public partial class frmpint : MetroFramework.Forms.MetroForm
    {

        public int RequestID { get; set; }//รหัสกิจนิมนต์
        public string Date { get; set; }
        public string Time { get; set; }
        public string Description { get; set; }
        public string Phone { get; set; }
        public string PlaceID { get; set; }
        public string UserID { get; set; }//ชื่อผู้รับ
        public string SecularID { get; set; }//ชื่อฆราวาส
        public int ID { get; set; }
        SqlConnection conn;
        public frmpint()
        {
            InitializeComponent();
        }

        private void Frmpint_Load(object sender, EventArgs e)
        {
            conn = new ConnecDB().SqlStrCon();
            ShowData();
        }
        private void ShowData()
        {
            string sql = "select RequestID as รหัส ,dbo.mydate(Date) as วันที่, Time as เวลา ,ps.Description as รายละเอียด , ps.Phone as เบอร์โทร , p.PlaceName as ชื่อสถานที่ , pp.Name as ชื่อผู้รับงาน , s.name as ชื่อฆราวาส from Placerequests ps inner join Places p on ps.PlaceID=p.PlaceID inner join Personnels pp on ps.UserID=pp.UserID inner join Seculars s on ps.SecularID=s.SecularID order by RequestID DESC";
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            da.Fill(ds);
            datag.DataSource = ds.Tables[0];
            datag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void Iconcerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Dgvdatag_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
           
          var x = datag.CurrentRow.Cells;
            RequestID = Convert.ToInt16(x[0].Value.ToString());
            Date = x[1].Value.ToString();
            Time = x[2].Value.ToString();
            Description = x[3].Value.ToString();
            Phone = x[4].Value.ToString();
            PlaceID = x[5].Value.ToString();
            UserID = x[6].Value.ToString();
            SecularID = x[7].Value.ToString();

            frmshowdataPlacerequests f = new frmshowdataPlacerequests();
            f.RequestID = this.RequestID;
            f.Date = this.Date;
            f.Time = this.Time;
            f.Description = this.Description;
            f.Phone = this.Phone;
            f.PlaceID = this.PlaceID;
            f.UserID = this.UserID;
            f.SecularID = this.SecularID;


            f.ShowDialog();
            ShowData();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void TxtID_TextChanged(object sender, EventArgs e)
        {
            ShowData1(txtID.Text);
        }
        private void ShowData1(string text)
        {
            string sql = "select RequestID as รหัส , dbo.mydate(Date) as วันที่ , Time as เวลา ,ps.Description as รายละเอียด , ps.Phone as เบอร์โทร , p.PlaceName as ชื่อสถานที่ , pp.Name as ชื่อผู้รับงาน , s.name as ชื่อฆราวาส from Placerequests ps inner join Places p on ps.PlaceID=p.PlaceID inner join Personnels pp on ps.UserID=pp.UserID inner join Seculars s on ps.SecularID=s.SecularID" +
                " where Date like '%' + @strName + '%'" +
                "or PlaceName like '%' + @strName + '%'" +
                "or s.name like '%' + @strName + '%'" +
                " order by RequestID DESC";
            SqlCommand com = new SqlCommand(sql, conn);
            com.Parameters.AddWithValue("@strName", text);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);
            datag.DataSource = ds.Tables[0];
            datag.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void Datag_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            xrEQd f = new xrEQd();
            f.ShowPreviewDialog();
        }
    }
}
