use master
go

if exists(select * from master.dbo.sysdatabases where name = 'monk2')
	drop database monk2
go

create database monk2
go

alter database monk2 collate Thai_CI_AS
go

Use monk2
go


create table Personnels								--�ؤ�ҡ���Ѵ
(
	UserID int identity(1,1) Primary key,			--���ʼ����ҹ
	Name nVarchar(50) Not Null,						--����-ʡ��
	Department nVarchar(20) Not Null,				--����
	Phone nVarchar(10) Not Null,					--������
	Username nVarchar(20) Not Null,					--username
	Password nVarchar(30) Not Null,					--password
	Age int not null,								--����
	Address nVarchar(100)  Null,						--������� 
	Prefix nVarchar(10)  Null,						--�ӹ�˹�ҹ��
	constraint UQ_Personnels_Username unique
							(
								Username
							)
	
)
go
create table Seculars								--������
(
	SecularID int identity(1,1) Primary key,		--���ʦ�����
	name  nVarchar(30) Not Null,					--����-ʡ��
	phone  nVarchar(30) Not Null,					--������
	Address  nVarchar(100) Not Null,					--�������
	age int not null,								--����
)
go
create table Vehicles								--�ҹ��˹�
(
	VehicleID int identity(1,1) Primary key,		--�����ҹ��˹�
	size nVarchar(30) Not Null,						--��Ҵ
	Vehiclecategory nVarchar(30) Not Null,			--�������ҹ��˹�
	color nVarchar(30) Not Null,					--��
	License nVarchar(30) Not Null,					--�к�¹
	capacity int Not Null,							--������
	Brand nVarchar(30) Not Null,					--������
	Status int not null,								--��ҹ�
	constraint UQ_Vehicles_License unique
							(
								License
							)
)
go
create table Places									--ʶҹ���
(
	PlaceID int identity(1,1) Primary key,			--����ʶҹ���
	PlaceName nVarchar(30) Not Null,				--����ʶҹ���
	Width nVarchar(30) Not Null,					--�������ҧ
	Length nVarchar(30) Not Null,					--�������
	Description nVarchar(100) Not Null,				-- ��������´
	Status int Not Null,								--ʶҹ�

	constraint UQ_Places_PlaceName unique
							(
								PlaceName
							)
	
)
go
create table CategoryKitnimons						--�������Ԩ������
(
	CategoryKitID int identity(1,1) Primary key,	--���ʻ������Ԩ������
	WorkName  nVarchar(30) Not Null,				--���ͧҹ
	Description nVarchar(100) Null,					--��������´
	constraint UQ_CategoryKitnimons_WorkName unique
							(
								WorkName
							)
)
go
create table Categoryequipments						--��������ʴ��ػ�ó�
(
	CategorymID int identity(1,1) Primary key,		--���ʻ�������ʴ��ػ�ó�
	Description nVarchar(100) Null					--��������´
	
)
go
create table Equipments								--��ʴ��ػ�ó�
(
	EquipmentID int identity(1,1) Primary key,		--������ʴ��ػ�ó�
	Status int Not Null,							--ʶҹ�
	Equipmenname nVarchar(30) Not Null,				--������ʴ��ػ�ó�
	Numbertreasury int Not Null,					--�ӹǹ㹤�ѧ
	Numberdamaged int Null,							--�ӹǹ�����ش
	Numberlost int Null,							--�ӹǹ�٭���
	Description nVarchar(100) Null,					--��������´
	CategorymID int,								--��������ʴ��ػ�ó�

	foreign key(CategorymID) references Categoryequipments(CategorymID),

	constraint UQ_Equipments_Equipmenname unique
							(
								Equipmenname
							)


)
go
create table Borrows								--��������ʴ��ػ�ó�
(
	BorrowID int identity(1,1) Primary key,			--���ʡ�������ʴ��ػ�ó�
	RedateB date Not Null,							--�ѹ�����Ҥ׹
	Redate date Not Null,							--�ѹ������
	Time time Not Null,								--����
	Retundate date not Null ,						--�ѹ�����Ҥ׹�ú
	Approve int not Null,							--͹��ѵ�
	UserID int,										--���ʼ����ҹ
	SecularID int									--���ʦ�����
	foreign key(UserID) references Personnels(UserID),
	foreign key(SecularID) references Seculars(SecularID)

)
go
create table Equipmentlists							--��¡�������ʴ��ػ�ó�
(	OrderKE int,									--�ӴѺ���
	BorrowID int,									--���ʡ�������ʴ��ػ�ó�
	Number int not null,							--�ӹǹ
	Date date not null,								--�ѹ���
	damaged nVarchar(30) null,						--���ش
	lost nVarchar(30) null,							--�٭���
	EquipmentID int ,								--������ʴ��ػ�ó�
	 Primary key (OrderKE,BorrowID),
	foreign key(BorrowID) references Borrows(BorrowID),
	foreign key(EquipmentID) references Equipments(EquipmentID)


)
go
create table LostDamageds							--�٭���/���ش
(
	List int identity(1,1) Primary key,				--��¡�÷��
	Number int not null,							--�ӹǹ
	Cause nVarchar(50) Null,						--���˵�
	Description nVarchar(100) Null,					--��������´
	EquipmentID int,								--������ʴ��ػ�ó�
	foreign key(EquipmentID) references Equipments(EquipmentID)

)

go
create table Donateequipments						--��ú�ԨҤ��ʴ��ػ�ó�
(
	DonateequipID int identity(1,1) Primary key,	--���ʡ�ú�ԨҤ��ʴ��ػ�ó�
	datedonat date Not Null,
	description nVarchar(50)Not Null,
	donor nVarchar(30)Not Null,
	UserID int ,
	foreign key(UserID) references Personnels(UserID),--������ʴ��ػ�ó�
	

)
go


create table DonateEMlists							--��¡�ú�ԨҤ�ػ�ó�
(	
	DonateequipID int   ,							--���ʡ�ú�ԨҤ��ʴ��ػ�ó�
	EquipmentID int  ,								--������ʴ��ػ�ó�
	Number int not null,							--�ӹǹ
	 Primary key (DonateequipID,EquipmentID),
	foreign key(DonateequipID) references Donateequipments(DonateequipID),
	foreign key(EquipmentID) references Equipments(EquipmentID)
	
	
	

)
go
create table Kitnmons								--����Ѻ�Ԩ������
(
	
	KitnmonID int identity(1,1) Primary key,		--���ʡ���Ѻ�Ԩ������
	Address nvarchar(30),							--�������
	Workname nvarchar(30),							--����	
	TimeD	time,									--����
	dateD	date,									--�ѹ���
	Approve int,									--͹��ѵ�
	Phone nVarchar(30) ,							--������
	UserID int,										--���ʼ����ҹ
	SecularID int,									--���ʦ�����
	CategoryKitID int,								--���ʻ������Ԩ������
	foreign key(UserID) references Personnels(UserID),
	foreign key(SecularID) references Seculars(SecularID),
	foreign key(CategoryKitID) references CategoryKitnimons(CategoryKitID)
)
go
create table Kitnimonlists							--���������ҹ�Ԩ������
(
	Number int,										--�ӹǹ
	UserID int,										--���ʼ����ҹ
	KitnmonID int,									--���ʡ���Ѻ�Ԩ������
	Dates date ,									--�ѹ���
	TimeS time ,									--���������
	TimeEN time,									--���Ҩ�				
	 Primary key (Number,UserID,KitnmonID),
	foreign key(UserID) references Personnels(UserID),
	foreign key(KitnmonID) references Kitnmons(KitnmonID),
)
go
create table Usevehicless							--������ҹ��˹�
(
	
	KitnmonID int,									--���ʡ���Ѻ�Ԩ������
	VehicleID int									--�����ҹ��˹�
	 Primary key (KitnmonID,VehicleID),
	foreign key(KitnmonID) references Kitnmons(KitnmonID),
	foreign key(VehicleID) references Vehicles(VehicleID)
)
go
create table Placerequests							--��â���ʶҹ���
(
	RequestID int identity(1,1) Primary key,		--���ʡ�â���ʶҹ���
	Date date,										--�ѹ���
	Time nVarchar(30) Not Null,						--����
	Description nVarchar(100) Null,					--��������´
	Phone nVarchar(30) ,							--������
	Approve int,									--͹��ѵ�
	PlaceID int,									--����ʶҹ���
	UserID int ,									--���ʼ����ҹ
	SecularID int,									--���ʦ�����
	foreign key(PlaceID) references Places(PlaceID),
	foreign key(UserID) references Personnels(UserID),
	foreign key(SecularID) references Seculars(SecularID)
	

)
go
create table Donates								--��ú�ԨҤ�Թ
(
	Donate int identity(1,1) Primary key,			--���ʡ�ú�ԨҤ�Թ
	Description nVarchar(100) Null,					--�������´
	Number money ,									--�ӹǹ
	Date date,										--�ѹ���
	UserID int,										--���ʼ����ҹ
	SecularID int									--���ʦ�����
	foreign key(UserID) references Personnels(UserID),
	foreign key(SecularID) references Seculars(SecularID)
)
go

INSERT INTO Personnels
           (Name ,						--����-ʡ��
			Department ,				--����
			Phone ,					--������
			Username ,					--username
			Password ,					--password
			Age ,								--����
			Address ,					--������� 
			Prefix )						--�ӹ�˹�ҹ��
     VALUES
		(	'�Ÿ�Ѫ �ѹ�����',
			'�������к�',
			'-',
			'admin123',
			'123',
			'-',
			'-',
			'���'
			
		)
GO